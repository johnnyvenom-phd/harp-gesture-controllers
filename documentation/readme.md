Larger files related to this project are downloadable at: 

https://www.dropbox.com/sh/qjay1ovuurpnhc1/AACK0QLZgFbEu6ZoZwVbejPia?dl=0

including: 

- video performances from ICLI 2019 in Porto, Portugal
- documentary video of project (HarpMocap_video.mp4)
- .ppt presentation from MOCO 2019 in Genoa, Italy

