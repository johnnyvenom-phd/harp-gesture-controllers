Harp Gesture Controllers
========================

This repo contains softwares and documentation for the gesture controllers used in the project "Harp Gesture Acquisition for Control of Audiovisual Synthesis". 

Project team: 

- John Sullivan - project PI, motion capture study, controller software
- Alex Tibbitts - harpist, performer, motion capture study, system co-designer
- Olafur Bogason - hardware and firmware designer, CTO of [Genki Instruments](http://genkiinstruments.com)
- Brice Gatinet - composer

For more information see the [documentation](./documentation) folder. 

A paper on the project was presented at MOCO '18 conference Genoa, Italy, and performances with the system were held in Montreal (April & May 2017) and at ICLI '18 (Porto). 

## Repo contents: 

- [documentation](./documentation)
    - images
    - ICLI submission for performance
    - MOCO 2018 conference paper on the project
    - additional media can be downloaded [here](https://www.dropbox.com/sh/qjay1ovuurpnhc1/AACK0QLZgFbEu6ZoZwVbejPia?dl=0) , including videos and .ppt slides of MOCO presentation (too large to host on Gitlab).
- HarpMocap_software
    - USE_THIS_genki-controller: Max interface software for hardware controllers
    - other_versions: older versions and variations of genki controller software
    - Max for Live device: genki controller software packaged as a Max for Live device
    - controller firmwares: Arduino firmware for the devices
    - ICLI_controller: uses early Genki Wave prototype, see note below. 

For the original project (HarpMocap_software) the hardware consisted of: 

- controller: 
    - ESP8266 with static IP address
    - IMU (MPU9250)
    - ERM haptic motor
    - 2 leds
    - 3d printed enclosure
    - bracelet to wear on back of hand or elsewhere on body
- Linksys WRT54GL Woreless router with Tomato firmware
    - not certain about network, but likely settings are: 
        - Network name: bodysuit (or bodysuit1)
        - Password: bodysuit (or bodysuit1)
        - Router access: 192.168.1.1, user: admin (?), password: ??? (maybe admin, maybe blank?)
    - if the above doesn't work, just factory reset the modem, create a new network in router settings, and update the controller code to connect to that network. 

For the 2018 ICLI performance, new Genki Instruments Wave prototypes were used that implemented BLE MIDI for sending controller data. The controllers are available and the **ICLI_controller** directory contains the Max patch that was used. The patch uses [GRM Tools](https://inagrm.com/en/store) extensively, which are not included here and not free. 