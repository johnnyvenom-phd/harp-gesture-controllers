{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 763.0, 79.0, 794.0, 892.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-24",
					"linecount" : 12,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 102.0, 454.0, 154.0, 150.0 ],
					"style" : "",
					"text" : "TODO- 28-Mar: \n\n- save presets when loaded as an abstraction\n- save min/max calibration with presets\n- add 2nd arg. for initial preset\n- haptic feedback?\n-record audio with mubu (stereo)\n- april 1st final patch. "
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 546.0, 429.0, 99.0, 20.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 546.0, 493.0, 20.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 465.0, 396.0, 99.0, 20.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 465.0, 460.0, 20.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 389.0, 365.0, 99.0, 20.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 389.0, 429.0, 20.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"linecount" : 6,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 641.0, 55.0, 119.0, 78.0 ],
					"style" : "",
					"text" : "OSC messages out \n/device# \n    /x-axis {float}\n    /y-axis {float}\n    /z-axis {float}\n    /jab {bang}"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 405.0, 235.0, 150.0, 18.0 ],
					"style" : "",
					"text" : "from device 142"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 617.0, 296.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-19",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 541.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-20",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 465.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-21",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 389.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 389.0, 257.0, 323.0, 20.0 ],
					"style" : "",
					"text" : "route /x-axis /y-axis /z-axis /jab"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 46.0, 235.0, 150.0, 18.0 ],
					"style" : "",
					"text" : "from device 140"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"linecount" : 10,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 293.0, 31.0, 328.0, 126.0 ],
					"style" : "",
					"text" : "USE:\n\n- initialize genki-controller with argument == device number\n- abstractions output controller values formatted as osc messages that you can use to route signals to your patch. \n\n**genki-controller(s) MUST be loaded as abstractions in your main patch, and should be given an initial argument.\n***I haven't tested 2 devices together yet so this may need additional work. "
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 258.0, 296.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-9",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 182.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-8",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 106.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-7",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 30.0, 296.0, 67.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 30.0, 257.0, 323.0, 20.0 ],
					"style" : "",
					"text" : "route /x-axis /y-axis /z-axis /jab"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 30.0, 187.0, 91.0, 20.0 ],
					"style" : "",
					"text" : "route /140 /142"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 152.5, 78.0, 126.0, 20.0 ],
					"style" : "",
					"text" : "genki-controller 142 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, 78.0, 116.0, 20.0 ],
					"style" : "",
					"text" : "genki-controller 140"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"order" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"order" : 1,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 2 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-2::obj-194" : [ "live.text[25]", "live.text[6]", 0 ],
			"obj-2::obj-37" : [ "number[16]", "number[1]", 0 ],
			"obj-2::obj-19" : [ "multislider[2]", "multislider", 0 ],
			"obj-1::obj-147" : [ "live.text[3]", "live.text[3]", 0 ],
			"obj-2::obj-23::obj-38" : [ "number[20]", "number[3]", 0 ],
			"obj-2::obj-62" : [ "matrixctrl[1]", "matrixctrl", 0 ],
			"obj-1::obj-40" : [ "number[13]", "number", 0 ],
			"obj-2::obj-114::obj-130" : [ "1pb_large[1]", "1pb_large", 0 ],
			"obj-2::obj-184" : [ "live.text[33]", "live.text[6]", 0 ],
			"obj-2::obj-105" : [ "uni-bi-polar[1]", "uni-bi-polar", 0 ],
			"obj-1::obj-23::obj-41" : [ "number[9]", "number[5]", 0 ],
			"obj-1::obj-132" : [ "live.text", "live.text", 0 ],
			"obj-2::obj-40" : [ "number[17]", "number", 0 ],
			"obj-1::obj-23::obj-40" : [ "number[10]", "number[4]", 0 ],
			"obj-1::obj-186" : [ "live.text[8]", "live.text[6]", 0 ],
			"obj-2::obj-185" : [ "live.text[20]", "live.text[6]", 0 ],
			"obj-1::obj-158" : [ "live.text[16]", "live.text[16]", 0 ],
			"obj-1::obj-105" : [ "uni-bi-polar", "uni-bi-polar", 0 ],
			"obj-1::obj-185" : [ "live.text[7]", "live.text[6]", 0 ],
			"obj-2::obj-158" : [ "live.text[28]", "live.text[16]", 0 ],
			"obj-1::obj-62" : [ "matrixctrl", "matrixctrl", 0 ],
			"obj-1::obj-114::obj-129" : [ "2pb_large", "2pb_large", 0 ],
			"obj-2::obj-23::obj-40" : [ "number[19]", "number[4]", 0 ],
			"obj-1::obj-187" : [ "live.text[9]", "live.text[6]", 0 ],
			"obj-2::obj-103" : [ "toggle[1]", "toggle", 0 ],
			"obj-1::obj-116" : [ "live.text[2]", "live.text[2]", 0 ],
			"obj-2::obj-114::obj-129" : [ "2pb_large[1]", "2pb_large", 0 ],
			"obj-1::obj-114::obj-132" : [ "live.text[1]", "live.text", 0 ],
			"obj-2::obj-147" : [ "live.text[18]", "live.text[3]", 0 ],
			"obj-2::obj-148" : [ "live.text[22]", "live.text[14]", 0 ],
			"obj-1::obj-103" : [ "toggle", "toggle", 0 ],
			"obj-1::obj-189" : [ "live.dial", "smooth", 0 ],
			"obj-2::obj-23::obj-41" : [ "number[18]", "number[5]", 0 ],
			"obj-1::obj-23::obj-47" : [ "toggle[4]", "toggle[2]", 0 ],
			"obj-2::obj-114::obj-128" : [ "3pb_large[1]", "3pb_large", 0 ],
			"obj-1::obj-184" : [ "live.text[6]", "live.text[6]", 0 ],
			"obj-2::obj-189" : [ "live.dial[1]", "smooth", 0 ],
			"obj-2::obj-77" : [ "number[14]", "number[13]", 0 ],
			"obj-1::obj-37" : [ "number[12]", "number[1]", 0 ],
			"obj-1::obj-157" : [ "live.text[15]", "live.text[15]", 0 ],
			"obj-2::obj-132" : [ "live.text[23]", "live.text", 0 ],
			"obj-1::obj-114::obj-102" : [ "live_large", "live_large", 0 ],
			"obj-1::obj-19" : [ "multislider[1]", "multislider", 0 ],
			"obj-1::obj-148" : [ "live.text[14]", "live.text[14]", 0 ],
			"obj-2::obj-157" : [ "live.text[29]", "live.text[15]", 0 ],
			"obj-1::obj-114::obj-128" : [ "3pb_large", "3pb_large", 0 ],
			"obj-1::obj-146" : [ "live.text[13]", "live.text[13]", 0 ],
			"obj-2::obj-146" : [ "live.text[31]", "live.text[13]", 0 ],
			"obj-1::obj-77" : [ "number[1]", "number[13]", 0 ],
			"obj-2::obj-114::obj-132" : [ "live.text[21]", "live.text", 0 ],
			"obj-1::obj-135" : [ "live.text[12]", "live.text[12]", 0 ],
			"obj-1::obj-76" : [ "number[2]", "number[2]", 0 ],
			"obj-2::obj-116" : [ "live.text[19]", "live.text[2]", 0 ],
			"obj-1::obj-133" : [ "live.text[11]", "live.text[11]", 0 ],
			"obj-2::obj-153" : [ "live.text[17]", "live.text[3]", 0 ],
			"obj-1::obj-194" : [ "live.text[10]", "live.text[6]", 0 ],
			"obj-2::obj-186" : [ "live.text[26]", "live.text[6]", 0 ],
			"obj-1::obj-23::obj-38" : [ "number[11]", "number[3]", 0 ],
			"obj-2::obj-149" : [ "live.text[27]", "live.text[4]", 0 ],
			"obj-1::obj-149" : [ "live.text[4]", "live.text[4]", 0 ],
			"obj-2::obj-187" : [ "live.text[30]", "live.text[6]", 0 ],
			"obj-1::obj-153" : [ "live.text[5]", "live.text[3]", 0 ],
			"obj-2::obj-23::obj-47" : [ "toggle[5]", "toggle[2]", 0 ],
			"obj-2::obj-135" : [ "live.text[32]", "live.text[12]", 0 ],
			"obj-1::obj-114::obj-130" : [ "1pb_large", "1pb_large", 0 ],
			"obj-2::obj-114::obj-102" : [ "live_large[1]", "live_large", 0 ],
			"obj-2::obj-76" : [ "number[15]", "number[2]", 0 ],
			"obj-2::obj-133" : [ "live.text[24]", "live.text[11]", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "genki-controller.maxpat",
				"bootpath" : "~/Dropbox/CIRMMT-student-project/Controller Patches/genki-controller",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "random_data.maxpat",
				"bootpath" : "~/Documents/Max 7/Library",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.jab.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.jab",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.median.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.median",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.split.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.split",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.extrema.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.extrema",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.windowedextrema.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.windowedextrema",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "input_scale.maxpat",
				"bootpath" : "~/Dropbox/CIRMMT-student-project/Controller Patches/genki-controller",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dot.autoscale.maxpat",
				"bootpath" : "~/Documents/Max 7/Library/dot_20121204/filters/dot.autoscale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "multislider001",
				"default" : 				{
					"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "myteststyle",
				"default" : 				{
					"accentcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "panelGold-1",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.764706, 0.592157, 0.101961, 0.25 ],
						"color1" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "texteditGold",
				"default" : 				{
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
