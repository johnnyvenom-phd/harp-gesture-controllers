INSTRUCTIONS/INFO: 

- Copy this entire folder to Max file path or root of your main working patch. 
- load genki-controller.maxpat as abstraction in your main patch. 
- it takes an argument of the device number (140 - 143)
- outputs signals in OSC messages (see main-patch-demo for details)

IMPROVEMENTS ON PATCH: 
- added min/max scaling for each axis. set these first. 
    - These will not (currently) save with presets, but i'll add this.
- added smoothing. 0.25 seems nice but adjust to your own liking. 
    - 0.00 == no smoothing; 1.00 = max smoothing (slower change over time)
- Improvements to visual output. 
- Working with Baptiste this afternoon to implement better gesture recording. 

*** Have not tested multiple controllers together yet.
*** It may cause conflicts, but I will test in the next day and make it work. 
