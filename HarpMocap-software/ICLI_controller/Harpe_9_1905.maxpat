{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 132.0, 79.0, 1149.0, 787.0 ],
		"bglocked" : 1,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-511",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1379.83252, 500.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-513",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1344.603516, 500.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-523",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1342.733276, 542.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-524",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1358.733276, 578.557373, 34.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-503",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1440.733276, 498.961182, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0 127 105 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-505",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1440.733276, 455.953979, 75.0, 22.0 ],
					"style" : "",
					"text" : "r genki_yaw"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-507",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1471.993164, 531.0, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-509",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1465.92688, 560.543945, 69.0, 22.0 ],
					"style" : "",
					"text" : "sel 1 14 15"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 25.0,
					"id" : "obj-501",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1192.067749, 488.796051, 66.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1329.203369, 410.638428, 66.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 25.0,
					"id" : "obj-491",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1.850647, 917.571899, 66.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1195.203369, 416.590088, 66.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 25.0,
					"id" : "obj-489",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 530.657471, 149.796021, 66.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 922.657471, 416.590088, 66.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-485",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2177.366943, 974.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 677.57312, 75.638428, 85.17749, 34.0 ],
					"style" : "",
					"text" : "CPU %"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-483",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2252.366943, 1049.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 140.016327, 241.967529, 51.502899, 34.0 ],
					"style" : "",
					"text" : "OFF"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-481",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2237.366943, 1034.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 56.019318, 241.967529, 51.502899, 34.0 ],
					"style" : "",
					"text" : "OFF"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-479",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2222.366943, 1019.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.213867, 190.111099, 140.266235, 34.0 ],
					"style" : "",
					"text" : "128"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-477",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2207.366943, 1004.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.213867, 166.111099, 140.266235, 34.0 ],
					"style" : "",
					"text" : "512"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-476",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2192.366943, 989.925537, 118.0, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.213867, 138.111099, 140.266235, 34.0 ],
					"style" : "",
					"text" : "44100 48000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-475",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2177.366943, 974.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.302612, 105.638428, 83.17749, 34.0 ],
					"style" : "",
					"text" : "MOTU"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 20.0,
					"id" : "obj-472",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2162.366943, 959.925537, 118.0, 34.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.302612, 75.638428, 83.17749, 34.0 ],
					"style" : "",
					"text" : "MOTU"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-435",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1890.0, 1637.456665, 166.0, 25.0 ],
					"style" : "",
					"text" : "choose a sampling rate"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-442",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1830.0, 1550.0, 195.245117, 25.0 ],
					"style" : "",
					"text" : "report sampling rate choices"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-454",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1902.0, 1609.456665, 55.0, 23.0 ],
					"presentation_rect" : [ 30.0, 30.0, 50.0, 21.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-467",
					"items" : [ 44100, ",", 48000, ",", 88200, ",", 96000 ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1811.0, 1637.456665, 78.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 140.111099, 78.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-468",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1811.0, 1553.0, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-469",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1811.0, 1584.252441, 110.0, 23.0 ],
					"style" : "",
					"text" : "adstatus sr"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-553",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1431.86377, 1287.913574, 29.5, 22.0 ],
					"style" : "",
					"text" : "12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-552",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1510.990112, 297.961182, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0 127 50 105"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-551",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1510.990112, 254.953979, 75.0, 22.0 ],
					"style" : "",
					"text" : "r genki_yaw"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-549",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1321.067749, 420.543945, 31.0, 22.0 ],
					"style" : "",
					"text" : "105"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-550",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1321.067749, 379.543945, 63.0, 22.0 ],
					"style" : "",
					"text" : "delay 200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-547",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1542.25, 330.0, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-548",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 6,
					"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1536.183716, 359.543945, 71.5, 35.0 ],
					"style" : "",
					"text" : "sel 1 12 13 20 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-545",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1461.83252, 287.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-546",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1426.603516, 287.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-543",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1424.733276, 329.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-544",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1440.733276, 365.557373, 34.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-541",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1380.733276, 213.804443, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0 127 70 120"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-542",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1380.733276, 182.953979, 75.0, 22.0 ],
					"style" : "",
					"text" : "r genki_yaw"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-538",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2074.0, 446.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-536",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-534",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-532",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-530",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-528",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-526",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1762.0, 1778.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-519",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1511.33252, 1779.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "sel 98"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-520",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1452.33252, 1779.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "sel 98"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-521",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 1511.33252, 1752.0, 50.5, 22.0 ],
					"style" : "",
					"text" : "keyup"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-522",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 1452.33252, 1752.0, 50.5, 22.0 ],
					"style" : "",
					"text" : "key"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-518",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1594.75, 1614.284546, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-515",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1607.25, 1493.510742, 20.0, 23.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-516",
					"items" : [ "WAVE Bluetooth", ",", "from Max 1", ",", "from Max 2", ",", "to Max 1", ",", "to Max 2", ",", "Dual Harmonizer.amxd" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1571.25, 1557.510742, 123.5, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-517",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1571.25, 1520.510742, 55.0, 23.0 ],
					"style" : "",
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-514",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-512",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-510",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-508",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-506",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-504",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-502",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-500",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-490",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-488",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1445.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-486",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1594.666748, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-484",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1594.666748, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-482",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1594.666748, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-480",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1594.666748, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-478",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1721.333374, 1594.666748, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-425",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1646.670044, 1530.670044, 40.0, 22.0 ],
					"style" : "",
					"text" : "ctlin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 164.763916, 929.333313, 49.0, 22.0 ],
					"style" : "",
					"text" : "*~ 0.25"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-27",
					"linecount" : 7,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2317.342529, 921.782227, 34.0, 147.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 1016.285278, 68.638428, 116.0, 47.0 ],
					"style" : "",
					"text" : "Input Voix\n(ADC2)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-443",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2382.0, 350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-440",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2382.0, 350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-432",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2382.0, 350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-427",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2382.0, 350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-418",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2382.0, 350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-441",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -14.0, 794.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-436",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -14.0, 794.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-433",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1571.25, 1798.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-431",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1469.86377, 1702.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-429",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1482.0, 1714.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-426",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1482.0, 1714.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-423",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1482.0, 1714.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-383",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1088.621948, 1254.307129, 168.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number_repet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-382",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3422.925537, 522.66333, 168.0, 22.0 ],
					"style" : "",
					"text" : "s cue_number_repet"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-373",
					"linecount" : 6,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2332.342529, 936.782227, 34.0, 127.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.970245, 994.638428, 138.0, 27.0 ],
					"style" : "",
					"text" : "Cue Repetition"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-367",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2332.342529, 936.782227, 34.0, 67.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1408.11145, 809.30188, 116.0, 27.0 ],
					"style" : "",
					"text" : "Reverb"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-345",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2437.342529, 1041.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1260.285278, 807.042603, 116.0, 27.0 ],
					"style" : "",
					"text" : "Disto"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-344",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2422.342529, 1026.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 149.0, 811.985596, 116.0, 27.0 ],
					"style" : "",
					"text" : "Evo"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-343",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2407.342529, 1011.782227, 34.0, 67.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1099.592041, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Bubble"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-342",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2392.342529, 996.782227, 34.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 934.592041, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Brookside"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-341",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2377.342529, 981.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 778.876587, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Dirty"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-340",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2362.342529, 966.782227, 34.0, 67.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 623.876587, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Insects"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-337",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2347.342529, 951.782227, 34.0, 67.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 478.876587, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Krystal"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-300",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2332.342529, 936.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 331.127563, 809.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Psy"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-294",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2317.342529, 921.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 809.638428, 60.0, 27.0 ],
					"style" : "",
					"text" : "Delay"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-115",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2302.342529, 906.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 750.625732, 100.71637, 70.0, 27.0 ],
					"style" : "",
					"text" : "Reset"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 40.0,
					"id" : "obj-97",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2162.366943, 959.925537, 311.0, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 996.0, 439.590088, 632.0, 61.0 ],
					"style" : "",
					"text" : "Cue Number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-470",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 151.0, 109.0, 1251.0, 718.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 610.506226, 669.0, 110.0, 22.0 ],
									"style" : "",
									"text" : "r open_genki_gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1152.601196, 839.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1152.601196, 794.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1152.601196, 753.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1152.601196, 709.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1195.980103, 709.174377, 75.0, 22.0 ],
									"style" : "",
									"text" : "r genki_yaw"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1152.601196, 892.920105, 101.0, 22.0 ],
									"style" : "",
									"text" : "s Psycho_Speed"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1777.571533, 735.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1814.0, 735.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1832.7948, 634.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 1832.7948, 669.543945, 69.0, 22.0 ],
									"style" : "",
									"text" : "sel 1 14 15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1590.931641, 844.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1590.931641, 799.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1590.931641, 758.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1590.931641, 714.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1634.310547, 714.174377, 75.0, 22.0 ],
									"style" : "",
									"text" : "r genki_yaw"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1590.931641, 898.920105, 79.0, 22.0 ],
									"style" : "",
									"text" : "s psychorate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 911.2948, 846.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 911.2948, 801.226624, 132.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.48 0.52"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 911.2948, 760.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 911.2948, 716.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 954.673706, 716.174377, 75.0, 22.0 ],
									"style" : "",
									"text" : "r genki_yaw"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 911.2948, 899.920105, 108.0, 22.0 ],
									"style" : "",
									"text" : "s Bubble_Delay_3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1359.601196, 844.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1359.601196, 799.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1359.601196, 758.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1359.601196, 714.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1402.980103, 714.174377, 75.0, 22.0 ],
									"style" : "",
									"text" : "r genki_yaw"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1359.601196, 897.920105, 108.0, 22.0 ],
									"style" : "",
									"text" : "s Bubble_Delay_2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1624.621094, 471.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1624.621094, 426.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1624.621094, 385.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1624.621094, 341.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1668.0, 341.174377, 75.0, 22.0 ],
									"style" : "",
									"text" : "r genki_yaw"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1624.621094, 524.920105, 108.0, 22.0 ],
									"style" : "",
									"text" : "s Bubble_Delay_1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 42.172852, 15.0, 110.0, 22.0 ],
									"style" : "",
									"text" : "r open_genki_gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 42.172852, 306.0, 110.0, 22.0 ],
									"style" : "",
									"text" : "r open_genki_gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 231.0, 634.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 231.0, 669.0, 112.0, 22.0 ],
									"style" : "",
									"text" : "s open_genki_gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 231.0, 598.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1442.396973, 471.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1442.396973, 426.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1442.396973, 385.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1442.396973, 341.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1485.775879, 341.174377, 88.621094, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1442.396973, 524.920105, 137.0, 22.0 ],
									"style" : "",
									"text" : "s Dirty_Whammy"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-200",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1763.071533, 194.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-201",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1672.071533, 194.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-202",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1672.071533, 149.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-203",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1672.071533, 108.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-204",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1672.071533, 63.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-205",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1722.173706, 63.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-206",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1750.37793, 235.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.78265"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-193",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1595.2948, 190.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-194",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1504.2948, 190.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-195",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1504.2948, 145.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-196",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1504.2948, 104.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-197",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1504.2948, 59.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-198",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1554.396973, 59.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-199",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1582.601196, 231.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.69321"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-186",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1440.2948, 185.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-187",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1349.2948, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-188",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1349.2948, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-189",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1349.2948, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-190",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1349.2948, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-191",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1399.396973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-192",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1427.601196, 226.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-179",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1259.2948, 185.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1168.2948, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-181",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1168.2948, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1168.2948, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-183",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1168.2948, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-184",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1218.396973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-185",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1246.601196, 226.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.68344"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1100.2948, 185.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-173",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1009.2948, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-174",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1009.2948, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-175",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1009.2948, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-176",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1009.2948, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-177",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1059.396973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-178",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1087.601196, 226.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.38056"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-165",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 937.2948, 185.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-166",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 846.2948, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-167",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 846.2948, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-168",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 846.2948, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-169",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 846.2948, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 896.396973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-171",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 924.601196, 226.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.5961"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-158",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 772.2948, 185.207275, 44.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 681.2948, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 681.2948, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-161",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 681.2948, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-162",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 681.2948, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-163",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 731.396973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-164",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 759.601196, 226.0, 64.0, 22.0 ],
									"style" : "",
									"text" : "0.33287"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-143",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 559.7948, 185.207275, 100.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-144",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 468.79483, 185.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-145",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 468.79483, 140.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-146",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 468.79483, 99.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-147",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 468.79483, 54.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-148",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 518.896973, 54.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-149",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 547.101196, 226.0, 58.693634, 22.0 ],
									"style" : "",
									"text" : "0.56915"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1668.0, 276.0, 150.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_delay_depth"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1502.0, 276.0, 150.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_delay_feedback"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-142",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1168.979248, 276.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_Presence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1009.2948, 276.0, 98.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_Treble"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 851.7948, 276.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_Middle"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 681.2948, 276.0, 89.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_bass"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 468.79483, 276.0, 137.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_gain"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1346.0, 276.0, 150.0, 22.0 ],
									"style" : "",
									"text" : "s Insects_delay_time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-139",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 244.172852, 276.0, 119.0, 22.0 ],
									"style" : "",
									"text" : "s Krystal_Wha"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 335.172852, 194.207275, 100.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 244.172852, 194.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-135",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 244.172852, 149.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 244.172852, 108.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 244.172852, 63.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 294.275024, 63.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 322.479248, 235.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "0.42"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-131",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 140.172852, 235.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-130",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 140.172852, 203.207275, 100.0, 22.0 ],
									"style" : "",
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-119",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 42.172852, 203.207275, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-120",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 42.172852, 158.047485, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 42.172852, 117.740967, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 42.172852, 72.995239, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-128",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 99.275024, 72.995239, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-129",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 42.172852, 272.120483, 119.0, 22.0 ],
									"style" : "",
									"text" : "s Krystal_Rate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 857.422852, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 854.672852, 475.383911, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 854.672852, 430.224121, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-110",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 854.672852, 389.917603, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-117",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 892.2948, 344.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-98",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 693.2948, 473.586792, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 693.2948, 428.427002, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 693.2948, 388.120483, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 693.2948, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 743.396973, 343.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 528.673706, 473.586792, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 528.673706, 428.427002, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 528.673706, 388.120483, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 528.673706, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 578.775879, 343.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 363.673737, 473.586792, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 363.673737, 428.427002, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 363.673737, 388.120483, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 363.673737, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 413.775909, 343.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.673737, 473.586792, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.673737, 428.427002, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.673737, 388.120483, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 202.673737, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 252.775909, 343.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 44.500031, 473.586792, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 44.500031, 428.427002, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 44.500031, 388.120483, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 44.500031, 343.374756, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 94.602203, 343.374756, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 44.500031, 523.920105, 132.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Vertical"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 202.673737, 523.920105, 132.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Horizontal"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 363.673737, 523.920105, 132.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Amp_Distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 528.673706, 523.920105, 120.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Del_Distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 854.672852, 523.920105, 107.666687, 22.0 ],
									"style" : "",
									"text" : "s delay_Feedback"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 693.2948, 523.920105, 100.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Random"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1059.396973, 477.006897, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1059.396973, 431.847107, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1059.396973, 391.540588, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1059.396973, 346.794861, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1118.499023, 346.794861, 89.0, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1242.063599, 480.386414, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1242.063599, 435.226624, 147.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0.1 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-418",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1059.396973, 545.920105, 119.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Brookside"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-479",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 79.602203, 808.966309, 119.0, 22.0 ],
									"style" : "",
									"text" : "s delay_Brookside"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-478",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 81.102203, 759.280151, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-476",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 81.102203, 724.615723, 100.0, 22.0 ],
									"style" : "",
									"text" : "/ 100."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-477",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 81.102203, 692.615723, 100.0, 22.0 ],
									"style" : "",
									"text" : "+ 25."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-469",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 81.102203, 590.466309, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-468",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 81.102203, 627.977051, 100.0, 22.0 ],
									"style" : "",
									"text" : "metro 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-467",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 81.102203, 662.750854, 100.0, 22.0 ],
									"style" : "",
									"text" : "random 25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-442",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1242.063599, 394.920105, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-433",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1242.063599, 350.174377, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-343",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1285.442505, 350.174377, 88.621094, 22.0 ],
									"style" : "",
									"text" : "r genki_pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-340",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1242.063599, 534.920105, 79.0, 22.0 ],
									"style" : "",
									"text" : "s psychorate"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-110", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-110", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-117", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-110", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-120", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-120", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-123", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-123", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-128", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-130", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-139", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-132", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-133", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-139", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-134", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-135", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-138", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-149", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-143", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-144", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-145", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-145", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-146", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-143", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-146", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-146", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-148", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-149", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-161", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-158", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-163", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-171", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-165", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-166", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-167", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-168", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-165", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-418", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-171", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-173", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-173", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-174", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-174", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-175", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-175", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-175", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-178", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-185", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-179", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-142", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-181", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-181", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-183", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-183", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-184", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-142", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-185", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-192", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-186", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-187", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-187", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-188", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-188", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-189", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-186", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-190", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-189", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-190", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-189", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-191", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-192", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-199", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-193", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-194", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-194", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-195", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-195", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-196", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-197", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-196", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-197", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-196", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-198", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-199", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-206", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-200", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-201", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-201", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-202", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-202", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-203", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-200", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-203", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-203", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-205", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-206", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-433", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-147", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-176", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-183", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-190", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-197", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-204", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-340", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-442", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-343", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-442", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-442", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-477", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-467", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-467", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-468", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-468", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-469", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-478", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-476", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-476", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-477", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-479", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-478", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-63", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-63", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-82", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "PAt_style0",
								"default" : 								{
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
									"clearcolor" : [ 1.0, 0.947758, 0.687073, 1.0 ],
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.952941, 0.564706, 0.098039, 1.0 ],
									"fontname" : [ "Arial" ],
									"bgcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"fontsize" : [ 12.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "grey comment",
								"default" : 								{
									"clearcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"textcolor" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "multislider001",
								"default" : 								{
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
									"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "myteststyle",
								"default" : 								{
									"accentcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-2",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-3",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjCyan-1",
								"default" : 								{
									"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-2",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-3",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-2",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-3",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberB-1",
								"default" : 								{
									"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "panelGold-1",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.764706, 0.592157, 0.101961, 0.25 ],
										"color1" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "textbutton004",
								"default" : 								{
									"selectioncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"centerjust" : [ 0 ],
									"bgcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "default",
								"multi" : 0
							}
, 							{
								"name" : "texteditGold",
								"default" : 								{
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
									"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "umenu001",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color" : [ 0.862745, 0.870588, 0.878431, 0.82 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"fontface" : [ 0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "chiba",
								"multi" : 0
							}
, 							{
								"name" : "umenu002",
								"default" : 								{
									"textjustification" : [ 1 ],
									"textcolor_inverse" : [ 1.0, 1.0, 1.0, 1.0 ],
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.501961, 0.501961, 0.501961, 1.0 ],
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"elementcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"fontsize" : [ 13.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "umenu003",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"fontname" : [ "Inconsolata" ],
									"fontsize" : [ 16.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "umenu004",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"fontname" : [ "Inconsolata" ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "chiba",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1862.0, 912.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 696.0, 348.958252, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p genkiii"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-497",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 651.850952, 1411.0, 20.0, 23.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-498",
					"items" : [ "WAVE Bluetooth", ",", "from Max 1", ",", "from Max 2", ",", "to Max 1", ",", "to Max 2", ",", "Dual Harmonizer.amxd" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 615.850952, 1475.0, 123.5, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-499",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 615.850952, 1438.0, 55.0, 23.0 ],
					"style" : "",
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-493",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 44.975983, 1294.0, 156.75, 25.0 ],
					"style" : "",
					"text" : "click to get list of ports"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-494",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 21.850983, 1297.0, 20.0, 23.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-495",
					"items" : [ "WAVE Bluetooth", ",", "from Max 1", ",", "from Max 2", ",", "to Max 1", ",", "to Max 2", ",", "Dual Harmonizer.amxd" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -14.149017, 1359.0, 123.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.0, 399.0, 123.5, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-496",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -14.149017, 1324.0, 55.0, 23.0 ],
					"style" : "",
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-492",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 0,
					"patching_rect" : [ 2067.90332, 1712.0, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlout"
				}

			}
, 			{
				"box" : 				{
					"activetextcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"activetextoncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"appearance" : 1,
					"automation" : "Arm",
					"automationon" : "Trigger",
					"id" : "obj-463",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 2090.40332, 1590.5, 66.0, 15.0 ],
					"prototypename" : "trigger.default",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "recenter[1]",
							"parameter_shortname" : "recenter",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Arm", "Trigger" ],
							"parameter_speedlim" : 0.0
						}

					}
,
					"text" : "Recenter",
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"texton" : "Trig",
					"varname" : "recenter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-465",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2090.40332, 1622.465454, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-473",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2090.40332, 1662.0, 55.0, 22.0 ],
					"style" : "",
					"text" : "$1 19 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-419",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1522.454468, 1634.0, 50.0, 22.0 ],
					"style" : "",
					"text" : "55"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-417",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 0,
					"patching_rect" : [ 1664.0, 1742.0, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlout 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-416",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 1583.0, 1671.0, 100.0, 35.0 ],
					"style" : "",
					"text" : "makenote 127 127"
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 255.0, 255.0, 255.0, 1.0 ],
					"activeslidercolor" : [ 0.0, 252.0, 0.0, 1.0 ],
					"appearance" : 2,
					"id" : "obj-374",
					"ignoreclick" : 1,
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 660.603027, 1658.510742, 47.0, 15.0 ],
					"prototypename" : "amount",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "battery.life[1]",
							"parameter_shortname" : "battery.life",
							"parameter_type" : 0,
							"parameter_mmax" : 100.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 5,
							"parameter_speedlim" : 0.0
						}

					}
,
					"varname" : "battery.life[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-305",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 673.603027, 1520.0, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 660.603027, 1605.0, 71.0, 36.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 660.603027, 1572.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "== 19"
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 255.0, 255.0, 255.0, 1.0 ],
					"activeslidercolor" : [ 0.0, 252.0, 0.0, 1.0 ],
					"appearance" : 2,
					"id" : "obj-474",
					"ignoreclick" : 1,
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1463.454468, 1586.510742, 47.0, 15.0 ],
					"prototypename" : "amount",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "battery.life[2]",
							"parameter_shortname" : "battery.life",
							"parameter_type" : 0,
							"parameter_mmax" : 100.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 5,
							"parameter_speedlim" : 0.0
						}

					}
,
					"varname" : "battery.life[2]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-471",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1431.86377, 1618.413574, 57.0, 23.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-466",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1431.86377, 1552.0, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 19"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-462",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 255.861511, 1486.484863, 100.0, 22.0 ],
					"style" : "",
					"text" : "s genki_roll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-460",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 142.509705, 1486.484863, 100.0, 22.0 ],
					"style" : "",
					"text" : "s genki_yaw"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-452",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 34.350647, 1486.484863, 100.0, 22.0 ],
					"style" : "",
					"text" : "s genki_pitch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-451",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1149.067749, 401.996826, 57.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"hiderwff" : 1,
					"id" : "obj-420",
					"maxclass" : "playbar",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 85.263916, 1.0, 196.0, 19.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-405",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2037.0, 309.314484, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2026.400024, 273.514496, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1974.400024, 273.514496, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2175.0, 211.057373, 22.0, 235.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2129.0, 211.057373, 22.0, 235.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-316",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 2312.0, 154.057373, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-318",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 2206.0, 154.057373, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-346",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2089.0, 207.057373, 23.0, 239.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[5]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-349",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 2084.0, 154.057373, 100.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-464",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 139.0, 109.0, 1126.0, 681.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-162",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 511.333374, 754.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "0 252 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-161",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 517.833374, 708.5, 32.0, 22.0 ],
									"style" : "",
									"text" : "> 20"
								}

							}
, 							{
								"box" : 								{
									"attr" : "activeslidercolor",
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-8",
									"lock" : 1,
									"maxclass" : "attrui",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 517.833374, 802.0, 150.0, 23.0 ],
									"style" : "",
									"text_width" : 106.0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 585.833374, 754.0, 66.0, 22.0 ],
									"style" : "",
									"text" : "255 0 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 752.333374, 718.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 7 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-158",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 585.833374, 708.5, 40.0, 22.0 ],
									"style" : "",
									"text" : "<= 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-114",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 415.666626, 175.465454, 79.0, 22.0 ],
									"style" : "",
									"text" : "print recenter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-115",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 415.666626, 103.5, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-117",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 415.666626, 46.0, 43.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 235.0, 18.0, 51.0, 15.0 ],
									"prototypename" : "M4L.live.numbox.slider",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "choose.haptic",
											"parameter_shortname" : "choose.haptic",
											"parameter_type" : 0,
											"parameter_mmax" : 123.0,
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"varname" : "choose_haptic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 356.0, 51.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"activetextcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"activetextoncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"appearance" : 1,
									"automation" : "Arm",
									"automationon" : "Trigger",
									"id" : "obj-116",
									"maxclass" : "live.text",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 356.0, 6.0, 66.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 235.0, 0.5, 53.0, 15.0 ],
									"prototypename" : "trigger.default",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "trigger.haptic",
											"parameter_shortname" : "trigger.haptic",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "Arm", "Trigger" ],
											"parameter_speedlim" : 0.0
										}

									}
,
									"text" : "Trigger",
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"texton" : "Trig",
									"varname" : "trigger_haptic"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-199",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 356.0, 103.5, 43.0, 22.0 ],
									"style" : "",
									"text" : "buddy"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-180",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 415.666626, 72.465454, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 356.0, 144.0, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 17 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 624.333374, 477.5, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 79.0, 323.0, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 84.833282, 488.0, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 89.333282, 647.0, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 200.333282, 11.965454, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 80.833282, 130.930908, 67.0, 22.0 ],
									"style" : "",
									"text" : "print haptic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 916.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 801.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 686.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 570.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 455.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 341.833374, 314.0, 36.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-201",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 642.833374, 20.0, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 95.0, 18.0, 47.0, 20.0 ],
									"style" : "",
									"text" : "Battery",
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"activeslidercolor" : [ 0.701961, 0.415686, 0.886275, 1.0 ],
									"appearance" : 2,
									"id" : "obj-196",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 804.666748, 20.0, 43.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 235.0, 56.5, 51.0, 15.0 ],
									"prototypename" : "M4L.live.numbox.slider",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "tilt.value",
											"parameter_shortname" : "tilt.value",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"varname" : "tilt_value"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-195",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 156.666565, 488.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "print enable"
								}

							}
, 							{
								"box" : 								{
									"activeslidercolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ],
									"appearance" : 2,
									"id" : "obj-191",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 913.666748, 20.0, 43.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 235.0, 142.0, 51.0, 15.0 ],
									"prototypename" : "M4L.live.numbox.slider",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "roll.value",
											"parameter_shortname" : "roll.value",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"varname" : "roll_value"
								}

							}
, 							{
								"box" : 								{
									"activeslidercolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
									"appearance" : 2,
									"id" : "obj-183",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 858.166748, 20.0, 43.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 235.0, 98.5, 51.0, 15.0 ],
									"prototypename" : "M4L.live.numbox.slider",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "pan.value",
											"parameter_shortname" : "pan.value",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"varname" : "pan_value"
								}

							}
, 							{
								"box" : 								{
									"automation" : "Normal",
									"automationon" : "Live",
									"id" : "obj-178",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 145.333282, 11.965454, 40.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 31.5, 6.75, 46.0, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "select.mode",
											"parameter_shortname" : "select.mode",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "Normal", "Live" ]
										}

									}
,
									"text" : "Normal",
									"texton" : "Live",
									"varname" : "select_mode"
								}

							}
, 							{
								"box" : 								{
									"autofit" : 1,
									"forceaspect" : 1,
									"id" : "obj-176",
									"maxclass" : "fpic",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "jit_matrix" ],
									"patching_rect" : [ 529.833374, 19.0, 95.0, 95.0 ],
									"pic" : "genki-applogo-01.png",
									"presentation" : 1,
									"presentation_rect" : [ 5.5, 6.75, 19.5, 19.5 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-173",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 15.833282, 136.0, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 145.333282, 84.0, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 393.333374, 708.5, 73.0, 22.0 ],
									"style" : "",
									"text" : "print battery"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-167",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 191.333282, 84.0, 66.0, 22.0 ],
									"style" : "",
									"text" : "print mode"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 967.833374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-153",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 850.833374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-152",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 734.833374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-151",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 614.333374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-150",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 501.333374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-149",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 380.833374, 314.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-142",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 15.833282, 537.5, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-141",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 8.666565, 364.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-131",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.333282, 186.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "r Resend"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-111",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 267.833282, 36.465454, 59.0, 22.0 ],
									"style" : "",
									"text" : "s Resend"
								}

							}
, 							{
								"box" : 								{
									"activetextcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"activetextoncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"appearance" : 1,
									"automation" : "Arm",
									"automationon" : "Resend All",
									"id" : "obj-106",
									"maxclass" : "live.text",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 267.833282, 11.965454, 66.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 160.0, 18.0, 66.0, 15.0 ],
									"prototypename" : "trigger.default",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "resend.data",
											"parameter_shortname" : "resend.data",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "Arm", "Resend All" ],
											"parameter_speedlim" : 0.0
										}

									}
,
									"text" : "Resend",
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"texton" : "Trig",
									"varname" : "resend_data"
								}

							}
, 							{
								"box" : 								{
									"activebgcolor" : [ 255.0, 255.0, 255.0, 1.0 ],
									"activeslidercolor" : [ 0.0, 252.0, 0.0, 1.0 ],
									"appearance" : 2,
									"id" : "obj-93",
									"ignoreclick" : 1,
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 467.333374, 847.0, 47.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 95.0, 0.5, 47.0, 15.0 ],
									"prototypename" : "amount",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_linknames" : 1,
											"parameter_longname" : "battery.life",
											"parameter_shortname" : "battery.life",
											"parameter_type" : 0,
											"parameter_mmax" : 100.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 0 ],
											"parameter_unitstyle" : 5,
											"parameter_speedlim" : 0.0
										}

									}
,
									"varname" : "battery.life"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"justification" : 1,
									"linecolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 642.833374, 98.465454, 313.0, 14.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.0, 126.0, 215.0, 5.0 ],
									"prototypename" : "M4L.live.line.dark.H"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"justification" : 1,
									"linecolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 642.833374, 73.465454, 313.0, 14.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.0, 81.5, 215.0, 5.0 ],
									"prototypename" : "M4L.live.line.dark.H"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"justification" : 1,
									"linecolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 642.833374, 48.465454, 313.0, 14.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.5, 37.75, 215.0, 5.0 ],
									"prototypename" : "M4L.live.line.dark.H"
								}

							}
, 							{
								"box" : 								{
									"activetextcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"activetextoncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"appearance" : 1,
									"automation" : "Arm",
									"automationon" : "Trigger",
									"id" : "obj-94",
									"maxclass" : "live.text",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 15.833282, 12.5, 66.0, 15.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 160.0, 0.5, 66.0, 15.0 ],
									"prototypename" : "trigger.default",
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "recenter",
											"parameter_shortname" : "recenter",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "Arm", "Trigger" ],
											"parameter_speedlim" : 0.0
										}

									}
,
									"text" : "Recenter",
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"texton" : "Trig",
									"varname" : "recenter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 0,
									"patching_rect" : [ 356.0, 175.465454, 40.0, 22.0 ],
									"style" : "",
									"text" : "ctlout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 15.833282, 44.465454, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-60",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 916.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 184.75, 129.5, 40.5, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "roll.max",
											"parameter_shortname" : "roll.max",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "roll_max"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-61",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 801.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 143.0, 129.5, 36.75, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "roll.min",
											"parameter_shortname" : "roll.min",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ -90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "roll_min"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-64",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 686.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 184.75, 87.0, 44.25, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "pan.max",
											"parameter_shortname" : "pan.max",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "pan_max"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-65",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 570.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 143.0, 87.0, 42.75, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "pan.min",
											"parameter_shortname" : "pan.min",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ -90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "pan_min"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-68",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 455.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 184.75, 40.5, 40.5, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "tilt.max",
											"parameter_shortname" : "tilt.max",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "tilt_max"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"id" : "obj-69",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 341.833374, 238.0, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 143.0, 40.5, 33.5, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "tilt.min",
											"parameter_shortname" : "tilt.min",
											"parameter_type" : 0,
											"parameter_mmin" : -90.0,
											"parameter_mmax" : 90.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ -90 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"varname" : "tilt_min"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 916.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 801.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-135",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 686.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 570.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 455.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 341.833374, 286.0, 109.0, 22.0 ],
									"style" : "",
									"text" : "scale -90 90 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 916.833374, 345.0, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 12 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 801.833374, 345.0, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 11 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 686.833374, 345.0, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 10 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 570.833374, 345.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 9 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 455.833374, 345.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 8 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 341.833374, 345.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 7 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 408.083374, 543.5, 70.5, 20.0 ],
									"style" : "",
									"text" : "Battery Life"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 415.333374, 624.0, 71.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 415.333374, 591.0, 47.0, 22.0 ],
									"style" : "",
									"text" : "== 19"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 334.333374, 666.5, 71.0, 33.0 ],
									"style" : "",
									"text" : "Calibration status"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 337.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 337.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 26"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "int" ],
									"patching_rect" : [ 488.333374, 520.0, 50.0, 22.0 ],
									"style" : "newobjGreen-1",
									"text" : "ctlin"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 884.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 884.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 810.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 810.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 24"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 731.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 731.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 23"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 656.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 656.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 22"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 580.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 580.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 21"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "gswitch2",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 501.333374, 624.0, 65.0, 36.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 501.333374, 591.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "== 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-286",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 15.833282, 84.0, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 19 16"
								}

							}
, 							{
								"box" : 								{
									"activebgoncolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ],
									"id" : "obj-263",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 229.833282, 364.0, 64.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.0, 139.5, 64.0, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "roll.enable",
											"parameter_shortname" : "roll.enable",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ]
										}

									}
,
									"text" : "Enable Roll",
									"texton" : "Enable Roll",
									"varname" : "roll_enable"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-265",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 229.833282, 425.5, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 3 16"
								}

							}
, 							{
								"box" : 								{
									"activebgoncolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
									"id" : "obj-267",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 156.333282, 364.0, 64.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.0, 96.0, 64.0, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "pan.enable",
											"parameter_shortname" : "pan.enable",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ]
										}

									}
,
									"text" : "Enable Pan",
									"texton" : "Enable Pan",
									"varname" : "pan_enable"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-269",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 156.333282, 425.5, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 2 16"
								}

							}
, 							{
								"box" : 								{
									"activebgoncolor" : [ 0.701961, 0.415686, 0.886275, 1.0 ],
									"id" : "obj-270",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 84.833282, 364.0, 57.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 12.0, 48.5, 64.0, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "tilt.enable",
											"parameter_shortname" : "tilt.enable",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ]
										}

									}
,
									"text" : "Enable Tilt",
									"texton" : "Enable Tilt",
									"varname" : "tilt_enable"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-273",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 84.833282, 425.5, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 1 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-221",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 224.0, 186.0, 57.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 90.5, 139.5, 35.5, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "roll.invert",
											"parameter_shortname" : "roll.invert",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ],
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 0.0 ]
										}

									}
,
									"text" : "Invert",
									"texton" : "Invert",
									"varname" : "roll_invert"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-223",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 224.0, 237.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 6 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-215",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 154.0, 186.0, 57.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 90.5, 96.0, 35.5, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "pan.invert",
											"parameter_shortname" : "pan.invert",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ]
										}

									}
,
									"text" : "Invert",
									"texton" : "Invert",
									"varname" : "pan_invert"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-218",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 154.0, 237.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 5 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-214",
									"maxclass" : "live.text",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 79.0, 186.0, 57.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 90.5, 48.5, 35.5, 20.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "tilt.invert",
											"parameter_shortname" : "tilt.invert",
											"parameter_type" : 2,
											"parameter_mmax" : 1.0,
											"parameter_enum" : [ "val1", "val2" ],
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 1.0 ]
										}

									}
,
									"text" : "Invert",
									"texton" : "Invert",
									"varname" : "tilt_invert"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-211",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 79.0, 237.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 4 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-204",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.333282, 44.465454, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 18 16"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"focusbordercolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ],
									"id" : "obj-147",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 219.333282, 529.5, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 301.333282, 110.5, 34.0, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "led.blue",
											"parameter_shortname" : "blue",
											"parameter_type" : 0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 50 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ],
									"varname" : "led_blue"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-148",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 219.333282, 595.5, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 15 16"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"focusbordercolor" : [ 0.0, 0.533333, 0.168627, 1.0 ],
									"id" : "obj-143",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 157.333282, 529.5, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 301.333282, 66.0, 34.0, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "led.green",
											"parameter_shortname" : "green",
											"parameter_type" : 0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 50 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
									"varname" : "led_green"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-144",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 157.333282, 595.5, 50.0, 22.0 ],
									"style" : "",
									"text" : "$1 14 6"
								}

							}
, 							{
								"box" : 								{
									"appearance" : 1,
									"focusbordercolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
									"id" : "obj-138",
									"maxclass" : "live.dial",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 89.333282, 529.5, 47.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 301.333282, 22.25, 34.0, 36.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "led.red",
											"parameter_shortname" : "red",
											"parameter_type" : 0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ 50 ],
											"parameter_unitstyle" : 0,
											"parameter_speedlim" : 0.0
										}

									}
,
									"textcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
									"varname" : "led_red"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-140",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 89.333282, 595.5, 55.0, 22.0 ],
									"style" : "",
									"text" : "$1 13 16"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-204", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-117", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-116", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-116", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-117", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-114", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-211", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-218", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-223", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-132", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-133", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-134", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-135", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-140", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-138", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-265", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-141", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-269", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-141", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-273", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-141", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-140", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-142", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-144", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-142", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-148", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-142", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-144", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-143", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-148", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-148", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-149", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-150", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-151", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-152", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-153", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-154", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-161", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-204", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-178", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-199", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-199", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-211", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-211", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-214", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-218", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-215", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-218", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-223", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-221", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-223", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-265", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-263", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-195", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-265", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-265", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-269", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-267", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-195", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-269", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-269", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-273", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-270", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-195", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-273", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-273", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-286", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-158", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-170", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 575.0, 476.833374, 575.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 575.0, 424.833374, 575.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 572.0, 556.833374, 572.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 572.0, 510.833374, 572.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 572.0, 635.833374, 572.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 572.0, 589.833374, 572.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 569.0, 711.833374, 569.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 569.0, 665.833374, 569.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 569.0, 786.833374, 569.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 569.0, 740.833374, 569.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 569.0, 939.833374, 569.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 569.0, 893.833374, 569.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 569.0, 865.833374, 569.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 569.0, 819.833374, 569.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 497.833374, 572.0, 392.833374, 572.0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 513.333374, 572.0, 346.833374, 572.0 ],
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-286", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-199", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-94", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-97", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "PAt_style0",
								"default" : 								{
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
									"clearcolor" : [ 1.0, 0.947758, 0.687073, 1.0 ],
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.952941, 0.564706, 0.098039, 1.0 ],
									"fontname" : [ "Arial" ],
									"bgcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"fontsize" : [ 12.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "grey comment",
								"default" : 								{
									"clearcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"textcolor" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjCyan-1",
								"default" : 								{
									"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "textbutton004",
								"default" : 								{
									"selectioncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"centerjust" : [ 0 ],
									"bgcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "default",
								"multi" : 0
							}
, 							{
								"name" : "umenu001",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color" : [ 0.862745, 0.870588, 0.878431, 0.82 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"fontface" : [ 0 ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "chiba",
								"multi" : 0
							}
, 							{
								"name" : "umenu002",
								"default" : 								{
									"textjustification" : [ 1 ],
									"textcolor_inverse" : [ 1.0, 1.0, 1.0, 1.0 ],
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.501961, 0.501961, 0.501961, 1.0 ],
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"color" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"fontname" : [ "Inconsolata" ],
									"elementcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
									"fontsize" : [ 13.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "umenu003",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"fontname" : [ "Inconsolata" ],
									"fontsize" : [ 16.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "umenu004",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
										"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"fontname" : [ "Inconsolata" ],
									"fontsize" : [ 14.0 ]
								}
,
								"parentstyle" : "chiba",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 646.406494, 1323.011597, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p Genki"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"extract" : 1,
					"id" : "obj-461",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "bp.Compressor.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 269.250977, 260.294525, 339.0, 116.0 ],
					"varname" : "bp.Compressor[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"extract" : 1,
					"id" : "obj-459",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "bp.Compressor.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 269.250977, 260.294525, 339.0, 116.0 ],
					"varname" : "bp.Compressor",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-421",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2193.40332, 1448.303467, 55.0, 23.0 ],
					"presentation_rect" : [ 45.0, 45.0, 50.0, 21.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-422",
					"items" : [ 32, ",", 64, ",", 128, ",", 256, ",", 512, ",", 1024, ",", 2048 ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2125.40332, 1476.303467, 78.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 166.111099, 78.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-428",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2125.40332, 1387.84668, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-444",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 2125.40332, 1423.099121, 87.0, 23.0 ],
					"style" : "",
					"text" : "adstatus iovs"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-445",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2200.40332, 1293.210938, 52.0, 23.0 ],
					"presentation_rect" : [ 45.0, 45.0, 50.0, 21.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-449",
					"items" : [ 1, ",", 2, ",", 4, ",", 8, ",", 16, ",", 32, ",", 64, ",", 128, ",", 256, ",", 512, ",", 1024, ",", 2048, ",", 4096 ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2126.40332, 1322.210938, 78.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 190.111099, 78.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-450",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2126.40332, 1237.80249, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-453",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 2126.40332, 1268.006592, 93.0, 23.0 ],
					"style" : "",
					"text" : "adstatus sigvs"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1590.490112, 1312.617676, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1491.490112, 1256.161133, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"items" : [ "Off", ",", "On" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1491.490112, 1340.617676, 60.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 217.967529, 60.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-295",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1491.490112, 1287.413574, 118.0, 23.0 ],
					"style" : "",
					"text" : "adstatus overdrive"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-298",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1585.454468, 1457.484863, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-302",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1490.454468, 1402.076172, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-313",
					"items" : [ "Off", ",", "On" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1490.454468, 1486.484863, 61.035553, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 135.25, 217.967529, 61.035553, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-314",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1490.454468, 1432.280518, 114.0, 23.0 ],
					"style" : "",
					"text" : "adstatus takeover"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2108.172363, 65.768066, 87.0, 22.0 ],
					"style" : "",
					"text" : "100, 40 25000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2001.453613, 95.882416, 43.0, 22.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1714.078613, 116.001068, 31.0, 22.0 ],
					"style" : "",
					"text" : "100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1864.51416, 37.768066, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1864.51416, 73.312012, 53.0, 22.0 ],
					"style" : "",
					"text" : "sel 1 22"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-448",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2317.342529, 921.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 321.92981, 363.666626, 54.0, 27.0 ],
					"style" : "",
					"text" : "Roll"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-447",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2302.342529, 906.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 249.730103, 363.666626, 58.0, 27.0 ],
					"style" : "",
					"text" : "Yaw"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-446",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2287.342529, 891.782227, 34.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 183.0, 363.666626, 70.0, 27.0 ],
					"style" : "",
					"text" : "Pitch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-409",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 735.656494, 1733.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-410",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 625.33728, 1733.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-411",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 735.656494, 1702.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-412",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 625.33728, 1702.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-413",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 731.406494, 1777.5, 102.0, 22.0 ],
					"style" : "",
					"text" : "s stop_calibrateZ"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-415",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 621.08728, 1777.5, 103.0, 22.0 ],
					"style" : "",
					"text" : "s start_calibrateZ"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-394",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 483.796021, 1724.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-395",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 373.476868, 1724.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-400",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 483.796021, 1693.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-406",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 373.476868, 1693.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-407",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 479.546021, 1768.5, 103.0, 22.0 ],
					"style" : "",
					"text" : "s stop_calibrateY"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-408",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 369.226868, 1768.5, 103.0, 22.0 ],
					"style" : "",
					"text" : "s start_calibrateY"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-365",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 225.84726, 1758.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-366",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 115.528107, 1758.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-368",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 225.84726, 1727.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-393",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 115.528107, 1727.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-359",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 221.59726, 1802.5, 103.0, 22.0 ],
					"style" : "",
					"text" : "s stop_calibrateX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-360",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 111.278107, 1802.5, 103.0, 22.0 ],
					"style" : "",
					"text" : "s start_calibrateX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-293",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 429.97467, 224.011566, 167.0, 22.0 ],
					"style" : "",
					"text" : "udpsend 192.168.1.108 9000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 439.602173, 200.796021, 167.0, 22.0 ],
					"style" : "",
					"text" : "udpsend 192.168.1.108 9000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-288",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "list", "list" ],
					"patching_rect" : [ 520.157471, 41.647766, 143.0, 22.0 ],
					"style" : "",
					"text" : "OSC-route /InputCasque"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-291",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 520.157471, 12.827576, 99.0, 22.0 ],
					"style" : "",
					"text" : "udpreceive 8000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "list", "list" ],
					"patching_rect" : [ 389.250977, 41.647766, 120.0, 22.0 ],
					"style" : "",
					"text" : "OSC-route /InputMic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 389.250977, 12.827576, 99.0, 22.0 ],
					"style" : "",
					"text" : "udpreceive 8000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-109",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4355.577148, 157.067139, 79.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-211",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 4355.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-287",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4355.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 23 24 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 866.160461, 1243.312866, 50.5, 22.0 ],
					"style" : "",
					"text" : "key"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 866.160461, 1282.563843, 43.0, 22.0 ],
					"style" : "",
					"text" : "sel 32"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 731.406494, 334.852417, 57.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1741.578613, 269.700287, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1149.067749, 138.543945, 31.0, 22.0 ],
					"style" : "",
					"text" : "105"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1108.067749, 108.800415, 63.0, 22.0 ],
					"style" : "",
					"text" : "delay 200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-392",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 191.969818, 569.239807, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-391",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 160.469818, 569.239807, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-390",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 160.469818, 602.239807, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-388",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 160.469818, 503.739807, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-389",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 160.469818, 532.783752, 69.0, 22.0 ],
					"style" : "",
					"text" : "sel 0 14 15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-387",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 160.469818, 631.239807, 41.0, 22.0 ],
					"style" : "",
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-249",
					"lockeddragscroll" : 0,
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "" ],
					"patching_rect" : [ 278.208679, 391.239807, 448.0, 196.0 ],
					"presentation_rect" : [ 0.0, 0.0, 448.0, 196.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "amxd~",
							"parameter_shortname" : "amxd~",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"annotation_name" : "",
						"parameter_enable" : 1,
						"patchername" : "Dual Harmonizer.amxd",
						"patchername_fallback" : "C74:/packages/Max for Live/patchers/Max Audio Effect/Dual Harmonizer.amxd"
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "max~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"name" : "Dual Harmonizer.amxd",
							"origname" : "C74:/packages/Max for Live/patchers/Max Audio Effect/Dual Harmonizer.amxd",
							"valuedictionary" : 							{
								"parameter_values" : 								{
									"Dry/Wet" : 100.0,
									"EchoEnable" : 0.0,
									"EchoFeedback" : 56.622711,
									"EchoMode" : 0.0,
									"EchoMultL" : 0.0,
									"EchoMultR" : 0.0,
									"EchoSyncL" : 10.0,
									"EchoSyncR" : 10.0,
									"EchoTimeL" : 0.0,
									"EchoTimeR" : 0.0,
									"Gain" : 16.96063,
									"Glide" : 0.0,
									"Latency" : 1.0,
									"Quality" : 1.0,
									"TranspL" : -45.0,
									"TranspR" : 75.0,
									"VibDepth" : 25.0,
									"VibDirection" : 0.0,
									"VibEnable" : 0.0,
									"VibNoiseAmount" : 50.0,
									"VibNoiseEnable" : 0.0,
									"VibOscAmount" : 50.0,
									"VibOscEnable" : 1.0,
									"VibRate" : 0.0
								}

							}

						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Dual Harmonizer.amxd",
									"origin" : "Dual Harmonizer.amxd",
									"type" : "amxd",
									"subtype" : "Undefined",
									"embed" : 0,
									"snapshot" : 									{
										"name" : "Dual Harmonizer.amxd",
										"origname" : "C74:/packages/Max for Live/patchers/Max Audio Effect/Dual Harmonizer.amxd",
										"valuedictionary" : 										{
											"parameter_values" : 											{
												"Dry/Wet" : 100.0,
												"EchoEnable" : 0.0,
												"EchoFeedback" : 56.622711,
												"EchoMode" : 0.0,
												"EchoMultL" : 0.0,
												"EchoMultR" : 0.0,
												"EchoSyncL" : 10.0,
												"EchoSyncR" : 10.0,
												"EchoTimeL" : 0.0,
												"EchoTimeR" : 0.0,
												"Gain" : 16.96063,
												"Glide" : 0.0,
												"Latency" : 1.0,
												"Quality" : 1.0,
												"TranspL" : -45.0,
												"TranspR" : 75.0,
												"VibDepth" : 25.0,
												"VibDirection" : 0.0,
												"VibEnable" : 0.0,
												"VibNoiseAmount" : 50.0,
												"VibNoiseEnable" : 0.0,
												"VibOscAmount" : 50.0,
												"VibOscEnable" : 1.0,
												"VibRate" : 0.0
											}

										}

									}
,
									"fileref" : 									{
										"name" : "Dual Harmonizer.amxd",
										"filename" : "Dual Harmonizer.amxd.maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "2b1fca860c8addba61699958738f2a5d"
									}

								}
, 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Dual Harmonizer.amxd",
									"origin" : "Dual Harmonizer.amxd",
									"type" : "amxd",
									"subtype" : "Undefined",
									"embed" : 0,
									"fileref" : 									{
										"name" : "Dual Harmonizer.amxd",
										"filename" : "Dual Harmonizer.amxd_20170210.maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "e1434b1eb0a5ff9e10ae38358162a409"
									}

								}
, 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Dual Harmonizer.amxd[1]",
									"origin" : "Dual Harmonizer.amxd",
									"type" : "amxd",
									"subtype" : "Undefined",
									"embed" : 0,
									"fileref" : 									{
										"name" : "Dual Harmonizer.amxd[1]",
										"filename" : "Dual Harmonizer.amxd[1].maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "9fd1a47e7c692ce2509d6e056c933aaa"
									}

								}
, 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Dual Harmonizer.amxd[1]",
									"origin" : "Dual Harmonizer.amxd",
									"type" : "amxd",
									"subtype" : "Undefined",
									"embed" : 0,
									"fileref" : 									{
										"name" : "Dual Harmonizer.amxd[1]",
										"filename" : "Dual Harmonizer.amxd[1].maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "d0c7f5274ec3195a731b370d055a4d62"
									}

								}
, 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Dual Harmonizer.amxd",
									"origin" : "Dual Harmonizer.amxd",
									"type" : "amxd",
									"subtype" : "Undefined",
									"embed" : 0,
									"fileref" : 									{
										"name" : "Dual Harmonizer.amxd",
										"filename" : "Dual Harmonizer.amxd_20170210.maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "b39555e014386f456ca2fdac81974f4d"
									}

								}
 ]
						}

					}
,
					"style" : "",
					"text" : "amxd~ \"Dual Harmonizer.amxd\"",
					"varname" : "amxd~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-306",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1523.33252, 19.528168, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-307",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1591.59021, 155.514496, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-312",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1591.59021, 117.117767, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 105. 40."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-348",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1555.242676, 80.905365, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-369",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1523.742676, 80.905365, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-372",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1523.33252, 49.403015, 69.0, 22.0 ],
					"style" : "",
					"text" : "sel 0 13 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-375",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1523.742676, 108.5289, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-385",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1523.33252, 162.233002, 34.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-386",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1591.59021, 80.905365, 79.0, 22.0 ],
					"style" : "",
					"text" : "r genki_pitch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1155.134033, 25.0, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-154",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2317.342529, 921.782227, 34.0, 107.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1439.203369, 81.227539, 116.0, 27.0 ],
					"style" : "",
					"text" : "Main OUT34"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-128",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2302.342529, 906.782227, 34.0, 107.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1304.203369, 81.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Main OUT12"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-81",
					"linecount" : 8,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2302.342529, 906.782227, 34.0, 167.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 858.592041, 75.638428, 174.0, 47.0 ],
					"style" : "",
					"text" : "Input Harpe \n(ADC1)"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-54",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2287.342529, 891.782227, 34.0, 107.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1153.203369, 81.638428, 116.0, 27.0 ],
					"style" : "",
					"text" : "Harpe Direct"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 475.83728, 1600.0, 96.0, 22.0 ],
					"style" : "",
					"text" : "s genki_number"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Avenir Book",
					"fontsize" : 40.0,
					"id" : "obj-404",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2121.076172, 930.143555, 306.0, 170.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 851.552612, 9.967529, 639.0, 61.0 ],
					"style" : "",
					"text" : "3. Audio On-Check Input-Reset"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-403",
					"linecount" : 7,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2272.342529, 828.132812, 34.0, 147.0 ],
					"style" : "",
					"text" : "4. Stop calibration",
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-402",
					"linecount" : 9,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2281.919678, 836.782227, 29.0, 187.0 ],
					"style" : "",
					"text" : "3. Start Calibration",
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-401",
					"linecount" : 10,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2248.342529, 952.842529, 29.0, 208.0 ],
					"style" : "",
					"text" : "2. Reset_Calibration",
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-399",
					"linecount" : 9,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2287.342529, 844.632812, 29.0, 187.0 ],
					"style" : "",
					"text" : "1. Main Calibration",
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Times New Roman",
					"fontsize" : 48.0,
					"id" : "obj-398",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2159.20874, 1007.632812, 322.0, 113.0 ],
					"style" : "",
					"text" : "3. Calibrate GENKI",
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 40.0,
					"id" : "obj-397",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2147.366943, 944.925537, 311.0, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 83.749039, 281.458252, 220.0, 61.0 ],
					"style" : "",
					"text" : "2. GENKI"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Avenir Black",
					"fontsize" : 40.0,
					"id" : "obj-396",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2127.919678, 930.143555, 593.0, 116.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.0, 20.967529, 788.0, 61.0 ],
					"style" : "",
					"text" : "1. Choose Soundcard, check pedal, reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-384",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 356.898926, 1500.610229, 101.0, 22.0 ],
					"style" : "",
					"text" : "r genki_receiving"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-380",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 475.83728, 1525.702515, 71.0, 22.0 ],
					"style" : "",
					"text" : "symbol #1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-381",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 475.83728, 1500.827515, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-379",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1767.510864, 1253.406006, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-376",
					"items" : "Built-in Output",
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1942.138062, 1338.629883, 156.667542, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 111.638428, 156.667542, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-377",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1942.138062, 1287.828857, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-378",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1942.138062, 1313.229492, 110.0, 23.0 ],
					"style" : "",
					"text" : "adstatus option 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-361",
					"items" : "Built-in Microphone",
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1767.510864, 1338.629883, 156.667542, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 81.638428, 156.667542, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-363",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1767.510864, 1287.828857, 20.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-371",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1767.510864, 1313.229492, 110.0, 23.0 ],
					"style" : "",
					"text" : "adstatus option 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3324.0, 632.124512, 35.0, 22.0 ],
					"style" : "",
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 231.224854, 1568.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 120.905701, 1568.371948, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 231.224854, 1637.36499, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 231.224854, 1678.149414, 99.0, 22.0 ],
					"style" : "",
					"text" : "s reset_calibrate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 120.905701, 1637.36499, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 120.905701, 1678.149414, 98.0, 22.0 ],
					"style" : "",
					"text" : "s main_calibrate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-370",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 2531.094238, 706.871582, 58.0, 22.0 ],
					"restore" : 					{
						"amxd~" : [ 							{
								"filetype" : "C74Snapshot",
								"version" : 2,
								"minorversion" : 0,
								"name" : "Dual Harmonizer.amxd",
								"origin" : "Dual Harmonizer.amxd",
								"type" : "amxd",
								"subtype" : "Undefined",
								"embed" : 1,
								"snapshot" : 								{
									"name" : "Dual Harmonizer.amxd",
									"origname" : "C74:/packages/Max for Live/patchers/Max Audio Effect/Dual Harmonizer.amxd",
									"valuedictionary" : 									{
										"parameter_values" : 										{
											"Dry/Wet" : 100.0,
											"EchoEnable" : 0.0,
											"EchoFeedback" : 56.622711,
											"EchoMode" : 0.0,
											"EchoMultL" : 0.0,
											"EchoMultR" : 0.0,
											"EchoSyncL" : 10.0,
											"EchoSyncR" : 10.0,
											"EchoTimeL" : 0.0,
											"EchoTimeR" : 0.0,
											"Gain" : 16.96063,
											"Glide" : 0.0,
											"Latency" : 1.0,
											"Quality" : 1.0,
											"TranspL" : -45.0,
											"TranspR" : 75.0,
											"VibDepth" : 25.0,
											"VibDirection" : 0.0,
											"VibEnable" : 0.0,
											"VibNoiseAmount" : 50.0,
											"VibNoiseEnable" : 0.0,
											"VibOscAmount" : 50.0,
											"VibOscEnable" : 1.0,
											"VibRate" : 0.0
										}

									}

								}

							}
 ],
						"battery.life[1]" : [ 15.0 ],
						"battery.life[2]" : [ 0.0 ],
						"gain~[3]" : [ 70 ],
						"gain~[4]" : [ 62 ],
						"gain~[5]" : [ 15 ],
						"gain~[BrookeL]" : [ 0 ],
						"gain~[BrookeR]" : [ 0 ],
						"gain~[BubbleL]" : [ 0 ],
						"gain~[BubbleR]" : [ 0 ],
						"gain~[Contact]" : [ 100 ],
						"gain~[DelayL]" : [ 0 ],
						"gain~[DelayR]" : [ 0 ],
						"gain~[DirectL]" : [ 33 ],
						"gain~[DirectR]" : [ 33 ],
						"gain~[DirtyL]" : [ 0 ],
						"gain~[DirtyR]" : [ 0 ],
						"gain~[DistoL]" : [ 0 ],
						"gain~[DistoR]" : [ 0 ],
						"gain~[EvoL]" : [ 0 ],
						"gain~[EvoR]" : [ 0 ],
						"gain~[HP3]" : [ 0 ],
						"gain~[HP4]" : [ 0 ],
						"gain~[InsectsL]" : [ 0 ],
						"gain~[InsectsR]" : [ 0 ],
						"gain~[KrystalL]" : [ 0 ],
						"gain~[KrystalR]" : [ 0 ],
						"gain~[MainL]" : [ 78 ],
						"gain~[MainR]" : [ 78 ],
						"gain~[PsyL]" : [ 0 ],
						"gain~[PsyR]" : [ 0 ],
						"gain~[ReverbL]" : [ 0 ],
						"gain~[ReverbR]" : [ 0 ],
						"gain~[Voix]" : [ 0 ],
						"live.gain~[Poeme]" : [ -27.50543 ],
						"livegain~[Harpe1]" : [ 0.721661 ],
						"livegain~[Harpe2]" : [ -70.0 ],
						"recenter" : [ 0.0 ]
					}
,
					"style" : "",
					"text" : "autopattr",
					"varname" : "u769001220"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 195.486938, 1253.665039, 31.303345, 20.0 ],
					"style" : "",
					"text" : "roll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-244",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 115.890579, 1253.665039, 39.621994, 20.0 ],
					"style" : "",
					"text" : "X"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-243",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 276.375641, 1253.665039, 31.303345, 20.0 ],
					"style" : "",
					"text" : "yaw"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.163647, 0.174699, 0.17409, 1.0 ],
					"id" : "obj-174",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 276.375641, 1328.172607, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 330.92981, 386.166626, 36.0, 294.0 ],
					"setminmax" : [ 0.0, 127.0 ],
					"setstyle" : 1,
					"slidercolor" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.163647, 0.174699, 0.17409, 1.0 ],
					"id" : "obj-221",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 195.486938, 1328.172607, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 259.980103, 386.166626, 37.5, 294.0 ],
					"setminmax" : [ 0.0, 127.0 ],
					"setstyle" : 1,
					"slidercolor" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.163647, 0.174699, 0.17409, 1.0 ],
					"id" : "obj-241",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 111.350983, 1328.172607, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 197.932861, 386.166626, 36.0, 294.0 ],
					"setminmax" : [ 0.0, 127.0 ],
					"setstyle" : 1,
					"slidercolor" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 356.898926, 1578.510742, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-362",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 356.898926, 1614.284546, 82.0, 22.0 ],
					"style" : "",
					"text" : "s reset_genki"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"id" : "obj-336",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 906.224915, 1778.343994, 102.0, 19.0 ],
					"style" : "",
					"text" : "prepend /Cue_Number"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"id" : "obj-358",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 906.224915, 1810.600708, 128.0, 19.0 ],
					"style" : "",
					"text" : "udpsend 192.168.1.101 9000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-350",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1620.082031, 1139.823364, 126.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_KrystalR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-351",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1620.082031, 1115.823364, 124.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_KrystalL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-352",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1620.082031, 924.376953, 96.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DistoR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-353",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1620.082031, 900.376953, 94.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DistoL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-354",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1688.082031, 965.31665, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1328.285278, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-355",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1666.082031, 965.31665, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1306.285278, 840.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DistoR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-356",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1642.082031, 965.31665, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1282.285278, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-357",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1620.082031, 965.31665, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1260.285278, 840.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DistoL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-347",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 994.305969, 1375.971924, 84.0, 22.0 ],
					"style" : "",
					"text" : "s genki_pulse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-339",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 451.796021, 1364.642578, 29.5, 22.0 ],
					"style" : "",
					"text" : "60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-338",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 497.928345, 1364.642578, 31.0, 22.0 ],
					"style" : "",
					"text" : "120"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-335",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 359.200897, 1432.642578, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-334",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 444.337311, 1440.809082, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-333",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 528.33728, 1284.883545, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-332",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 438.337311, 1284.883545, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-331",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 361.704956, 1284.883545, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-330",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 231.224854, 1537.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-329",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 120.905701, 1537.697998, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-322",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 451.796021, 1400.011597, 91.0, 22.0 ],
					"style" : "",
					"text" : "s tempo_metro"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-323",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 444.337311, 1469.011597, 80.0, 22.0 ],
					"style" : "",
					"text" : "s stop_metro"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-324",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 359.200897, 1469.011597, 81.0, 22.0 ],
					"style" : "",
					"text" : "s start_metro"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-325",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 361.476868, 1400.011597, 84.0, 22.0 ],
					"style" : "",
					"text" : "s genki_pulse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-326",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 528.33728, 1323.011597, 78.0, 22.0 ],
					"style" : "",
					"text" : "s genki_blue"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-327",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 438.337311, 1323.011597, 86.0, 22.0 ],
					"style" : "",
					"text" : "s genki_green"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-328",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 361.704956, 1323.011597, 73.0, 22.0 ],
					"style" : "",
					"text" : "s genki_red"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-321",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 231.224854, 1598.5, 95.0, 22.0 ],
					"style" : "",
					"text" : "s stop_calibrate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-320",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 120.905701, 1598.5, 95.0, 22.0 ],
					"style" : "",
					"text" : "s start_calibrate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-319",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 26.850647, 1281.005859, 72.0, 22.0 ],
					"style" : "",
					"text" : "r genki_acc"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-317",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 276.375641, 1281.005859, 76.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-315",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 195.486938, 1281.005859, 75.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 111.350983, 1281.005859, 79.0, 22.0 ],
					"style" : "",
					"text" : "ctlin 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1795.51416, 321.961182, 57.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1864.51416, 155.514496, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1514.16748, 110.638428, 23.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1842.51416, 155.514496, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1492.16748, 110.638428, 26.0, 298.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[HP4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-308",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1817.51416, 155.514496, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1467.16748, 110.638428, 23.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-311",
					"interp" : 0.0,
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1795.51416, 155.514496, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1445.16748, 110.638428, 26.0, 298.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[HP3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1888.806396, 116.001068, 83.0, 22.0 ],
					"style" : "",
					"text" : "receive~ HP4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1795.51416, 116.001068, 83.0, 22.0 ],
					"style" : "",
					"text" : "receive~ HP3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-310",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1030.422119, 103.468628, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-309",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1066.262939, 43.859497, 45.0, 22.0 ],
					"style" : "",
					"text" : "r reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-246",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1218.067749, 221.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1382.01416, 110.638428, 23.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1196.067749, 221.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1360.01416, 110.638428, 26.0, 298.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[MainR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1171.067749, 221.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1335.01416, 110.638428, 23.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"interp" : 50.0,
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1149.067749, 221.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1307.01416, 111.638428, 26.0, 298.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[MainL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-301",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1259.026733, 186.871582, 111.0, 22.0 ],
					"style" : "",
					"text" : "receive~ GeneralR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-303",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1149.067749, 186.871582, 109.0, 22.0 ],
					"style" : "",
					"text" : "receive~ GeneralL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3845.576904, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3776.576904, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1499.10791, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-299",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 969.262939, 98.468628, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 969.262939, 60.135376, 43.0, 22.0 ],
					"style" : "",
					"text" : "sel 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 969.262939, 129.411255, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-304",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1057.54187, 1313.836548, 29.5, 22.0 ],
					"style" : "",
					"text" : "23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 104.509705, 846.810242, 138.0, 22.0 ],
					"style" : "",
					"text" : "receive~ mic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-297",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 63.763916, 816.143555, 136.0, 22.0 ],
					"style" : "",
					"text" : "receive~ mic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 296.292175, 92.468628, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 946.054321, 118.638428, 38.534241, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-290",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 269.250977, 92.468628, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 914.03186, 118.638428, 41.534241, 298.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[Contact]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 324.666626, 38.011566, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 263.792175, 41.647766, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-279",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4242.577148, 157.067139, 78.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 45000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-281",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 4242.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-282",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4242.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 22 23 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-275",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4125.577148, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 1000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-277",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 4125.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-278",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4125.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 21 22 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-272",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4002.577148, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-273",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 4002.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-274",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4002.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 20 21 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-269",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3893.577148, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-270",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3893.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-271",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3893.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 19 20 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-266",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3776.576904, 157.067139, 65.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3776.576904, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-268",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3776.576904, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 18 19 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-263",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3662.577148, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 7000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-264",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3662.577148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-265",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3662.577148, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 17 18 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3724.364258, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1446.895264, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-262",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3776.576904, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3659.364258, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1381.895264, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3694.576904, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3592.364258, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1314.895264, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-258",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3627.576904, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3519.364258, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1241.895264, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-256",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3559.577148, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3452.364258, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1174.895264, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "19"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3486.577148, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 19"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3387.65332, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1110.184326, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3405.576904, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 969.262939, 28.859497, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-252",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3555.866211, 157.067139, 79.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 15000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-253",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3555.866211, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-254",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3555.866211, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 16 17 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-238",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3436.866211, 157.067139, 65.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3436.866211, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-251",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3436.866211, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 15 16 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3348.0, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1070.531006, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3336.0, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3274.468994, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 997.0, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3265.540039, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 25,
					"numoutlets" : 25,
					"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 2616.366455, 30.078705, 365.0, 22.0 ],
					"style" : "",
					"text" : "sel 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2616.366455, 6.067139, 85.0, 22.0 ],
					"style" : "",
					"text" : "r cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 859.103027, 29.796021, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 859.103027, 347.744507, 84.0, 22.0 ],
					"style" : "",
					"text" : "send~ poeme"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 54.763916, 631.239807, 96.0, 22.0 ],
					"style" : "",
					"text" : "receive~ poeme"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 859.103027, 297.961182, 92.0, 22.0 ],
					"style" : "",
					"text" : "send~ poemeR"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 859.103027, 321.961182, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ poemeL"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 12.0,
					"id" : "obj-208",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 859.103027, 143.796021, 54.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[Poeme]",
							"parameter_shortname" : "live.gain~[Poeme]",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ -70 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~[Poeme]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "bang" ],
					"patching_rect" : [ 859.103027, 108.800415, 63.0, 23.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"mode" : "basic",
						"originallength" : [ 50880.0, "ticks" ],
						"originaltempo" : 120.0,
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"style" : "",
					"text" : "sfplay~ 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-210",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 859.103027, 65.796021, 101.0, 22.0 ],
					"style" : "",
					"text" : "open Poeme2.aif"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-181",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3329.991211, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 2000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-196",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3329.991211, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-197",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3329.991211, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 14 15 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-177",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3210.991211, 157.067139, 72.0, 38.0 ],
					"style" : "",
					"text" : "0., 1. 20000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3210.991211, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-180",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3210.991211, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 13 14 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-170",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3105.241211, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 1000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-171",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3105.241211, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-164",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2986.077148, 157.067139, 79.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 20000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2986.077148, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-162",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3105.241211, 266.067139, 96.0, 23.0 ],
					"style" : "",
					"text" : "recall 12 13 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-161",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2986.077148, 266.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "recall 11 12 $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-156",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2879.577148, 157.067139, 72.0, 23.0 ],
					"style" : "",
					"text" : "0., 1. 6000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-157",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2881.834717, 198.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-159",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2881.834717, 231.078705, 60.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-148",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2870.366455, 266.067139, 95.0, 23.0 ],
					"style" : "",
					"text" : "recall 10 11 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 253.59726, 1117.853027, 116.0, 22.0 ],
					"style" : "",
					"text" : "send~ verb_harpeR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 244.034271, 1141.333252, 114.0, 22.0 ],
					"style" : "",
					"text" : "send~ verb_harpeL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 63.763916, 1117.853027, 120.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_harpeR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 63.763916, 1141.333252, 118.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_harpeL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 176.251801, 11.827576, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 12.0,
					"id" : "obj-78",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 75.763916, 100.5, 54.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[Harpe2]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ -70 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "livegain~[Harpe2]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "bang" ],
					"patching_rect" : [ 111.221619, 57.827576, 89.0, 23.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"mode" : "basic",
						"originallength" : [ 549120.0, "ticks" ],
						"originaltempo" : 120.0,
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"style" : "",
					"text" : "sfplay~ 2"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 64.0,
					"id" : "obj-75",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2398.268066, 469.944397, 535.0, 78.0 ],
					"style" : "",
					"text" : "WRITE!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1287.203857, 124.804443, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0 127 50 105"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1249.296753, 100.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1182.567749, 100.804443, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1149.067749, 54.543945, 69.0, 22.0 ],
					"style" : "",
					"text" : "sel 1 10 12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1214.067749, 133.394409, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1272.203857, 162.233002, 34.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2282.705566, 248.067139, 45.0, 22.0 ],
					"style" : "",
					"text" : "r reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 246.0, 227.0, 972.0, 632.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 280.0, 8.0, 150.0, 20.0 ],
									"style" : "",
									"text" : "Press T to change pitch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 665.5, 235.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 2092.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 104.5, 35.0, 50.5, 22.0 ],
									"style" : "",
									"text" : "sel 116"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 4,
									"outlettype" : [ "int", "int", "int", "int" ],
									"patching_rect" : [ 104.5, 4.0, 50.5, 22.0 ],
									"style" : "",
									"text" : "key"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 350.0, 46.0, 47.0, 22.0 ],
									"style" : "",
									"text" : "r pedal"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 835.0, 242.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 2158.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 828.5, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 2027.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 741.5, 184.0, 75.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1962"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 654.5, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1896.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 564.5, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1831.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 478.5, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1765.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 388.5, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1700.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 309.0, 184.0, 75.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1635"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 218.0, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1569.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 130.0, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1504.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 56.0, 184.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1438.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 828.5, 142.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1373.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 746.888916, 142.0, 75.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1308"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 655.277771, 142.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1242.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 564.666687, 142.0, 84.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1177.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 476.055542, 142.0, 82.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1111.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 384.444458, 142.0, 85.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 1046.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 309.833344, 142.0, 68.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 981"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 225.222229, 142.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 915.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 140.611115, 142.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 850.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 56.0, 142.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 784.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 828.5, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 719.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 749.777771, 104.0, 68.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 654"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 661.055542, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 588.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 572.333313, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 523.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 252.0, 49.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 184.0, 23.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.0, 273.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 483.611115, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 457.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 394.888885, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 392.4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 33,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 104.5, 301.0, 355.0, 22.0 ],
									"style" : "",
									"text" : "selector~ 32"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 64.0, 45.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 64.0, 76.0, 77.0, 22.0 ],
									"style" : "",
									"text" : "counter 1 32"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 316.166656, 104.0, 68.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 327"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 227.444443, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 261.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 138.722229, 104.0, 78.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 130.8"
								}

							}
, 							{
								"box" : 								{
									"channels" : 1,
									"fontsize" : 13.0,
									"id" : "obj-27",
									"maxclass" : "live.gain~",
									"numinlets" : 1,
									"numoutlets" : 4,
									"orientation" : 1,
									"outlettype" : [ "signal", "", "float", "list" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 79.0, 381.0, 136.0, 35.0 ],
									"presentation_rect" : [ 0.0, 0.0, 50.0, 35.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "live.gain~",
											"parameter_shortname" : "live.gain~",
											"parameter_type" : 0,
											"parameter_mmin" : -70.0,
											"parameter_mmax" : 6.0,
											"parameter_initial_enable" : 1,
											"parameter_initial" : [ -70 ],
											"parameter_unitstyle" : 4
										}

									}
,
									"showname" : 0,
									"varname" : "live.gain~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 100.0, 444.0, 37.0, 22.0 ],
									"style" : "",
									"text" : "dac~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 56.0, 104.0, 72.0, 22.0 ],
									"style" : "",
									"text" : "cycle~ 65.4"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 5 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 6 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 7 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 8 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 9 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 10 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 20 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 19 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 18 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 17 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 16 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 15 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 14 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 13 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 12 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 11 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 30 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 29 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 28 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 27 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 26 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 25 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 24 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 23 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 22 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 21 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 32 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 4 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 31 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1865.781738, 1050.17395, 77.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 710.876587, 294.458252, 89.499146, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p accordage"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1287.203857, 93.953979, 79.0, 22.0 ],
					"style" : "",
					"text" : "r genki_pitch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1437.36377, 1810.600708, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 314.826843, 118.111099, 90.099487, 90.099487 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1004.868469, 1282.563843, 45.0, 22.0 ],
					"style" : "",
					"text" : "r reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-439",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1258.997681, 1340.094849, 56.0, 22.0 ],
					"style" : "",
					"text" : "s pedal2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-424",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 934.87616, 1285.386475, 47.0, 22.0 ],
					"style" : "",
					"text" : "r pedal"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-438",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1323.052856, 1340.094849, 49.0, 22.0 ],
					"style" : "",
					"text" : "s pedal"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-434",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 385.529907, 1117.853027, 118.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_delayR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-437",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 385.529907, 1141.853027, 116.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_delayL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-286",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1258.997681, 1300.928223, 49.0, 22.0 ],
					"style" : "",
					"text" : "sel 127"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 250.0,
					"id" : "obj-414",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 906.224915, 1424.510742, 359.0, 288.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 996.0, 489.590088, 357.0, 288.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2022.087769, 1026.703979, 96.988434, 96.988434 ],
					"presentation" : 1,
					"presentation_rect" : [ 740.575989, 124.116882, 90.099487, 90.099487 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2022.087769, 1155.988403, 121.0, 22.0 ],
					"style" : "",
					"text" : "s reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 54.763916, 608.239807, 83.0, 22.0 ],
					"style" : "",
					"text" : "receive~ tape"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 13.045868, 291.961182, 70.0, 22.0 ],
					"style" : "",
					"text" : "send~ tape"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 12.0,
					"id" : "obj-158",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 13.850647, 100.5, 54.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[Harpe]",
							"parameter_shortname" : "live.gain~[Harpe]",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ -70 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "livegain~[Harpe1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "bang" ],
					"patching_rect" : [ 18.350647, 57.827576, 89.0, 23.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"mode" : "basic",
						"originallength" : [ 549120.0, "ticks" ],
						"originaltempo" : 120.0,
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"style" : "",
					"text" : "sfplay~ 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-194",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 18.350647, 11.827576, 40.0, 23.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 374.42453, 92.468628, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1070.424561, 110.638428, 31.0, 300.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 346.757904, 92.468628, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1030.757935, 110.638428, 34.0, 300.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[Voix]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3203.838867, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 926.369873, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-192",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3198.258789, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3131.344727, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 853.875732, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3131.344727, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3048.344727, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 770.875732, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "13"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3048.344727, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 13"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1004.868469, 1313.836548, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1454.864258, 1133.994629, 117.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_Evo1R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-237",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1456.864258, 1109.994629, 115.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_Evo1L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1311.733276, 1117.516479, 127.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_BubbleR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1311.733276, 1141.853027, 125.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_BubbleL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1141.121948, 1117.516479, 143.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_BrooksideR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1141.121948, 1141.853027, 141.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_BrooksideL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-228",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 991.827148, 1117.853027, 114.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_DirtyR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 991.827148, 1141.853027, 112.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_DirtyL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 837.103027, 1117.853027, 128.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_InsectsR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 837.103027, 1141.853027, 126.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_InsectsL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 685.406494, 1117.853027, 126.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_KrystalR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 685.406494, 1141.853027, 124.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_KrystalL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 537.657471, 1117.853027, 108.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_psyR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-218",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 537.657471, 1141.853027, 106.0, 22.0 ],
					"style" : "",
					"text" : "send~ norm_psyL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2981.344727, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 703.875732, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-190",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2981.344727, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2921.344727, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 643.875732, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "11"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2921.344727, 337.078705, 53.0, 22.0 ],
					"style" : "",
					"text" : "store 11"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2854.817871, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 577.348877, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-186",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2854.817871, 337.078705, 54.0, 22.0 ],
					"style" : "",
					"text" : "store 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2792.846436, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 515.377441, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2792.846436, 337.078705, 47.0, 22.0 ],
					"style" : "",
					"text" : "store 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1648.659058, 1254.307129, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-149",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 232.09726, 864.143555, 109.0, 23.0 ],
					"style" : "",
					"text" : "plug Altiverb6.vst",
					"varname" : "message"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3159.862793, 621.124512, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 3159.862793, 589.124512, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 994.305969, 1402.303223, 87.0, 22.0 ],
					"style" : "",
					"text" : "s cue_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2727.094727, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 449.625732, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2734.344727, 337.078705, 47.0, 22.0 ],
					"style" : "",
					"text" : "store 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1648.659058, 1321.413574, 71.0, 22.0 ],
					"style" : "",
					"text" : "metro 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1648.659058, 1287.413574, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-136",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1648.659058, 1401.413574, 57.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 761.888184, 75.638428, 57.0, 23.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1648.659058, 1363.401855, 84.0, 23.0 ],
					"style" : "",
					"text" : "adstatus cpu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2660.616211, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 383.147217, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2667.866211, 337.078705, 47.0, 22.0 ],
					"style" : "",
					"text" : "store 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2598.175781, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 320.706787, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2605.425781, 337.078705, 47.0, 22.0 ],
					"style" : "",
					"text" : "store 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3243.241211, 621.124512, 36.0, 22.0 ],
					"style" : "",
					"text" : "write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1530.83252, 960.710205, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 236.302612, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1508.83252, 960.710205, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.302612, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[EvoR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1483.83252, 960.710205, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 189.302612, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"interp" : 0.0,
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1461.83252, 960.710205, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 167.302612, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[EvoL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1466.364258, 924.376953, 96.0, 22.0 ],
					"style" : "",
					"text" : "receive~ Evo1R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1466.364258, 900.376953, 94.0, 22.0 ],
					"style" : "",
					"text" : "receive~ Evo1L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 934.87616, 1322.261475, 38.463013, 38.463013 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 934.87616, 1372.481323, 61.0, 22.0 ],
					"style" : "",
					"text" : "counter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2282.705566, 305.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2280.268066, 337.078705, 47.0, 22.0 ],
					"style" : "",
					"text" : "store 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 242.0, 207.0, 1097.0, 511.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"visible" : 1,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-123",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 796.405762, 35.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "wclose"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-122",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 892.094116, 88.0, 63.0, 22.0 ],
									"style" : "",
									"text" : "delay 300"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 979.094116, 20.116913, 100.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-113",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 979.094116, 55.634033, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-114",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 892.094116, 49.233673, 36.0, 22.0 ],
									"style" : "",
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 892.094116, 20.116913, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 892.094116, 152.567017, 31.0, 22.0 ],
									"style" : "",
									"text" : "9 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 892.094116, 181.567017, 71.0, 22.0 ],
									"style" : "",
									"text" : "8 0.262097"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 892.094116, 125.567017, 37.0, 22.0 ],
									"style" : "",
									"text" : "12 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 574.166626, 22.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 54.0, 35.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.884155, 476.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "9 0.317204"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.884155, 441.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "8 0.299107"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1247.282471, 195.0, 58.5, 22.0 ],
									"style" : "",
									"text" : "store 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 185.594055, 46.216568, 132.0, 22.0 ],
									"style" : "",
									"text" : "r delay_Vertical"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 253.031738, 75.216568, 132.0, 22.0 ],
									"style" : "",
									"text" : "r delay_Horizontal"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 285.275421, 110.88324, 132.0, 22.0 ],
									"style" : "",
									"text" : "r delay_Amp_Distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 399.608765, 86.88324, 120.0, 22.0 ],
									"style" : "",
									"text" : "r delay_Del_Distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 550.333313, 81.883232, 107.666687, 22.0 ],
									"style" : "",
									"text" : "r delay_Feedback"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.0, 66.38324, 50.0, 22.0 ],
									"style" : "",
									"text" : "wclose"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 485.5, 134.88324, 73.0, 33.0 ],
									"style" : "",
									"text" : "Del Random"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 482.0, 110.88324, 100.0, 22.0 ],
									"style" : "",
									"text" : "r delay_Random"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 456.5, 337.116913, 100.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1270.0, 87.0, 51.0, 22.0 ],
									"style" : "",
									"text" : "260 0.6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1275.064941, 56.336666, 36.0, 22.0 ],
									"style" : "",
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1275.064941, 23.00333, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 456.5, 372.634033, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 376.999969, 404.666656, 63.0, 22.0 ],
									"style" : "",
									"text" : "delay 300"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.884155, 406.0, 77.0, 22.0 ],
									"style" : "",
									"text" : "15 0.682927"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.884155, 379.0, 37.0, 22.0 ],
									"style" : "",
									"text" : "14 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.884155, 347.0, 77.0, 22.0 ],
									"style" : "",
									"text" : "13 0.792683"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 284.384155, 323.567017, 44.0, 22.0 ],
									"style" : "",
									"text" : "12 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 284.384155, 293.567017, 76.0, 22.0 ],
									"style" : "",
									"text" : "11 0.353659"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 284.384155, 269.567017, 77.0, 22.0 ],
									"style" : "",
									"text" : "10 0.731707"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 369.499969, 366.233673, 78.0, 22.0 ],
									"style" : "",
									"text" : "sel 1 2 11 24"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 369.499969, 337.116913, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1080.108765, 283.567017, 83.0, 22.0 ],
									"style" : "",
									"text" : "send~ DistoR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1080.108765, 307.567017, 81.0, 22.0 ],
									"style" : "",
									"text" : "send~ DistoL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1149.981689, 135.166687, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1099.76416, 135.166687, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1125.981689, 135.166687, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1080.108765, 135.166687, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1080.108765, 35.182983, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1080.108765, 62.549927, 121.0, 23.0 ],
									"style" : "",
									"text" : "plug GuitarRig5.vst"
								}

							}
, 							{
								"box" : 								{
									"autosave" : 1,
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 8,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
									"patching_rect" : [ 1080.108765, 94.916748, 92.5, 22.0 ],
									"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_invisible" : 1,
											"parameter_longname" : "vst~[21]",
											"parameter_shortname" : "vst~[2]",
											"parameter_type" : 3
										}

									}
,
									"saved_object_attributes" : 									{
										"annotation_name" : "",
										"parameter_enable" : 1
									}
,
									"snapshot" : 									{
										"filetype" : "C74Snapshot",
										"version" : 2,
										"minorversion" : 0,
										"name" : "snapshotlist",
										"origin" : "vst~",
										"type" : "list",
										"subtype" : "Undefined",
										"embed" : 1,
										"snapshot" : 										{
											"pluginname" : "Guitar Rig 5.vst",
											"plugindisplayname" : "Guitar Rig 5",
											"pluginsavedname" : "Guitar Rig 5",
											"pluginsaveduniqueid" : 0,
											"version" : 1,
											"isbank" : 0,
											"isbase64" : 1,
											"blob" : "5127.CMlaKA....fQPMDZ....A3TZGUC...P......51afL2a04FY.............................LwxsjlasH....vHNkzHCM0HD81X00VYtQ2Hi3TRiL0a04FYSgVYrw1HS8VctQ1H..............PQA...gQWXjI.....YtM2bhkFa5YA....D....3o8XhAFXPWO8SWScOS+.xjA.QTp.mDFcgQl.....uYlaoIVZromTA...wR...fm1cRYWOILLTX3qY+JNo2ZvhIXhZJkXhJdCDxPtx3EkQAaXqmk1ND+2a2GLljHf6tdd6664oeswFtKIF1JMVEpGPt45dDPpivkJ85Aj4u8R26Hf0IzKEwnVNfnQBLjGvrXl1aZEdHb+d8H7f..XoFLUZbJok6K8BZQhj+988gPLZCLCiwOXzBwx4EYtOQC+IUhBlHhWHzNbKiVIm2RZydlK38rYe6yGyGkobBCDpVC8YzBohfUFyGGgIo9cg1sOXsvQoOneJevTCdj2FkULENmQsHyUeVTKTV6U1Jhyj7mWsRF4XzxpRuziL+mgGEBiU69UX+bYVooqU53gSefR26oV8ey31KAPmBesFR446iIo1y.qow1R6UzU7ZDt5hg0oYl1xcrzIhOMxYtuik1NUNaKnIxu.kFN6UWSeskU4J9R1UmgQix7OhXz5e1vC9A459FagQWXjI.....cyIGbhkFa5EaD..vXCC..3oc6ccscZyrENmaySgV9VuHf5Bubx+xfwcbAgK+4NgP.SrZQRz7q24E6LBTYFIjAgsYljiyMwpvd9ztM68dZLW09o+yW9xWd7+9kub3+LyxjYhgmOvw966w9sZ6wXXq6zGXO766ce2SpnrGieflceMSGaiuumsydL+yO95gC8p.r8C7FaYXGTQezX6mY5MFX1uh8XqdFdeeONIA18Xzcrbg+P363po+bEPeXiTWtlPcIEEt8XF3oYYL0wa4SfMtk1ub7pj.HQ3c.5dNo2I7c.1nuCrUb8L7MJnIB.VPbqpEvzVaNCacFVgCDUNnFKCWMVk89wW+JCyg9Nis6WouVfVOMeClglN8zLqXYD34X6XsnUGAfMREKm9FQfnO.9cqYaaXBaPo8ptjRPFiGDFH7TXSv.+2gC.ypjvN7Yf7oJf.CKe3qHD8NYeKlvOFk50pAaPasvuiNWl7pvWN7EMMBLRuUzMK9WyLdb3sOYdS8pNlJcl0raG6qdYzMyuuwQe+66gRJThUY3X.9yfOcBvXJhHF903ZpEXToml8yeeu8V0iVBjPkJGOi9UBoPDSUXOlrW4C5YB+6AZl9FgLXrFu5qfMDbqMtOvIUbvsWtuBWsPsv.3a.080fsH72D3XoEDqvEJpReoEZbPah.fN1Kxlixnzdgv.bTrj3b6PqJciizC.SLfeqZliWnXkmFPpDpThf.FOCKGHuzRa1BvDeIXAbqtBXTM4Gm6qO8Q9EyfWvDwzxReHh9X07JjGlXa5EjfrCqhol+JJ+RnZufgLW5XCsIMKoQPQTIxX33oune9usZJ0Z7syc0MFcu30Sm9owvFYLTq.igpzn1D+pUDX3JqBUwDJRmR8k+8rSF7zwcN4lm1+7ym18z6LZO7ScpOcvRWlDJEoIyW1.MJlPQlDOxsuQqeIJc2K6e2n5Sm8z3Gu.7YLGeZRPYlDbHJxGsPRdtcIMFVIIhLCpWWTHPXpXW+Sc92i8cFHZ2b+6HpY.KlY.6mQa7tnGwh3ODlRXIUgx9qizd9oemgW1P9AUGCq47iFa15oyt6yXUWg1CadsG4s0IJhOT2wALmpArS8e9MwctGzb.7zX.dJTVwbic0aFLHAgrjGecwvW2QPCqQNl8QXhBjGjseHFks07Cu4CNlisP5o7aJRh7RJjGoWECzq.VfvalQhWiBfXyDLZLHfo4xRkQS5jcZhDt5n7P7y.ixl9LZsD6p4Zbrg9yLrkM24BnRbkIapKcjz8ftVmV87Ist93SMtn5eQYID.+h6C+hY0FmC0wOKTEKmlSY5Zba5br9J6br.i.bKIyDScGG20X+7NXAsFy7bvKvHQcCJrcR8AUidfnWBD8zr8cc7MnP933DPN1lBwm6jX7kuiaAIIVZAmdI3rigtiW+rwYPM.EbKRsCL7BXt0wGD5Wf9j8l9n9fXTCfcwQgnz.Cksr6SbgdwwbrlnNBebAclsyxvVfEIVBU.DJifolwrHKsxlu8qRqnfSl8qFy94E27KU68E7t1ryjl+TpGYqcSMrfSpQwYeyVaiivHiYyC0JzwNEj6cK1Xz0ZF7d1ZlwJMzStNWwklwHL0VaZJSw17IocONvfpxgU.MGVpJ2UQrbWoInchTLzNAXBuIKSSn0NcUbhSjwwHWVLRC7QEbLRUFF0SiiAl7diG++jhlfQ.+3XDp.Bc2Wof26s0a1ahyUHW6s2C1NAWknWqcBd13dp1MbmMs2ocBZ17dj1IvYK5EZ2fqx2yytAWaduM6Fk6RzCyGAfNr5F4dG9qye6MOAVAzRgO1Fx1KWRq4+8QIp13hlrs9c8yTao55o+x.0Sk0ZQ1oeFGVhpbTbhpRaadpm3jXD438LMEvV2jJ40Ex1XtEDnOJAeR0IO.8C7zsCRJ+RfmSOipMCmo7zDaruaYKS+NEddCRKr7.COCa8T3IHPA.zucZA1LCch0F5L3OubGBGptPusLwEcqJQp9I5rLqczZUwnrk8b0DIpajGZ+3K2I2fU8LImg22tVsa60+xi96YtpEuBezFynaFt5gF.LWL8OzA1AN9iXN63CtBzySya9AG45ZBzWzY.i5XWWGufCtVKbpTxbdxRgx+fSGCBz7XBmArhGzzwFl1RvAIL1CLclBaoIFeap1D3WNjhOWoGHvRy+4EeMKuyGNLFAPPg0Xy.fq4R14RNuPUXxE9g1n5Fo3qR3nza2Ob4cAeLrwW9W65QpVb6Go5amlLPqazjjh.iQjpchOx415zG95Z4VvvoyQMiyV6wqLQcpgG9fYIGPCJeb.S8i99VhsX5hl7UjKxk9qgtlpj36DyOFhyqn2PfqtPcIYt5hafWr2hR+GS9nqhW7tkHpD5Rj6HK2ir0Lm6W57QKjLQwSzcXfY.6oOnxa7beomZMpwXNqOWUT+8M22EEQTDtcAohmkKkSi5UHTjNEGXt8ES5J14LUYV0ZW+x4lfW9bL4euCkJafTOTXOXBBTvLj93D7cr27MaBVsSw2iI36QDMZ5AeseJIBJvLZBX29Dh2.lvkFCCDg.6g++3PPtfjUbJwfOVR61cRs9Kos5tYn91P8+cBX1Fc9cBvJoWrOl.iWiQvaLxX4ZqZ4JT1MLfhnRTLL2ewO4Nel9jN6+aOuaegWy3r1A2822hdfi1VzCRaeoj9SY1lS4KNCfJZ4tX55vrbw284TL+uloX9eLK.h2k4BOGYlK70QWVcQqe0SdpjcSU.Qh5kp5ISTz696occ0je3x4RSFHN55OqdyFN62210ddYGxB5b7RSepticfFHbzS0WNrTUVHy.lff4I08JcixaUQBBk1iMWvA62iAXCoU51AnPLAf2VO7U7SHJjo6Asa7blt7dHx2rsvHG+.DAErYxIO4+vG02L2LhiensickU8f7isqLh07wfPqmkKdqklyOoY0SaTf5MOueek63F9PigCXwMmK1TdMlwm1Q3UrjO+5y6tdqYzw1MvaLlwbgFxapQb4MfU1nPEeCFu6jL1ZdjpJxzrwvyvgzIQFXiN2jPBiVRgsdcxfoINEOBcJ7JJBDpRI9AqxdNsDN0YqwQFn0HI53FZ99nCnonr.gvT6zZ3ftUiHWSrFgJ0U2z4OlQOSCbQmxNpfIqpO7B6+dE8qkaJDgNGhNyIfQ0wzY+3tYL8GXuespmU898696tWe5c0dtq4z2mtYdSQKRY8uvJ8YGLePkOMQ47ZGOKMSxufatMYhVcCT+ouG19w22TDXk4ICvtIYE2r9MrmcS2J7EzsBOOOKoFa.Aj9U5i4EGFZfLghMnqXg8sHKUWjPJTskvKm.EX6IiMcBPmizvdgkT3HjV0wIq.miMbCFQALpjkfysNSI+JB5A1z0MolGPSm7bHU1D+3pZCIOd5vxkVXUeXXJ9TvhKsAapKbfl+NGPu+w6xhVkzlQaywcbbrXt0yINr2maBSEaluU+m2+g6t95Ib7RBdG8dUcEwsn5JEu.rnfneqK9WQzuoaLiFZdDe8J+ZiPYMRUZkgro6En.6vM9OR6gZHGFh3n.DwigHdJ.QBXHRfBPjHFhDo.DIggHIJ.QxXHRlBPjBFhTHOhbS7GcqFU3NxkCEPTf2HWdT.QANibEPADE3KxUDEPTfqHWIT.QAdhbkQADE3HxUAEPTfeHKVz0rFpiHBgGNL7vQb7fs46f5FhP3Q.COBDGOhX3Qj33QBCORDGOxX3Ql33QACOJjFO9o0xxwzg79e74vvCw8+3yigGh6+wW.COD2+iuHFdHt+GeIL7Pb+O9xX3g39e7UvvCw8+3BRSDaDr8N2dB4cB4B3xAJNxCJ9bfhm7fRHGnDHOnDyAJQxCJobfRh7fRNGnjIOnTxAJx6mZDteJJvI0HbmTTQMiFg6ihJJazHbWTTQkiFg6ghJJdzHbGTTQ8iFg6ehJJgzHb2SzPUj7RbNsX.oQKmMKmrjDgPEGFpn.+Sd7XHhB7N4IfgHJv2jmHFhn.OSdRXHhB7K4IigHJvqjmBFhnAeRV3Nkni5a6YwkGUbjGU74QEO4QkPdTIPdTIlGUhjGUR4QkD4QkbdTISdTojGUjuR3T15WPKYG0CP7YgauhVkWDZsCV3bdmP.poVujN6f+Mv1H.oqNhshE.5oitLPGEQDBP253igH30jGUPAFWVgGGwYU.cNTNEwCQIT1wkU1QbTAkW7Ykc7TfriGkSwSCxN9rxNdJP1IjU1IPAxNATNk.MH6DxJ6Dn.YmXVYmHEH6DQ4ThzfrSLqrSjBjcRYkcRTfrSBkSIQCxNorxNIJP1ImU1ISAxNYTNkLMH6jyJ6jo.YmRVYmBMjjfBJqRgFDdJYEdJ67CBt28sKCzskoG.1AZCMXdvvqWxg2VqdOb0M7+q8y2L+NG1FWM9xNtetiYjaMCJ7WwRFTMIO3y63aXZnGPb6NtUs2GW6aDywDIO1Ue2s+4QOiLt0et9HiiML0lGa8Oq9iRA223lYs9MuF3p4yur1KJetsrU31lC+eGdABOpmVww9DgbAbLx95MpG.dYIVBuyYsvXgoK.a8LKqHKRpC93I9oa5Bgp0D+fg93zMINivyoPxWU7SRNWTML52SS+YzsLGwZ0IzzRQMnK9H8jUshTJ6pAcljAYThtkZvwYFcLlrJYDJmpDG5M8b78YxooQpytgjt5wNycIloHHYSQ77vi0Zl+.Co5vpqdKv8vpC8p3Ak4+3qgW4YnYF.sm0WtCszuWZPGw5BGF8LSCu9ZAZK9Vg2Z.XX7lHbzSWDmAR.cw2mwzQ+4kavvK6A3RlaM5GtEykDmxhmFhEnAL7SawQVkhP31AsoyxKgOOvY3PS7iR1WuUtDn+7wNSsWSCUSlGogXSaH1MsgTmFp2xvh0PrYaH1ZB0QZHNzFBcqONQULUVme6LcYfeKOR+zlUIw9kApkUIQSM4v2kYAUqfGMWzlr7XOyvDagoXGd3f.UYVdk2PiHbuLW7n6DYnFFaX7cFa0agzGgcUMkeUJVH2qKqX4qIhvB42dYE+ZjUhRnZeBkVVgWyhcozhaGJtDVCWTgUFgKJVZtHVpe6PlXT9mu+7P177PwWmGxgpvKUdN3xiCP3+MojbPdbN3Fy6xx2xwzp7dv0jVCWSA07UdaYaWaLKnjrMNJjs0M7fX+0YXh7nVpJklgE0DuEaTri69MjqIj2N0u8GhYpxqy+jqiFDQ8Ry+PNV4eK7vtZtknmh7Luttuu8SzZF7i0VybYDeYhPJenX7bnQHwtMA8koEWS.EbhJn9WYYKaKdswTlns2vWukPOYkSaOtUzdnb4LQbG8BZgcHBwxFD+8RgYtHuCQ.RP0w5RUW2OOMj5kTfEmBrqmBowJujBbaKE3ho.+1RA9XJHrsTPHlBhaKEDiofTVJfUX3EjXgpEl6gnNpyaftr01PLHEiA4sFCbaGFh50XYqqrs7PkXJTuzTHu+oH85xaZj2wSDoXKGov7nDQBtBHwgUibEDc4G1g9ygKqgwJJSfvd4GCmJKc6YByW0vNrfHgMRl6EZyfqdEAqJg97RouL9XeDwpRaJjLTScEVAMK6Mr+zxDrejVNJhe29V3J9iQ3OsOln7nV02h3eZeKBE9onrceJudfpenpXE9oTe69Td8vFekOEzBZlwAQxq3aq45OxI.sNnI2agqkjqJ7HK6vpYnxgUy3QK51azgZVBUidwEiAaZr8o+7DtAmrT7XWeRScoijtGz05zpmOo00GepwEUC2zqqtEjUJ43wswEMYa865mo1R00S+kApmJq0Z3VR1vjYVyot61PVwDl.GXt8ES5J14LUYV0ZW+x4lfW1ZxJGS1e52Y3kMjeP0wvZN+nwlsd5r6ltcjMkGT3bMZa.qhRLYen8iubmbCV0yjbFde6Z0tsW+K2REA9ZIr1Y+pwredwM+R0deAuqM6Lo4Ok5skr1Zq8bvaavpB+ZOsL2F4k7ZmcHai7hasaV8aAUkW6A+TR.d4bHsbTbxMvMKGOGXKpE5es5xKgsQTBjLCb7rrzr0FF19IG7mgujFzs7DiJKpSfVvHlnqiphfiWeXXso0fBudBQj.8cA1CgAYZZtzae0erXDm.19AdisVb1VNZr8y+3q+Or1P.fTXzEFYB....PjSSMkXowldV.....A....dZOlXfAFz0S+z0T2yzOfLY.PDkJvIonent-gui>\n            <component-audio version=\"2\">\n              <parameters enable-automation=\"0\" num-parameters=\"1\" static-automation=\"1\">\n                <parameter id=\"iA\" name=\"InstanceActive\" value=\"0\">\n                  <base-parameters remote-max=\"1\" remote-min=\"0\"/>\n                </parameter>"
										}
,
										"snapshotlist" : 										{
											"current_snapshot" : 0,
											"entries" : [ 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"snapshot" : 													{
														"pluginname" : "Guitar Rig 5.vst",
														"plugindisplayname" : "Guitar Rig 5",
														"pluginsavedname" : "Guitar Rig 5",
														"pluginsaveduniqueid" : 0,
														"version" : 1,
														"isbank" : 0,
														"isbase64" : 1,
														"blob" : "5127.CMlaKA....fQPMDZ....A3TZGUC...P......51afL2a04FY.............................LwxsjlasH....vHNkzHCM0HD81X00VYtQ2Hi3TRiL0a04FYSgVYrw1HS8VctQ1H..............PQA...gQWXjI.....YtM2bhkFa5YA....D....3o8XhAFXPWO8SWScOS+.xjA.QTp.mDFcgQl.....uYlaoIVZromTA...wR...fm1cRYWOILLTX3qY+JNo2ZvhIXhZJkXhJdCDxPtx3EkQAaXqmk1ND+2a2GLljHf6tdd6664oeswFtKIF1JMVEpGPt45dDPpivkJ85Aj4u8R26Hf0IzKEwnVNfnQBLjGvrXl1aZEdHb+d8H7f..XoFLUZbJok6K8BZQhj+988gPLZCLCiwOXzBwx4EYtOQC+IUhBlHhWHzNbKiVIm2RZydlK38rYe6yGyGkobBCDpVC8YzBohfUFyGGgIo9cg1sOXsvQoOneJevTCdj2FkULENmQsHyUeVTKTV6U1Jhyj7mWsRF4XzxpRuziL+mgGEBiU69UX+bYVooqU53gSefR26oV8ey31KAPmBesFR446iIo1y.qow1R6UzU7ZDt5hg0oYl1xcrzIhOMxYtuik1NUNaKnIxu.kFN6UWSeskU4J9R1UmgQix7OhXz5e1vC9A459FagQWXjI.....cyIGbhkFa5EaD..vXCC..3oc6ccscZyrENmaySgV9VuHf5Bubx+xfwcbAgK+4NgP.SrZQRz7q24E6LBTYFIjAgsYljiyMwpvd9ztM68dZLW09o+yW9xWd7+9kub3+LyxjYhgmOvw966w9sZ6wXXq6zGXO766ce2SpnrGieflceMSGaiuumsydL+yO95gC8p.r8C7FaYXGTQezX6mY5MFX1uh8XqdFdeeONIA18Xzcrbg+P363po+bEPeXiTWtlPcIEEt8XF3oYYL0wa4SfMtk1ub7pj.HQ3c.5dNo2I7c.1nuCrUb8L7MJnIB.VPbqpEvzVaNCacFVgCDUNnFKCWMVk89wW+JCyg9Nis6WouVfVOMeClglN8zLqXYD34X6XsnUGAfMREKm9FQfnO.9cqYaaXBaPo8ptjRPFiGDFH7TXSv.+2gC.ypjvN7Yf7oJf.CKe3qHD8NYeKlvOFk50pAaPasvuiNWl7pvWN7EMMBLRuUzMK9WyLdb3sOYdS8pNlJcl0raG6qdYzMyuuwQe+66gRJThUY3X.9yfOcBvXJhHF903ZpEXToml8yeeu8V0iVBjPkJGOi9UBoPDSUXOlrW4C5YB+6AZl9FgLXrFu5qfMDbqMtOvIUbvsWtuBWsPsv.3a.080fsH72D3XoEDqvEJpReoEZbPah.fN1Kxlixnzdgv.bTrj3b6PqJciizC.SLfeqZliWnXkmFPpDpThf.FOCKGHuzRa1BvDeIXAbqtBXTM4Gm6qO8Q9EyfWvDwzxReHh9X07JjGlXa5EjfrCqhol+JJ+RnZufgLW5XCsIMKoQPQTIxX33oune9usZJ0Z7syc0MFcu30Sm9owvFYLTq.igpzn1D+pUDX3JqBUwDJRmR8k+8rSF7zwcN4lm1+7ym18z6LZO7ScpOcvRWlDJEoIyW1.MJlPQlDOxsuQqeIJc2K6e2n5Sm8z3Gu.7YLGeZRPYlDbHJxGsPRdtcIMFVIIhLCpWWTHPXpXW+Sc92i8cFHZ2b+6HpY.KlY.6mQa7tnGwh3ODlRXIUgx9qizd9oemgW1P9AUGCq47iFa15oyt6yXUWg1CadsG4s0IJhOT2wALmpArS8e9MwctGzb.7zX.dJTVwbic0aFLHAgrjGecwvW2QPCqQNl8QXhBjGjseHFks07Cu4CNlisP5o7aJRh7RJjGoWECzq.VfvalQhWiBfXyDLZLHfo4xRkQS5jcZhDt5n7P7y.ixl9LZsD6p4Zbrg9yLrkM24BnRbkIapKcjz8ftVmV87Ist93SMtn5eQYID.+h6C+hY0FmC0wOKTEKmlSY5Zba5br9J6br.i.bKIyDScGG20X+7NXAsFy7bvKvHQcCJrcR8AUidfnWBD8zr8cc7MnP933DPN1lBwm6jX7kuiaAIIVZAmdI3rigtiW+rwYPM.EbKRsCL7BXt0wGD5Wf9j8l9n9fXTCfcwQgnz.Cksr6SbgdwwbrlnNBebAclsyxvVfEIVBU.DJifolwrHKsxlu8qRqnfSl8qFy94E27KU68E7t1ryjl+TpGYqcSMrfSpQwYeyVaiivHiYyC0JzwNEj6cK1Xz0ZF7d1ZlwJMzStNWwklwHL0VaZJSw17IocONvfpxgU.MGVpJ2UQrbWoInchTLzNAXBuIKSSn0NcUbhSjwwHWVLRC7QEbLRUFF0SiiAl7diG++jhlfQ.+3XDp.Bc2Wof26s0a1ahyUHW6s2C1NAWknWqcBd13dp1MbmMs2ocBZ17dj1IvYK5EZ2fqx2yytAWaduM6Fk6RzCyGAfNr5F4dG9qye6MOAVAzRgO1Fx1KWRq4+8QIp13hlrs9c8yTao55o+x.0Sk0ZQ1oeFGVhpbTbhpRaadpm3jXD438LMEvV2jJ40Ex1XtEDnOJAeR0IO.8C7zsCRJ+RfmSOipMCmo7zDaruaYKS+NEddCRKr7.COCa8T3IHPA.zucZA1LCch0F5L3OubGBGptPusLwEcqJQp9I5rLqczZUwnrk8b0DIpajGZ+3K2I2fU8LImg22tVsa60+xi96YtpEuBezFynaFt5gF.LWL8OzA1AN9iXN63CtBzySya9AG45ZBzWzY.i5XWWGufCtVKbpTxbdxRgx+fSGCBz7XBmArhGzzwFl1RvAIL1CLclBaoIFeap1D3WNjhOWoGHvRy+4EeMKuyGNLFAPPg0Xy.fq4R14RNuPUXxE9g1n5Fo3qR3nza2Ob4cAeLrwW9W65QpVb6Go5amlLPqazjjh.iQjpchOx415zG95Z4VvvoyQMiyV6wqLQcpgG9fYIGPCJeb.S8i99VhsX5hl7UjKxk9qgtlpj36DyOFhyqn2PfqtPcIYt5hafWr2hR+GS9nqhW7tkHpD5Rj6HK2ir0Lm6W57QKjLQwSzcXfY.6oOnxa7beomZMpwXNqOWUT+8M22EEQTDtcAohmkKkSi5UHTjNEGXt8ES5J14LUYV0ZW+x4lfW9bL4euCkJafTOTXOXBBTvLj93D7cr27MaBVsSw2iI36QDMZ5AeseJIBJvLZBX29Dh2.lvkFCCDg.6g++3PPtfjUbJwfOVR61cRs9Kos5tYn91P8+cBX1Fc9cBvJoWrOl.iWiQvaLxX4ZqZ4JT1MLfhnRTLL2ewO4Nel9jN6+aOuaegWy3r1A2822hdfi1VzCRaeoj9SY1lS4KNCfJZ4tX55vrbw284TL+uloX9eLK.h2k4BOGYlK70QWVcQqe0SdpjcSU.Qh5kp5ISTz696occ0je3x4RSFHN55OqdyFN62210ddYGxB5b7RSepticfFHbzS0WNrTUVHy.lff4I08JcixaUQBBk1iMWvA62iAXCoU51AnPLAf2VO7U7SHJjo6Asa7blt7dHx2rsvHG+.DAErYxIO4+vG02L2LhiensickU8f7isqLh07wfPqmkKdqklyOoY0SaTf5MOueek63F9PigCXwMmK1TdMlwm1Q3UrjO+5y6tdqYzw1MvaLlwbgFxapQb4MfU1nPEeCFu6jL1ZdjpJxzrwvyvgzIQFXiN2jPBiVRgsdcxfoINEOBcJ7JJBDpRI9AqxdNsDN0YqwQFn0HI53FZ99nCnonr.gvT6zZ3ftUiHWSrFgJ0U2z4OlQOSCbQmxNpfIqpO7B6+dE8qkaJDgNGhNyIfQ0wzY+3tYL8GXuespmU898696tWe5c0dtq4z2mtYdSQKRY8uvJ8YGLePkOMQ47ZGOKMSxufatMYhVcCT+ouG19w22TDXk4ICvtIYE2r9MrmcS2J7EzsBOOOKoFa.Aj9U5i4EGFZfLghMnqXg8sHKUWjPJTskvKm.EX6IiMcBPmizvdgkT3HjV0wIq.miMbCFQALpjkfysNSI+JB5A1z0MolGPSm7bHU1D+3pZCIOd5vxkVXUeXXJ9TvhKsAapKbfl+NGPu+w6xhVkzlQaywcbbrXt0yINr2maBSEaluU+m2+g6t95Ib7RBdG8dUcEwsn5JEu.rnfneqK9WQzuoaLiFZdDe8J+ZiPYMRUZkgro6En.6vM9OR6gZHGFh3n.DwigHdJ.QBXHRfBPjHFhDo.DIggHIJ.QxXHRlBPjBFhTHOhbS7GcqFU3NxkCEPTf2HWdT.QANibEPADE3KxUDEPTfqHWIT.QAdhbkQADE3HxUAEPTfeHKVz0rFpiHBgGNL7vQb7fs46f5FhP3Q.COBDGOhX3Qj33QBCORDGOxX3Ql33QACOJjFO9o0xxwzg79e74vvCw8+3yigGh6+wW.COD2+iuHFdHt+GeIL7Pb+O9xX3g39e7UvvCw8+3BRSDaDr8N2dB4cB4B3xAJNxCJ9bfhm7fRHGnDHOnDyAJQxCJobfRh7fRNGnjIOnTxAJx6mZDteJJvI0HbmTTQMiFg6ihJJazHbWTTQkiFg6ghJJdzHbGTTQ8iFg6ehJJgzHb2SzPUj7RbNsX.oQKmMKmrjDgPEGFpn.+Sd7XHhB7N4IfgHJv2jmHFhn.OSdRXHhB7K4IigHJvqjmBFhnAeRV3Nkni5a6YwkGUbjGU74QEO4QkPdTIPdTIlGUhjGUR4QkD4QkbdTISdTojGUjuR3T15WPKYG0CP7YgauhVkWDZsCV3bdmP.poVujN6f+Mv1H.oqNhshE.5oitLPGEQDBP253igH30jGUPAFWVgGGwYU.cNTNEwCQIT1wkU1QbTAkW7Ykc7TfriGkSwSCxN9rxNdJP1IjU1IPAxNATNk.MH6DxJ6Dn.YmXVYmHEH6DQ4ThzfrSLqrSjBjcRYkcRTfrSBkSIQCxNorxNIJP1ImU1ISAxNYTNkLMH6jyJ6jo.YmRVYmBMjjfBJqRgFDdJYEdJ67CBt28sKCzskoG.1AZCMXdvvqWxg2VqdOb0M7+q8y2L+NG1FWM9xNtetiYjaMCJ7WwRFTMIO3y63aXZnGPb6NtUs2GW6aDywDIO1Ue2s+4QOiLt0et9HiiML0lGa8Oq9iRA223lYs9MuF3p4yur1KJetsrU31lC+eGdABOpmVww9DgbAbLx95MpG.dYIVBuyYsvXgoK.a8LKqHKRpC93I9oa5Bgp0D+fg93zMINivyoPxWU7SRNWTML52SS+YzsLGwZ0IzzRQMnK9H8jUshTJ6pAcljAYThtkZvwYFcLlrJYDJmpDG5M8b78YxooQpytgjt5wNycIloHHYSQ77vi0Zl+.Co5vpqdKv8vpC8p3Ak4+3qgW4YnYF.sm0WtCszuWZPGw5BGF8LSCu9ZAZK9Vg2Z.XX7lHbzSWDmAR.cw2mwzQ+4kavvK6A3RlaM5GtEykDmxhmFhEnAL7SawQVkhP31AsoyxKgOOvY3PS7iR1WuUtDn+7wNSsWSCUSlGogXSaH1MsgTmFp2xvh0PrYaH1ZB0QZHNzFBcqONQULUVme6LcYfeKOR+zlUIw9kApkUIQSM4v2kYAUqfGMWzlr7XOyvDagoXGd3f.UYVdk2PiHbuLW7n6DYnFFaX7cFa0agzGgcUMkeUJVH2qKqX4qIhvB42dYE+ZjUhRnZeBkVVgWyhcozhaGJtDVCWTgUFgKJVZtHVpe6PlXT9mu+7P177PwWmGxgpvKUdN3xiCP3+MojbPdbN3Fy6xx2xwzp7dv0jVCWSA07UdaYaWaLKnjrMNJjs0M7fX+0YXh7nVpJklgE0DuEaTri69MjqIj2N0u8GhYpxqy+jqiFDQ8Ry+PNV4eK7vtZtknmh7Luttuu8SzZF7i0VybYDeYhPJenX7bnQHwtMA8koEWS.EbhJn9WYYKaKdswTlns2vWukPOYkSaOtUzdnb4LQbG8BZgcHBwxFD+8RgYtHuCQ.RP0w5RUW2OOMj5kTfEmBrqmBowJujBbaKE3ho.+1RA9XJHrsTPHlBhaKEDiofTVJfUX3EjXgpEl6gnNpyaftr01PLHEiA4sFCbaGFh50XYqqrs7PkXJTuzTHu+oH85xaZj2wSDoXKGov7nDQBtBHwgUibEDc4G1g9ygKqgwJJSfvd4GCmJKc6YByW0vNrfHgMRl6EZyfqdEAqJg97RouL9XeDwpRaJjLTScEVAMK6Mr+zxDrejVNJhe29V3J9iQ3OsOln7nV02h3eZeKBE9onrceJudfpenpXE9oTe69Td8vFekOEzBZlwAQxq3aq45OxI.sNnI2agqkjqJ7HK6vpYnxgUy3QK51azgZVBUidwEiAaZr8o+7DtAmrT7XWeRScoijtGz05zpmOo00GepwEUC2zqqtEjUJ43wswEMYa865mo1R00S+kApmJq0Z3VR1vjYVyot61PVwDl.GXt8ES5J14LUYV0ZW+x4lfW1ZxJGS1e52Y3kMjeP0wvZN+nwlsd5r6ltcjMkGT3bMZa.qhRLYen8iubmbCV0yjbFde6Z0tsW+K2REA9ZIr1Y+pwredwM+R0deAuqM6Lo4Ok5skr1Zq8bvaavpB+ZOsL2F4k7ZmcHai7hasaV8aAUkW6A+TR.d4bHsbTbxMvMKGOGXKpE5es5xKgsQTBjLCb7rrzr0FF19IG7mgujFzs7DiJKpSfVvHlnqiphfiWeXXso0fBudBQj.8cA1CgAYZZtzae0erXDm.19AdisVb1VNZr8y+3q+Or1P.fTXzEFYB....PjSSMkXowldV.....A....dZOlXfAFz0S+z0T2yzOfLY.PDkJvIonent-gui>\n            <component-audio version=\"2\">\n              <parameters enable-automation=\"0\" num-parameters=\"1\" static-automation=\"1\">\n                <parameter id=\"iA\" name=\"InstanceActive\" value=\"0\">\n                  <base-parameters remote-max=\"1\" remote-min=\"0\"/>\n                </parameter>"
													}
,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "a6b1a8d3f5e77f2bdba72ec9cea73728"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_3.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "c259c7d29daff2945e0a13b717fa589d"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_4.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "e49a174465f520a643b4a51c1a5576fa"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_5.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "b5f4b1b83cc02ff5e5837d1fc2237619"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_6.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "ade8367234782afdb620c5c4eece61f1"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_3.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "8baf871949e0d9abd5f0a3995c908465"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_4.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "18127d1fc2199efc6f4a87308fab3072"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_5.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "d7eabcab9ded5733685090b53d8275ee"
													}

												}
, 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "Guitar Rig 5",
													"origin" : "Guitar Rig 5.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"fileref" : 													{
														"name" : "Guitar Rig 5",
														"filename" : "Guitar Rig 5_20161002_6.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "c50d68ea65747af781851d483cddb86e"
													}

												}
 ]
										}

									}
,
									"style" : "",
									"text" : "vst~",
									"varname" : "vst~[2]",
									"viewvisibility" : 0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 485.5, 177.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "prepend 13"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1159.594116, 574.067017, 93.0, 22.0 ],
									"style" : "",
									"text" : "send~ BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1159.594116, 550.067017, 91.0, 22.0 ],
									"style" : "",
									"text" : "send~ BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1012.782471, 574.067017, 116.0, 22.0 ],
									"style" : "",
									"text" : "send~ BrookesideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1012.782471, 550.067017, 114.0, 22.0 ],
									"style" : "",
									"text" : "send~ BrookesideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 904.811646, 557.067017, 80.0, 22.0 ],
									"style" : "",
									"text" : "send~ DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 904.811646, 581.067017, 78.0, 22.0 ],
									"style" : "",
									"text" : "send~ DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 798.594116, 551.067017, 94.0, 22.0 ],
									"style" : "",
									"text" : "send~ InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 796.405762, 575.067017, 92.0, 22.0 ],
									"style" : "",
									"text" : "send~ InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 684.811646, 557.067017, 92.0, 22.0 ],
									"style" : "",
									"text" : "send~ KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 684.811646, 581.067017, 90.0, 22.0 ],
									"style" : "",
									"text" : "send~ KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1225.282471, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1175.064941, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1201.282471, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1154.782471, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1154.782471, 348.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 1154.782471, 373.634033, 48.0, 22.0 ],
									"style" : "",
									"text" : "Bubble"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 867.594116, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 817.376587, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 843.594116, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 797.094116, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1014.282471, 348.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 905.311646, 342.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1083.282471, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1033.064941, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1059.282471, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1012.782471, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 975.311646, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 925.094116, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 951.311646, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 904.811646, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-117",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 755.311646, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 705.094116, 397.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-119",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 731.311646, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-120",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 684.811646, 397.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-116",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 798.594116, 342.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-115",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 684.811646, 342.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 1014.282471, 373.634033, 64.0, 22.0 ],
									"style" : "",
									"text" : "Brookside"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 905.311646, 367.634033, 35.0, 22.0 ],
									"style" : "",
									"text" : "Dirty"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 797.094116, 367.634033, 49.0, 22.0 ],
									"style" : "",
									"text" : "Insects"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 684.811646, 367.634033, 47.0, 22.0 ],
									"style" : "",
									"text" : "Krystal"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1273.905762, 348.634033, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1344.405762, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1294.188232, 403.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1320.405762, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1273.905762, 403.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1273.905762, 575.067017, 74.0, 22.0 ],
									"style" : "",
									"text" : "send~ psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1273.905762, 551.067017, 72.0, 22.0 ],
									"style" : "",
									"text" : "send~ psyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "signal" ],
									"patching_rect" : [ 1273.905762, 373.634033, 69.0, 22.0 ],
									"style" : "",
									"text" : "Psychotica"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1194.311646, 195.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1194.311646, 157.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "preset", "int", "preset", "int" ],
									"patching_rect" : [ 1193.311646, 241.567017, 100.0, 40.0 ],
									"preset_data" : [ 										{
											"number" : 1,
											"data" : [ 6, "obj-96", "gain~", "list", 83, 10.0, 6, "obj-95", "gain~", "list", 83, 10.0, 6, "obj-6", "gain~", "list", 103, 10.0, 6, "obj-5", "gain~", "list", 103, 10.0, 6, "obj-102", "gain~", "list", 117, 10.0, 6, "obj-101", "gain~", "list", 117, 10.0, 6, "obj-120", "gain~", "list", 99, 10.0, 6, "obj-119", "gain~", "list", 99, 10.0, 6, "obj-20", "gain~", "list", 78, 10.0, 6, "obj-19", "gain~", "list", 78, 10.0, 6, "obj-31", "gain~", "list", 107, 10.0, 6, "obj-30", "gain~", "list", 107, 10.0, 6, "obj-37", "gain~", "list", 123, 10.0, 6, "obj-36", "gain~", "list", 123, 10.0, 6, "obj-40", "gain~", "list", 115, 10.0, 6, "obj-39", "gain~", "list", 115, 10.0, 6, "obj-66", "gain~", "list", 90, 10.0, 6, "obj-65", "gain~", "list", 90, 10.0 ]
										}
, 										{
											"number" : 2,
											"data" : [ 6, "obj-96", "gain~", "list", 124, 10.0, 6, "obj-95", "gain~", "list", 124, 10.0, 6, "obj-6", "gain~", "list", 129, 10.0, 6, "obj-5", "gain~", "list", 129, 10.0, 6, "obj-102", "gain~", "list", 122, 10.0, 6, "obj-101", "gain~", "list", 122, 10.0, 6, "obj-120", "gain~", "list", 131, 10.0, 6, "obj-119", "gain~", "list", 131, 10.0, 6, "obj-20", "gain~", "list", 127, 10.0, 6, "obj-19", "gain~", "list", 127, 10.0, 6, "obj-31", "gain~", "list", 124, 10.0, 6, "obj-30", "gain~", "list", 124, 10.0, 6, "obj-37", "gain~", "list", 130, 10.0, 6, "obj-36", "gain~", "list", 130, 10.0, 6, "obj-40", "gain~", "list", 127, 10.0, 6, "obj-39", "gain~", "list", 127, 10.0, 6, "obj-66", "gain~", "list", 66, 10.0, 6, "obj-65", "gain~", "list", 66, 10.0, 5, "<invalid>", "toggle", "int", 1 ]
										}
, 										{
											"number" : 24,
											"data" : [ 6, "obj-96", "gain~", "list", 0, 10.0, 6, "obj-95", "gain~", "list", 0, 10.0, 6, "obj-6", "gain~", "list", 0, 10.0, 6, "obj-5", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "obj-102", "gain~", "list", 0, 10.0, 6, "obj-101", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "obj-120", "gain~", "list", 0, 10.0, 6, "obj-119", "gain~", "list", 0, 10.0, 6, "obj-20", "gain~", "list", 0, 10.0, 6, "obj-19", "gain~", "list", 0, 10.0, 6, "obj-31", "gain~", "list", 0, 10.0, 6, "obj-30", "gain~", "list", 0, 10.0, 6, "obj-37", "gain~", "list", 1, 10.0, 6, "obj-36", "gain~", "list", 1, 10.0, 6, "obj-40", "gain~", "list", 0, 10.0, 6, "obj-39", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0 ]
										}
 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 687.25, 41.500122, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 687.25, 288.567017, 83.0, 22.0 ],
									"style" : "",
									"text" : "send~ Evo1R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 687.25, 312.567017, 81.0, 22.0 ],
									"style" : "",
									"text" : "send~ Evo1L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 687.25, 66.38324, 105.0, 22.0 ],
									"style" : "",
									"text" : "plug Evolution.vst"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 753.311646, 136.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 705.094116, 136.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 731.311646, 136.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 684.811646, 136.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"autosave" : 1,
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 8,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
									"patching_rect" : [ 687.25, 97.0, 92.5, 22.0 ],
									"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_invisible" : 1,
											"parameter_longname" : "vst~",
											"parameter_shortname" : "vst~",
											"parameter_type" : 3
										}

									}
,
									"saved_object_attributes" : 									{
										"annotation_name" : "",
										"parameter_enable" : 1
									}
,
									"snapshot" : 									{
										"filetype" : "C74Snapshot",
										"version" : 2,
										"minorversion" : 0,
										"name" : "snapshotlist",
										"origin" : "vst~",
										"type" : "list",
										"subtype" : "Undefined",
										"embed" : 1,
										"snapshot" : 										{
											"pluginname" : "Evolution.vst",
											"plugindisplayname" : "GRM Evolution",
											"pluginsavedname" : "GRM Evolution",
											"pluginsaveduniqueid" : 0,
											"version" : 1,
											"isbank" : 0,
											"isbase64" : 1,
											"blob" : "17828.CMlaKA....fQPMDZ....AbjbEYG.CP......AjjaoQGHEY2arUGco8la.....................TDZL....bjbsAxQxUjc....lguP..P..........zChHl3Opn5p+.H..7CSL2rOlVbA..........vO....+......Q.LD.FAPQ.fD.GA..........9nppqd..........................Q....3Q...fG....7D...fOwBQ3A.j..J.PB.j..J.PB.j..I.fB.n..I.PB.j..J.PB.n..I.PB.j..I.PB............................................J.PB.n..J.PB.j..I.PB.j..I.fB.j..J.PB.n..K.fB.n..J.fB.j..I..B.b..F..B.n..J.fB.n..J.fB..........P.....B....L.....A....E....X....vA..fB.....D.....A..fB.....D.....AA.fA.....D.....A..PB.....D.....A..PB.....D.....A..fB.....D.....AA.fB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..PB.....D.....A..vA.....D.....A..PB.....D.....A..PB.....D.....AA.PB.....D.....A..fB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..PB.....D.....A..PB.....D.....A..PB.....D.....A..vA.....D.....AK..C.r..L.fB.v..N..C.r..M..C.z..L..C.v..K..C.r..M.fB.........j..I..A.L..H.PA.b..E.PA.T..E.PA.X..E.PA.T..E.PA.T..D..A.P..E..A.T..D.PA.3BP..fL.A..F.PA.XA.W..F.nA.b..B.n..K..C.z..N..D.DA.R.vD.PA.U.PF.rA.c.vC.j..G.v..P..K..C.z..O.PD.HA.S..E.TA.V..F.nA.a..G.zA.e..H.HB.i.vI.nB.q..K.zB.t.vK.DC.y.fN.rC.MAfS..E.UAfU.nE.aAvW.PF.oAva.HG.3APd..H.BBvf.XH.HBPh.nH.MBfi..I.QB.k.TI.VBfl.zI.eBPn.PJ.lB.p.jJ.pB.q.zJ.vBPr.HK.zBPs.bK.5Bvt..L.ACvv.PL.ECvw.fL.ICvx.vL.OC.z.DM.TCf0.bM.bCP2.7M.fCf3.PN.kCf4.bN.nC.6.HO.yCf8.jO.6C.+.7O.HDf..T..I.fB.3..P.vE.jA.d.PH.PB......7iJpt5OLwby+.H..3ioEWvO....+......Q.LD.FAPQ.fD.GA..........9nppqJE.SA......8fHhIB...............XF.mA.Z.nF.qAfa..G.yA.c.TG.1Avc.nG.6A.e.zG.9Ave.DH.DBPg.bH.KB.i.7H.RBvj.bI.XBPl.rI.bB...........................................zL.NCfz.LM.UC.1.jM.ZCv1.3M.gCv3.jN.pCv5.zN.tCv6..O.wC.8.TO.2C.9.nO.8Cf+.3P.cDPM.nP.LD...jQ.....A....P...DQ.....A....PP..LQ.....A....P..........A....P...T......A....P...vC.....A....PP..P......A....P...bL.....A....P...3K.....A....P...vF.....A....P...P......A....P...Ph.....A....P...T......A....P...XE.....A....P...P......A....P...nI.....A....P...L......A....P...TF.....A....P...P......A....P...Pk.....A....PP..T......A....P...fA.....A....P...P......A....P...XO.....A....P...X......A....P...3C.....A....P...P......A....P...vf.....A....P...T......A....P...vB.....A....P.DBP...X.a..w.E...IDK.PH.A..PBS..DDL..Aj.KB.g.D..DGb..PLPA..PBnC.DBP...j.Y..QAF.P.Ivl.PH.A..fAiA.DCT...j.o..g.D...Ib..PPv..DPBbH.DBP...bPu..w.E...IzM.PH.A..PBMA..E3K.Aj.WB.g.D...FDG.PLPA..PBAC.DBP...jPG..AAC.P.Ivi.PH.A..wAI..DCT...jv+..g.D...ILH.PTfA.DPB7I.DBP...XfO..w.E...I3H.PH.A...B+..DDL..Aj.BB.g.D...G...PLPA..PBIC.DBP...jfJ..PA9BP.Ifj.PH.A..fArA.DCT...jPq..g.D...IDA.PPv..DPBnH.DBP...bvu..w.E...IXN.PH.A..PBaA.DEX..Aj.ZB.g.D...FLF.PLPA..PBeB.DBP...j.A..AAC.P.Ifg.PH.A..vAkA.DCT...jv0..g.D...InC..Tfu.DPBXI.DBP...XPb..w.E...IrK.PH.A..PBa.......+nhpq9CSL27O.B..+nxrW8C...vO......XB..fAE.g.3kP...........fOpp5p.jPk......POHhXh.................bPO..w.E...I.M.PH.A..PBt...E3K.Aj.TB.g.D...FvF.PLPA..PBzB.DBP...jPE..AAC.P.I.i.PH.A.............................................XvX..w.E...IfJ.PH.A..PBK..DDL..Aj.HB.g.D...GzK.PLPA..PBfC.DBP...j.T..PA9BP.I.l.PH.A....wA....P.....D...D.....P.....DD...I....P.....D...E.....P.....D...HB....P.....D...D.....P.....DD..MB....P.....D...C.....P.....D.........P.....D...D.....P.....D...FI....P.....D...E.....P.....D...O.....P.....D...D.....P.....D...kC....P.....D...F.....P.....D...iA....P.....D...D.....P.....D...VH....P.....D...E.....P.....DD..y.....P.....D...D.....P.....D...5B....P.....D...C.....P.....D...H.....P.....D...D.....P.....D...1I....P.....D...E.....P.....D...sA....P.....D...D.....P.....D.PBOC.DBP...jPK..PA9BP.I3j.PH.A..fArA.DCT...jfr..g.D...IPA.PPv..DPBtH.DBP..PbvA..w.E...IvN.PH.A..PBoA.DEX..AjfaB.g.D...FLF.PLPA..PBlB.DBP...j.B..AAC.P.I3g.PH.A..vA8B.DCT...jv2..g.D...I3D..Tfu.DPBdI.DBP...XPb..w.E...ILL.PH.A..PBe..DDL..AjfOB.g.D..DGj..PLPA..QBJ..DBP...jfg..QAF.f.I3m.PH.A..fA9..DCT...j.j..g.D...H7C.PPv..DPBJH.DBP...b....w.E...IrL.PH.A..PBq...E3K.AjfRB.g.D...FvF.PLPA..PBvB.DBP...jfD..AAC.P.Inh.PH.A..vA+B.DCT...jv4..g.D...I7E.PTfA.DPBpI.DBP...XvX..w.E...IDJ.PH.A..PBF..DDL..AjfFB.g.D...GTF.PLPA..PBbC.DBP...jvN..PA9BP.Ink.PH.A......vOpn5p+vDyM+Cf..fOlVbA+....7C.....DH.D.fvAP.PAC.wO.........3ipptJDEX.......zChHlH................DBP...f.u..AAC.P.IHg.PH.A..vA8..DCT...jPz..g.D...I7B..Tfu.DPBRI.DBP...X.a..w.E...ITK............................................DBP...jfb..QAF.P.IHm.PH.A..fAiA.DCT...jPp..g.D...Iv..PPv..DPBhH.DBP...bPu..w.E...IHN....A.....D.....A..fXB....D.....AA.PA.....D.....A..fH.....D.....A...A.....D.....A..f.B....D.....AA.fA.....D.....A..PA.....D.....A..fC.....D.....A..PF.....D.....A...I.....D.....A...J.....D.....A..fL.....D.....A..vM.....D.....A...P.....D.....A..vP.....D.....A..fQ.....D.....A..PR.....D.....A...S.....D.....A..fT.....D.....AA.vU.....D.....A...W.....D.....A...X.....D.....A..fY.....D.....A..fZ.....D.....A...b.....D.....A..Pc.....D.....A..fd.....D.....A..Pe.....D.....A..Pf.....D.....A.Dvg..P.KB..AvH..Dvi..P.RB..ALI..Dvk..P.XB..AjI..Dvl..P.bB..A3I..D.n..P.hB..ALJ..DPo..P.mB..ArJ..Dfq..P.uB..ALK..Dfs..P.3B..AjK..Dfv..P.FC..AnL..DPy..P.NC..AHM..Dvz..P.UC..AfM..DP1..P.ZC..ArM..Df2..P.gC..ALN..DP5..P.pC..ArN..DP6..P.tC..A7N..D.7..P.wC..APO..DP8..P.2C..AfO..Df9..P.8C..A3O.RDvE..Q.BD..BTC.QHPD.Dg.M.vDBrB.UHvf.Dw.K.PEBLL.RLvD.Tg.CBPDC7..UHvv.Hw.a.vDCLB.TLvb.Pw.SAvDDLC.SLvN.Tw.iB.ECLF.UPv3.Lw.i..ECLG.TLvT.PAACAvDCrC.ULvn.Pw.iA.XE...SLvH.Pw.yA.ECLE.SPvL.Lw.6.PECLJ.TLvX.TAAiCvDCLB.TLvb.Pw.SA.EDLD.SLvN.Tw.iB.ECLF.RTvG.Hw.I.vDEDA......7iJpt5OLwby+.H..7iJyd0O....+.....PLEPADAPvFDDPAY7C.........9nppq1QAA.F.....8fHhIB...............XQAAB.GDD.HbTP.vHw.I.PEEDD.aPP.PjQAAXfFDD.BWTPfAzAAA.jEFDL.RLPB.LQAQ.fFDD.CXTP.BDAAG............................................bQAADPGDD.PcTP.fIw.I..EEDB.ZPP.LfQAALPDDb..VTPf.vAAA.BGED.LRLPB.TQAAAvFDD.DYTP.FnAAAf...DX.....A....P...jA.....A....PP..P......A....P...jA.....A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PfFWA..hbE..nxU..fKWA..xbE..nyU..fOWA..BcE..nzU..fSWA..RcE..n0U..fXWA..pcE..31U..fbWA..5cE..H3U..fhWA..NdE..H4U..flWA..hdE..n5U..fqWA..xdE..n6U..fuWA..BeE..n7U..fyWA..ReE..n8U..f3WA..peE..39U..f7WA..5eE..H.V..fBXA..NfE..HAV..fFXA..hfE..nBV..fKXA..xfE..nCV..fOXA..BgE..nDV..fSXA..RgE..nEV..fXXA..pgE..3FV..fbXA..5gE..HHV..fhXA..NhE..HIV..flXA..hhE..nJV..fqXA..xhE..nKV..fuXA..BiE..nLV..fyXA..RiE..nMV..f3XA..piE..3NV..f7XA..5iE..3OV..f.YA..JjE..3PV..fDYA..ZjE..HRV..fJYA..tjE..HSV..fNYA..9jE..HTV..fRYA..NkE..HUV..fVYA..hkE..nVV..faYA..xkE..nWV........9nppq9CSL27O.B..9XZwE7C...vO......PVtB..YIK..jkt9TnG39Cf..fOpp5pNmE.......POHhXh................xmE..neV..f.ZA..JnE..3fV..fDZA..ZnE..HhV..fJZA..tnE..HiV..fNZA..9nE..HjV..fRZA..NoE.............................................JpE..3nV..fjZA..ZpE..HpV..fpZA..tpE..HqV..ftZA..9pE..HrV..fxZA..NqE..HsV..f1ZA..hqE...........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D3OW..f.cA..JzE..3PW..fDcA..ZzE..HRW..fJcA..tzE..HSW..fNcA..9zE..HTW..fRcA..N0E..HUW..fVcA..h0E..nVW..facA..x0E..nWW..ffcA..J1E..3XW..fjcA..Z1E..HZW..fpcA..t1E..HaW..ftcA..91E..HbW..fxcA..N2E..HcW..f1cA..h2E..ndW..f6cA..x2E..neW..f.dA..J3E..3fW..fDdA..Z3E..HhW..fJdA..t3E..HiW..fNdA..93E..HjW..fRdA..N4E..HkW..fVdA..h4E..nlW..fadA..x4E..nmW..ffdA..J5E..3nW..fjdA..Z5E..HpW..fpdA..t5E..HqW..ftdA..95E..HrW..fxdA..N6E..HsW..f1dA..h6E..ntW..f6dA..x6E..nuW..f+dA..B7E..nvW..fCeA..R7E..nwW..fHeA..p7E..3xW..fLeA..57E..3yW..fPeA..J8E..3zW..fTeA..Z8E.............+vDyM+Cf..fOlVbA+....7C......7ki..vWRB..eoI.....+.H..3ipptpreA.......zChHlH...............fzeA..Z+E..H9W..f5eA..t+E..H+W..f9eA..B.H..n.f..fC.B..R.H..nAf..fH.B..p.H..3Bf..fL.B............................................fZ.B..tAH..HGf..fd.B..BBH..nHf..fi.B..RBH..nIf..fn.B..pBH..3Jf..fr.B..5BH..3Kf..fv.B...........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....ApKH..3tf..f7BB..5KH..3uf..f.CB..JLH..3vf..fDCB..ZLH..Hxf..fJCB..tLH..Hyf..fNCB..9LH..Hzf..fRCB..NMH..H0f..fVCB..hMH..n1f..faCB..xMH..n2f..ffCB..JNH..33f..fjCB..ZNH..H5f..fpCB..tNH..H6f..ftCB..9NH..H7f..fxCB..NOH..H8f..f1CB..hOH..n9f..f6CB..xOH..n+f..f+CB..BPH..n.g..fCDB..RPH..nAg..fHDB..pPH..3Bg..fLDB..5PH..3Cg..fPDB..JQH..3Dg..fTDB..ZQH..HFg..fZDB..tQH..HGg..fdDB..BRH..nHg..fiDB..RRH..nIg..fnDB..pRH..3Jg..frDB..5RH..3Kg..fvDB..JSH..3Lg..fzDB..ZSH..HNg..f5DB..tSH..HOg..f9DB..9SH..HPg..fBEB..NTH..HQg..fFEB..hTH..nRg..fKEB..xTH..nSg..fOEB............vOLwby+.H..7Cf..vO....+.......E5F..Tnb..Pg5A....vO.B..9nppqJYg........8fHhIB...............nag..fuEB..BWH..nbg..fyEB..RWH..ncg..f3EB..pWH..3dg..f7EB..5WH..Hfg..fBFB..NXH..Hgg.............................................njg..fSFB..RYH..nkg..fXFB..pYH..3lg..fbFB..5YH..Hng..fhFB..NZH..Hog..flFB..hZH..npg............A....P..........A....PP.........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PfxHB..NiH..HMh..f1HB..hiH..nNh..f6HB..xiH..nOh..f+HB..BjH..nPh..fCIB..RjH..nQh..fHIB..pjH..3Rh..fLIB..5jH..3Sh..fPIB..JkH..3Th..fTIB..ZkH..HVh..fZIB..tkH..HWh..fdIB..BlH..nXh..fiIB..RlH..nYh..fnIB..plH..3Zh..frIB..5lH..3ah..fvIB..JmH..3bh..fzIB..ZmH..Hdh..f5IB..tmH..Heh..f9IB..BnH..nfh..fCJB..RnH..ngh..fHJB..pnH..3hh..fLJB..5nH..3ih..fPJB..JoH..3jh..fTJB..ZoH..Hlh..fZJB..toH..Hmh..fdJB..BpH..nnh..fiJB..RpH..noh..fnJB..ppH..3ph..frJB..5pH..3qh..fvJB..JqH..3rh..fzJB..ZqH..Hth..f5JB..tqH..Huh..f9JB..9qH..Hvh..fBKB..NrH..Hwh..fFKB..hrH..nxh........+nhpq9CSL27O.B..9XZwE7C...vO......vhRA..KpE..rnX..........fOpp5p5sH.......POHhXh................htH..n5h..fqKB..xtH..n6h..fuKB..BuH..n7h..fyKB..RuH..n8h..f3KB..puH..39h..f7KB..5uH.............................................tvH..HCi..fNLB..9vH..HDi..fRLB..NwH..HEi..fVLB..hwH..nFi..faLB..xwH..nGi..ffLB..JxH...........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D3pi..frNB..55H..3qi..fvNB..J6H..3ri..fzNB..Z6H..Hti..f5NB..t6H..Hui..f9NB..96H..Hvi..fBOB..N7H..Hwi..fFOB..h7H..nxi..fKOB..x7H..nyi..fOOB..B8H..nzi..fSOB..R8H..n0i..fXOB..p8H..31i..fbOB..58H..H3i..fhOB..N9H..H4i..flOB..h9H..n5i..fqOB..x9H..n6i..fuOB..B+H..n7i..fyOB..R+H..n8i..f3OB..p+H..39i..f7OB..5+H..3+i..f.PB..J.I..3.j..fDPB..Z.I..HBj..fJPB..t.I..HCj..fNPB..9.I..HDj..fRPB..NAI..HEj..fVPB..hAI..nFj..faPB..xAI..nGj..ffPB..JBI..3Hj..fjPB..ZBI..HJj..fpPB..tBI..HKj..ftPB..9BI..HLj..fxPB..NCI..HMj..f1PB..hCI..nNj..f6PB..xCI..nOj..f+PB..BDI.......vOpn5p+vDyM+Cf..fOlVbA+....7C......DoL..Pj5...Q5C..........3ipptpTQB.......zChHlH...............fdQB..BFI..nXj..fiQB..RFI..nYj..fnQB..pFI..3Zj..frQB..5FI..3aj..fvQB..JGI..3bj..fzQB............................................fCRB..RHI..ngj..fHRB..pHI..3hj..fLRB..5HI..3ij..fPRB..JII..3jj..fTRB..ZII..Hlj..fZRB...........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AJRI..3Hk..fjTB..ZRI..HJk..fpTB..tRI..HKk..ftTB..9RI..HLk..fxTB......f.....RZK7abjhlD+P0mr6KonIwOYcFn9FGonIwO75EJ9ZwO+6iyZ1YuwQNqY6y29vBOS+Hk97sOrvSbudgJ97sOrvCpJxbOaG9l8FW4KmPOIWpI9V9xIzSVmApui01.2ZgOYcFn9F2.2ZgO9NhT9VZK73SSrUguwgDog4ysoEauS+Hk9baZw1abyunu9baZw16lcRsOWFBE9F2PuptOQ43S9Nzqp6SVmApuwMzqp6isjdvux3Ml9XKoG7abf7rY9XKoG7qDC6iOgAT79F2.2ZgOVcyz9NvsV3SVmApui01.jmkOzZ1I+x1mysgOzZ1I+FGHhNhO1w5L+RLY73y7dsyuwc1IU4SbQLzuPFbd9DWDC8abHvZi9DWDC8KR24oOnwtO+FGhB8pOfcrN+Rx+y5CXGqyuw8UHA6CXGqyu0oqw9DWDC8KaBfo49DWDC8ab.ll29rwAy769QJsOmaSK+FWc5ZrO.....7iJpt5OLwby+.H..3ioEWvO....+....XDZw8KrD6Sga73uu...........9nppqB6K+N......8fHhIB...............zSVmApuwU9xIzycKOtuMkI18HEMI7abSYhM9ftff7qLdioOnKHH+FGqmysOnKHH+1ia.7SRosvuwQJZR7CUexN..........................................3CK7L8iT5y29vBOw85Ep3y29vBOnpHy8rc3a1abkubB8jbol3a4KmPOYcFn9NVaCbqE9j0Yf5abCbqE936HR4K..viO....A....P...jVr....A....PP..L+h....A....P...bYH....A....P...36P....A....P...ntO....A....PP..bvu....A....P...LrO....A....P...X0M....A....P...LVa....A....P...bVS....A....P...nLQ....A....P...DMI....A....P...3y4....A....P...7ab....A....P...LiO....A....P...DpJ....A....P...Hm0....A....P...Xt5....A....P...76K....A....PP..ngO....A....P...jzu....A....P...D2E....A....P...3CY....A....P....zu....A....P...7f+....A....P...jwK....A....P...fQe....A....P...3yd....A....P...7ab....A....PvHyBrOlqdN+F2HyBrOlSDP+5guI6CYJSzuwkQxR6S3OkzuXz229D9SI86XkcM.8qT.+3VaBr1n9bYaG5Ka+Xv49jmMG3KaIwCx9bYHT1KaMDJg9DXsQ4KapCL.9PJZR1Ka4Yyg8L75E3KarwAS9jmMG5KaSR7f8n3TL6Kau+f+8Dl4p6KaMDJg9.Uxk5KajFNx9fdBp6KaZt5497hqK66XkgM.8csE+3VaZ3H+9ftff7KazEDD+ftff7KaGNx+9T9xI7abzEDD+jrzo6KcAAwOYcFn9FGcAAwO75EJ9dK756iyZ1YuwUnWU6y29vBOzEDj97sOrvSbs92P97sOrvig8UfO7M4E8xVq+MbOe6CK7vF1w5LOe6CK7v1fMgaOAO0m8FG1w5LO.B+I9hcrNySVmApuwgcrNyycKOtuP+0w8HEMI7abUlXK9ftff76zORoOnKHH+FmgphrOnKHH+ZPPp6yAFPwui010rShOsQqL9xVdiorOZ3H+9FGjTZqO1R5A+N8iT5isjdvuwIlLd4isjdvuSYhM9DF.....+nhpq9CSL27O.B..9XZwE7C...vO....jv909JCss4i8sMlu..........fOpp5pCp298B....POHhXh................55ysoEau77Dz9bYHT3abiCl49DkiO463fYtOYcFn9F23fYtO+aXx9ZOma6CGlLtuiUV1.7GiU7iascxKA1iW1B..........................................9DMIw2abk8na9baZw1KonIoO2lVr8FGWDCqO2lVr8txpA6yFZ+duwsejR6CPkbgu6Goz9HxYM4Ka6Goz93ksc...nYD...P.....D...+PF...P.....DD..F2K...P.....D...OkL...P.....D...9vB...P.....D...ZaG...P.....DD..8rK...P.....D...9NF...P.....D...ctM...P.....D...9rE...P.....D...p7K...P.....D...m7B...P.....D...93E...P.....D...K4K...P.....D...N9D...P.....D...2lF...P.....D...wwE...P.....D...9rA...P.....D...W3K...P.....D...QJM...P.....DD..dYK...P.....D...9FG...P.....D...o6C...P.....D...9vB...P.....D...F8M...P.....D...e6C...P.....D...8dB...P.....D...ZIM...P.....D...6EJ...P.....D...+xF...P.....DLVYaCveLVwOt01IuDXOdYaG+xlOMBiOdYaG+xlOMBiOx+yR9FmOMBiO1+FF9FkiO4SzjDeuwU1it4ysoEaujhlj9baZw1abbQLr9baZw16JqFrOan868F29QJsO.UxE9tejR6iHm0jursejR6iW11wurglQE7iW11wurglQE7CYJSjuwglQE7yjwabuX4d59baZwzabg+Tx97sOrvC3VKoOe6CK7DmQeeiOe6CK7nsc32y29vRuwcxKA1ytFGbum7Rf8.JNE46XsIjMz5ydgpxurQJZR5SzqpyurAk8n4ydgpxurke9q2ydgpxurcYaG5CpJxzurgzcd5CpJxzurwtrs6ydgpxuiUF2.7GiU7iascxKA1iW11wur4Siv3iW11wur4Siv3i7+rjuw4Siv3i8ugguQ43S9DMIw2abk8na9baZw1KonIoO2lVr8FGWDCqO2lVr8txpA6yFZ+duwsejR6CPkbgu6Goz9HxYM4Ka6Goz93ksc7KanYTA+3ksc7KanYTA+PlxD4abnYTA+LY7F2KVtmtO2B....vOpn5p+vDyM+Cf..fOlVbA+....7C....91oMOr3y287xIw0K..........3ipptZQ9NVa.....zChHlH...............fOjoLQ+FWUq+kOlSDP+V05e4i4pmyuwU05e4y4PNyu8wfS9DSFu7abk1BO9rWnp76oEKhO6EpJ+FWpckfO6EpJ...........................................Q.8aYO5dOjoLQ+FWpckfOg+TR+dZwh3S3Okzui0lVfasOg+TR+FWVTNtOg+TR+V7fr6CYJSzuwEyb06i4DAzu..P89....D.....A...jy....D.....AA.PVTB...D.....A..vdgB...D.....A..vueA...D.....A...t9....D.....AA.PN+B...D.....A..PHAC...D.....A..P3OA...D.....A..vXkA...D.....A..vn9....D.....A.........D.....A...aZA...D.....A..PB6....D.....A..fsc....D.....A...a9B...D.....A..fA+....D.....A..vVr....D.....A..vurA...D.....A..PPPB...D.....AA.fE1C...D.....A..furA...D.....A..PrNA...D.....A..fyZB...D.....A..vurA...D.....A..fYt....D.....A..fKH....D.....A..v8NB...D.....A..fOAC...D.....A..fuwA...D.....AcHmU97EpJ3abs92P97EpJ3KHhNhOHvZC9NVafH5H9bPic6KafH5H9DMIw4abH2.P9fRHt46xpAkOnDha9FWfH5nOnDha9tCLf5yHfOnuwQ+0w5SruBouzecr9TYhs5abzecr97Ind6a2lVjOeBp29FGI+OiOeBp29Bhni3yAM1suiU12.zYHw6iasEXsQ4yDaU5trMNXl4CI+OauwI69C5ibolZuPKCg9HWpo1abv6hq9HWpo1K7t3pOK94O9FG7t3pOPrBV9pPup5S94uluwQxRm5i3H+mukDOn9ffTG5abkbol9.5uN5KYcFnOCgRn9FGQGEkOmC4r9RzQQ4SCNeruwQzQQ4i8n6ruogib9zHrb6abGSYh9PBdp66wTlnOL+h99F2wTlnOtOrB+ZoSW4i6Cqvuw041z3i6CqvunysI97kSD7abx3MF9DZr66qLdigOMkI19xlLdigOukG24vVmaSSOukG24vVmaSSOQ1P69FWmaSSO88sB+R+0w1y.2Zwuw0PnD3ShNJxurwAS9jnih7abEogi.....7iJpt5OLwby+.H..3ioEWvO....+....TZb+Jve9vaKq668QC..........9nppq160d6K.....8fHhIB...............3iG9lruwcmmf5i6Vdru6sOr9zFZ+5abEmh49fasj5awoXtOjoLQ9FWwoXtOrwAy8FkiO6y6hqSuw4s735S4Km...........................................rtOt0Fri0oOC1DN8FGpQXoODC6q7NB3C5CoUV8twoSWi4S4Km.Oo86N9T9xIvSbACn38T9xIvyx8zXOsXQ47FG..7MO....A....P...Xfu....A....PP..7Ar....A....P...X+a....A....P...DWe....A....P...3iK....A....PP..zqu....A....P...L+B....A....P...f7r....A....P...D2U....A....P...3SX....A....P...ntu....A....P...Tcy....A....P...n8a....A....P...D2W....A....P...3i7....A....P...jvt....A....P...7Tx....A....P...rbB....A....P...DI3....A....PP..3C1....A....P...z6X....A....P....Tb....A....P...7In....A....P...3qs....A....P...bfO....A....P...nYu....A....P...fCl....A....P...3yh....A....P...Hxu....A....PvuroBiQ5yhLKvuiUV3.7OFq6iasA6Xc5yfMgSuwgZDV5Cwv9Jui.9f9PZkUuab5z0X9T9xIvSZ+tiOkubB7DWv.JdOkubB7rbOM1SKVTNuwU05eyyjDOXuUs927.fVF3abUs927zgbV4a.e.aOBRcf9F2kgPgO1+Fl9VjYA5i8ugouw022J5i8ugouap7k93RMU5abap7k9fo485qgphjOXZdu9FWEyufOXZdu9pQEF2SAHOqurw2jW1yAM1suwckUC3SXlqtuNx+S9Dl4p6abU4Km9Dl4p6q1IUqOnRtz9F2WU2rOuKtt99U0M6yFZ+lur8U0M6yx8zfuw8U0M6yIbQTuQlE39H+OKyabjyZ19T9xIr6X9EsOXGqS6DW3OkrOkubB7Hwv95S4Km.OwopHy5S4Km.OQBdp9T9xIvab24In9fcrNyKri0oOC1DN8NVaWiCl9j3AY4ab.Emh97Ind4acMNnOeBpW9FmsjdfOeBpW9ZKoG3CmVrfuwYKoG3S5nnYuQ43S9jNJZ1abWiCl9jNJZ1603fI.....+nhpq9CSL27O.B..9XZwE7C...vO....Bv7h9f3Dr86Up4CZ..........fOpp5pvNVm9.....POHhXh................6FmNcMlOkubB7j1u63S4Km.OwELfh2S4Km.OK2Si8zhEkyabUs927LIwC1aUq+MO.nkA9FWUq+MOcHmU9FvGvB..........................................JeoOtTSk9F2lJeoOXZdu9ZnpH4Cll2quwUw7K3Cll2quZTgw8T.xy5Ka7M4k8bPic6abWY0.9Dl4p6qi7+jOgA..9FG...P.....D...05C...P.....DD..hqK...P.....D...eUM...P.....D...9bB...P.....D...7FG...P.....DD..Q6C...P.....D...Km....P.....D...pJB...P.....D...kuL...P.....D...7BK...P.....D...3fI...P.....D...eBJ...P.....D...wYK...P.....D...9vI...P.....D...Z1K...P.....D...3fI...P.....D...22B...P.....D...+xF...P.....D...MgC...P.....DD..KxL...P.....D...+xF...P.....D...dUM...P.....D...XrN...P.....D...8FG...P.....D...C5C...P.....D...Km....P.....D...ACH...P.....D...sXA...P.....D...8VE...P.....DT05eySGxYkuA7Ar8HH0A5abWFBE9X+aX5aQlEnO1+Fl9FWeeqnO1+Fl9toxW5iK0TouwsoxW5Cll2quFpJR9fo485abUL+B9fo485qFUXbOEf7r9xFeSdYOGzX29F2UVMfOgYt595H+O4SXlqtuwUkub5SXlqtuZmTs9fJ4R6abeUcy97t3556WU2rOan8a9x1WU2rOK2SC9F2WU2rOmvEQ8FYVf6i7+rLuwQNqY6S4Kmvti4Wz9fcrNsSbg+Tx9T9xIviDC6qOkubB7DmphLqOkubB7DI3o5S4Km.uwcmmf5C1w5LuvNVm9LXS3z6XscMNX5ShGjkuwATbJ5ymf5ku003f97Ind4ab1R5A97Ind4qsjdfObZwB9FmsjdfOoihl8FkiO4S5nnYuwcMNX5S5nnYuWiCl9H88s36XsMIwC3ygi7uur8EpJ1ygi7uuwIVAa1igWuvunpHy8LfhS7abu+f+8.HOa7KHhNhO.xyF+FGn3TjO.xyF+Bxyl4CdWbwuwAsLD5CbxKwur8Nh9.m7R7abnFgk9.....vOpn5p+vDyM+Cf..fOlVbA+....7C...ffC86BgcuOm1ab+V...........3ippt5H+6ab.....zChHlH...............fOTu8A+95Ep3C0aevuwo0rS3C0aevuSR7.9b3H+66XkQN.+iw593VavNVm9LXS3zabnFgk9PLrux6HfOnOjVY0..........................................fEkyabUs927LIwC1aUq+MO.nkA9FWUq+MOcHmU9FvGv1ifTGnuwcYHT3i8ugouEYVf9X+aX5ab88sh9X+aX56l..fOt....D.....A..Pu9B...D.....AA.v7K....D.....A...xyB...D.....A..PbWA...D.....A..fOgA...D.....AA.f59B...D.....A..P0MC...D.....A..f1uA...D.....A..PbeA...D.....A..fOxC...D.....A..PB6B...D.....A..vSIC...D.....A..vxI....D.....A..PjfC...D.....A..fOXC...D.....A..PuiA...D.....A...PwA...D.....A..vmfB...D.....A..fu1B...D.....AA.vA9....D.....A..fl8B...D.....A...NXB...D.....A..POwC...D.....A..PH+B...D.....A..fni....D.....A..PER....D.....A..PRCC...D.....A..POKB...D.....A..vuwA...D.....A4m+Z8DDQG7abfH5H8b+tK7KHhNRO1WgD+FGHhNRO1+FF+le9q0yb0ywuwkNJZ1S75ExukiOy8Dudg76XsA7t35S75Exuw86aE6S75Exuq7ky9LW8b7abV5z09X+aX7qkNcsO1WgD+FmkNcsO2u6B+txWN6SPDcvuw86aE6yhLKvu.uKt9rHyB7ab.e.q9rHyB7aw7KpOAQzA+FmxwmoO2u6B+pb7Y5i8UHwuwob7Y5i8ugwuEyun9LW8b7ab.e.q9Dudg7Kv6hqOwqWH+NVYkCv+XrtOt0Fri0oOC1DN8FGpQXoODC6q7NB3C5CoUV8twoSWi4S4Km.Oo86N9T9xIvSbACn38T9xIvyx8zXOsXQ47FWUq+MOSR7f8V05eyC.ZYfuwU05eySGxYkuA7Ar8HH0A5abWFBE9X+aX5aQlEnO1+Fl9FWeeqnO1+Fl9toxW5iK0TouwsoxW5Cll2quFpJR9fo485abUL+B9fo485qFUXbOEf7r9xFeSdYOGzX29F2UVMfOgYt595H+O4SXlqtuwUkub5SX.....7iJpt5OLwby+.H..3ioEWvO....+....zb0ewVC8rrOU+Ub9B..........9nppqtLuwQN.....8fHhIB...............T9xIviDC6qOkubB7DmphLqOkubB7DI3o5S4Km.uwcmmf5C1w5LuvNVm9LXS3z6XscMNX5ShGjkuwATbJ5ymf5E..........................................fhl8FkiO4S5nnYuwcMNX5S5nnYuWiCl9H88s36XsYbcY4iFnKvuw8sOr3iFnKvuvgIC9nnII7abCPd18rOYO7K..jcO....A....P...r1H....A....PP..3fY....A....P....hU....A....P...7Kb....A....P...HpO....A....PP..jwu....A....P...Huj....A....P...nA5....A....P...LVa....A....P...TgQ....A....P...nQH....A....P...TlX....A....P...3C2....A....P...7ab....A....P...LnO....A....P...j8H....A....P...Tl4....A....P...3Sh....A....P...3ku....A....PP..7R.....A....P...XwB....A....P...LLF....A....P...3S5....A....P...36X....A....P...7ug....A....P...3CB....A....P...zab....A....P...XvO....A....P...Trn....A....PPurctMs7ShZWQuwMJGe7S4Km.OKxr.+T9xIvSbMBK29T9xIvSSkurOHcmm7xFpJxrO.....vlKoGpO.....D2nb7oOFDjZ7lNJZ5SmaSSuwkzvQ5ykgPIuCp2d9zIB3sabCPdV9T9xIvi1IUiOkubB7DGe.qcOkubB7.fVF1iuiHMuw8sOrxiqRAXue6Cq7.fVF3abe6Cq7DYVf4a0T4aO.nkg9F2mysgO1+Fl9xuU74i8ugouwonII5i8ugouS+Hk93RMU5abS+Hk9XaG9568zHjO11gu9FG.ZYfO11gu9xeJ41SAHOqur8EpJ1yAM1suwAYv42SXlqtu+aXR9Dl4p6abtjdn9Dl4p6anDhqOx+yx9F2ZPisOgYt59l5.C7SXlqtuwAHOa7SXlqtu6EpJ+r4PN6abxRmN+vEwv5qrzoyOpCLf9FmrzoyO4m+Z9ReL37S2lVjui0l4wMvOXZdu9FW+h9tOXZdu9ZCse6y3m+puwkdnP6ShNJpuQb2x9naSK5KaP81G+naSK5abU4KG+fo485q4wMPRtkFcfTjcuwVczk1atAvmrGsOsqD09xF..........fk.........A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7....................D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7.........."
										}
,
										"snapshotlist" : 										{
											"current_snapshot" : 0,
											"entries" : [ 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "GRM Evolution",
													"origin" : "Evolution.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 0,
													"snapshot" : 													{
														"pluginname" : "Evolution.vst",
														"plugindisplayname" : "GRM Evolution",
														"pluginsavedname" : "GRM Evolution",
														"pluginsaveduniqueid" : 0,
														"version" : 1,
														"isbank" : 0,
														"isbase64" : 1,
														"blob" : "17828.CMlaKA....fQPMDZ....AbjbEYG.CP......AjjaoQGHEY2arUGco8la.....................TDZL....bjbsAxQxUjc....lguP..P..........zChHl3Opn5p+.H..7CSL2rOlVbA..........vO....+......Q.LD.FAPQ.fD.GA..........9nppqd..........................Q....3Q...fG....7D...fOwBQ3A.j..J.PB.j..J.PB.j..I.fB.n..I.PB.j..J.PB.n..I.PB.j..I.PB............................................J.PB.n..J.PB.j..I.PB.j..I.fB.j..J.PB.n..K.fB.n..J.fB.j..I..B.b..F..B.n..J.fB.n..J.fB..........P.....B....L.....A....E....X....vA..fB.....D.....A..fB.....D.....AA.fA.....D.....A..PB.....D.....A..PB.....D.....A..fB.....D.....AA.fB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..PB.....D.....A..vA.....D.....A..PB.....D.....A..PB.....D.....AA.PB.....D.....A..fB.....D.....A..fB.....D.....A..fB.....D.....A..PB.....D.....A..fB.....D.....A..PB.....D.....A..PB.....D.....A..PB.....D.....A..vA.....D.....AK..C.r..L.fB.v..N..C.r..M..C.z..L..C.v..K..C.r..M.fB.........j..I..A.L..H.PA.b..E.PA.T..E.PA.X..E.PA.T..E.PA.T..D..A.P..E..A.T..D.PA.3BP..fL.A..F.PA.XA.W..F.nA.b..B.n..K..C.z..N..D.DA.R.vD.PA.U.PF.rA.c.vC.j..G.v..P..K..C.z..O.PD.HA.S..E.TA.V..F.nA.a..G.zA.e..H.HB.i.vI.nB.q..K.zB.t.vK.DC.y.fN.rC.MAfS..E.UAfU.nE.aAvW.PF.oAva.HG.3APd..H.BBvf.XH.HBPh.nH.MBfi..I.QB.k.TI.VBfl.zI.eBPn.PJ.lB.p.jJ.pB.q.zJ.vBPr.HK.zBPs.bK.5Bvt..L.ACvv.PL.ECvw.fL.ICvx.vL.OC.z.DM.TCf0.bM.bCP2.7M.fCf3.PN.kCf4.bN.nC.6.HO.yCf8.jO.6C.+.7O.HDf..T..I.fB.3..P.vE.jA.d.PH.PB......7iJpt5OLwby+.H..3ioEWvO....+......Q.LD.FAPQ.fD.GA..........9nppqJE.SA......8fHhIB...............XF.mA.Z.nF.qAfa..G.yA.c.TG.1Avc.nG.6A.e.zG.9Ave.DH.DBPg.bH.KB.i.7H.RBvj.bI.XBPl.rI.bB...........................................zL.NCfz.LM.UC.1.jM.ZCv1.3M.gCv3.jN.pCv5.zN.tCv6..O.wC.8.TO.2C.9.nO.8Cf+.3P.cDPM.nP.LD...jQ.....A....P...DQ.....A....PP..LQ.....A....P..........A....P...T......A....P...vC.....A....PP..P......A....P...bL.....A....P...3K.....A....P...vF.....A....P...P......A....P...Ph.....A....P...T......A....P...XE.....A....P...P......A....P...nI.....A....P...L......A....P...TF.....A....P...P......A....P...Pk.....A....PP..T......A....P...fA.....A....P...P......A....P...XO.....A....P...X......A....P...3C.....A....P...P......A....P...vf.....A....P...T......A....P...vB.....A....P.DBP...X.a..w.E...IDK.PH.A..PBS..DDL..Aj.KB.g.D..DGb..PLPA..PBnC.DBP...j.Y..QAF.P.Ivl.PH.A..fAiA.DCT...j.o..g.D...Ib..PPv..DPBbH.DBP...bPu..w.E...IzM.PH.A..PBMA..E3K.Aj.WB.g.D...FDG.PLPA..PBAC.DBP...jPG..AAC.P.Ivi.PH.A..wAI..DCT...jv+..g.D...ILH.PTfA.DPB7I.DBP...XfO..w.E...I3H.PH.A...B+..DDL..Aj.BB.g.D...G...PLPA..PBIC.DBP...jfJ..PA9BP.Ifj.PH.A..fArA.DCT...jPq..g.D...IDA.PPv..DPBnH.DBP...bvu..w.E...IXN.PH.A..PBaA.DEX..Aj.ZB.g.D...FLF.PLPA..PBeB.DBP...j.A..AAC.P.Ifg.PH.A..vAkA.DCT...jv0..g.D...InC..Tfu.DPBXI.DBP...XPb..w.E...IrK.PH.A..PBa.......+nhpq9CSL27O.B..+nxrW8C...vO......XB..fAE.g.3kP...........fOpp5p.jPk......POHhXh.................bPO..w.E...I.M.PH.A..PBt...E3K.Aj.TB.g.D...FvF.PLPA..PBzB.DBP...jPE..AAC.P.I.i.PH.A.............................................XvX..w.E...IfJ.PH.A..PBK..DDL..Aj.HB.g.D...GzK.PLPA..PBfC.DBP...j.T..PA9BP.I.l.PH.A....wA....P.....D...D.....P.....DD...I....P.....D...E.....P.....D...HB....P.....D...D.....P.....DD..MB....P.....D...C.....P.....D.........P.....D...D.....P.....D...FI....P.....D...E.....P.....D...O.....P.....D...D.....P.....D...kC....P.....D...F.....P.....D...iA....P.....D...D.....P.....D...VH....P.....D...E.....P.....DD..y.....P.....D...D.....P.....D...5B....P.....D...C.....P.....D...H.....P.....D...D.....P.....D...1I....P.....D...E.....P.....D...sA....P.....D...D.....P.....D.PBOC.DBP...jPK..PA9BP.I3j.PH.A..fArA.DCT...jfr..g.D...IPA.PPv..DPBtH.DBP..PbvA..w.E...IvN.PH.A..PBoA.DEX..AjfaB.g.D...FLF.PLPA..PBlB.DBP...j.B..AAC.P.I3g.PH.A..vA8B.DCT...jv2..g.D...I3D..Tfu.DPBdI.DBP...XPb..w.E...ILL.PH.A..PBe..DDL..AjfOB.g.D..DGj..PLPA..QBJ..DBP...jfg..QAF.f.I3m.PH.A..fA9..DCT...j.j..g.D...H7C.PPv..DPBJH.DBP...b....w.E...IrL.PH.A..PBq...E3K.AjfRB.g.D...FvF.PLPA..PBvB.DBP...jfD..AAC.P.Inh.PH.A..vA+B.DCT...jv4..g.D...I7E.PTfA.DPBpI.DBP...XvX..w.E...IDJ.PH.A..PBF..DDL..AjfFB.g.D...GTF.PLPA..PBbC.DBP...jvN..PA9BP.Ink.PH.A......vOpn5p+vDyM+Cf..fOlVbA+....7C.....DH.D.fvAP.PAC.wO.........3ipptJDEX.......zChHlH................DBP...f.u..AAC.P.IHg.PH.A..vA8..DCT...jPz..g.D...I7B..Tfu.DPBRI.DBP...X.a..w.E...ITK............................................DBP...jfb..QAF.P.IHm.PH.A..fAiA.DCT...jPp..g.D...Iv..PPv..DPBhH.DBP...bPu..w.E...IHN....A.....D.....A..fXB....D.....AA.PA.....D.....A..fH.....D.....A...A.....D.....A..f.B....D.....AA.fA.....D.....A..PA.....D.....A..fC.....D.....A..PF.....D.....A...I.....D.....A...J.....D.....A..fL.....D.....A..vM.....D.....A...P.....D.....A..vP.....D.....A..fQ.....D.....A..PR.....D.....A...S.....D.....A..fT.....D.....AA.vU.....D.....A...W.....D.....A...X.....D.....A..fY.....D.....A..fZ.....D.....A...b.....D.....A..Pc.....D.....A..fd.....D.....A..Pe.....D.....A..Pf.....D.....A.Dvg..P.KB..AvH..Dvi..P.RB..ALI..Dvk..P.XB..AjI..Dvl..P.bB..A3I..D.n..P.hB..ALJ..DPo..P.mB..ArJ..Dfq..P.uB..ALK..Dfs..P.3B..AjK..Dfv..P.FC..AnL..DPy..P.NC..AHM..Dvz..P.UC..AfM..DP1..P.ZC..ArM..Df2..P.gC..ALN..DP5..P.pC..ArN..DP6..P.tC..A7N..D.7..P.wC..APO..DP8..P.2C..AfO..Df9..P.8C..A3O.RDvE..Q.BD..BTC.QHPD.Dg.M.vDBrB.UHvf.Dw.K.PEBLL.RLvD.Tg.CBPDC7..UHvv.Hw.a.vDCLB.TLvb.Pw.SAvDDLC.SLvN.Tw.iB.ECLF.UPv3.Lw.i..ECLG.TLvT.PAACAvDCrC.ULvn.Pw.iA.XE...SLvH.Pw.yA.ECLE.SPvL.Lw.6.PECLJ.TLvX.TAAiCvDCLB.TLvb.Pw.SA.EDLD.SLvN.Tw.iB.ECLF.RTvG.Hw.I.vDEDA......7iJpt5OLwby+.H..7iJyd0O....+.....PLEPADAPvFDDPAY7C.........9nppq1QAA.F.....8fHhIB...............XQAAB.GDD.HbTP.vHw.I.PEEDD.aPP.PjQAAXfFDD.BWTPfAzAAA.jEFDL.RLPB.LQAQ.fFDD.CXTP.BDAAG............................................bQAADPGDD.PcTP.fIw.I..EEDB.ZPP.LfQAALPDDb..VTPf.vAAA.BGED.LRLPB.TQAAAvFDD.DYTP.FnAAAf...DX.....A....P...jA.....A....PP..P......A....P...jA.....A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PfFWA..hbE..nxU..fKWA..xbE..nyU..fOWA..BcE..nzU..fSWA..RcE..n0U..fXWA..pcE..31U..fbWA..5cE..H3U..fhWA..NdE..H4U..flWA..hdE..n5U..fqWA..xdE..n6U..fuWA..BeE..n7U..fyWA..ReE..n8U..f3WA..peE..39U..f7WA..5eE..H.V..fBXA..NfE..HAV..fFXA..hfE..nBV..fKXA..xfE..nCV..fOXA..BgE..nDV..fSXA..RgE..nEV..fXXA..pgE..3FV..fbXA..5gE..HHV..fhXA..NhE..HIV..flXA..hhE..nJV..fqXA..xhE..nKV..fuXA..BiE..nLV..fyXA..RiE..nMV..f3XA..piE..3NV..f7XA..5iE..3OV..f.YA..JjE..3PV..fDYA..ZjE..HRV..fJYA..tjE..HSV..fNYA..9jE..HTV..fRYA..NkE..HUV..fVYA..hkE..nVV..faYA..xkE..nWV........9nppq9CSL27O.B..9XZwE7C...vO......PVtB..YIK..jkt9TnG39Cf..fOpp5pNmE.......POHhXh................xmE..neV..f.ZA..JnE..3fV..fDZA..ZnE..HhV..fJZA..tnE..HiV..fNZA..9nE..HjV..fRZA..NoE.............................................JpE..3nV..fjZA..ZpE..HpV..fpZA..tpE..HqV..ftZA..9pE..HrV..fxZA..NqE..HsV..f1ZA..hqE...........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D3OW..f.cA..JzE..3PW..fDcA..ZzE..HRW..fJcA..tzE..HSW..fNcA..9zE..HTW..fRcA..N0E..HUW..fVcA..h0E..nVW..facA..x0E..nWW..ffcA..J1E..3XW..fjcA..Z1E..HZW..fpcA..t1E..HaW..ftcA..91E..HbW..fxcA..N2E..HcW..f1cA..h2E..ndW..f6cA..x2E..neW..f.dA..J3E..3fW..fDdA..Z3E..HhW..fJdA..t3E..HiW..fNdA..93E..HjW..fRdA..N4E..HkW..fVdA..h4E..nlW..fadA..x4E..nmW..ffdA..J5E..3nW..fjdA..Z5E..HpW..fpdA..t5E..HqW..ftdA..95E..HrW..fxdA..N6E..HsW..f1dA..h6E..ntW..f6dA..x6E..nuW..f+dA..B7E..nvW..fCeA..R7E..nwW..fHeA..p7E..3xW..fLeA..57E..3yW..fPeA..J8E..3zW..fTeA..Z8E.............+vDyM+Cf..fOlVbA+....7C......7ki..vWRB..eoI.....+.H..3ipptpreA.......zChHlH...............fzeA..Z+E..H9W..f5eA..t+E..H+W..f9eA..B.H..n.f..fC.B..R.H..nAf..fH.B..p.H..3Bf..fL.B............................................fZ.B..tAH..HGf..fd.B..BBH..nHf..fi.B..RBH..nIf..fn.B..pBH..3Jf..fr.B..5BH..3Kf..fv.B...........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....ApKH..3tf..f7BB..5KH..3uf..f.CB..JLH..3vf..fDCB..ZLH..Hxf..fJCB..tLH..Hyf..fNCB..9LH..Hzf..fRCB..NMH..H0f..fVCB..hMH..n1f..faCB..xMH..n2f..ffCB..JNH..33f..fjCB..ZNH..H5f..fpCB..tNH..H6f..ftCB..9NH..H7f..fxCB..NOH..H8f..f1CB..hOH..n9f..f6CB..xOH..n+f..f+CB..BPH..n.g..fCDB..RPH..nAg..fHDB..pPH..3Bg..fLDB..5PH..3Cg..fPDB..JQH..3Dg..fTDB..ZQH..HFg..fZDB..tQH..HGg..fdDB..BRH..nHg..fiDB..RRH..nIg..fnDB..pRH..3Jg..frDB..5RH..3Kg..fvDB..JSH..3Lg..fzDB..ZSH..HNg..f5DB..tSH..HOg..f9DB..9SH..HPg..fBEB..NTH..HQg..fFEB..hTH..nRg..fKEB..xTH..nSg..fOEB............vOLwby+.H..7Cf..vO....+.......E5F..Tnb..Pg5A....vO.B..9nppqJYg........8fHhIB...............nag..fuEB..BWH..nbg..fyEB..RWH..ncg..f3EB..pWH..3dg..f7EB..5WH..Hfg..fBFB..NXH..Hgg.............................................njg..fSFB..RYH..nkg..fXFB..pYH..3lg..fbFB..5YH..Hng..fhFB..NZH..Hog..flFB..hZH..npg............A....P..........A....PP.........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PP.........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....P..........A....PfxHB..NiH..HMh..f1HB..hiH..nNh..f6HB..xiH..nOh..f+HB..BjH..nPh..fCIB..RjH..nQh..fHIB..pjH..3Rh..fLIB..5jH..3Sh..fPIB..JkH..3Th..fTIB..ZkH..HVh..fZIB..tkH..HWh..fdIB..BlH..nXh..fiIB..RlH..nYh..fnIB..plH..3Zh..frIB..5lH..3ah..fvIB..JmH..3bh..fzIB..ZmH..Hdh..f5IB..tmH..Heh..f9IB..BnH..nfh..fCJB..RnH..ngh..fHJB..pnH..3hh..fLJB..5nH..3ih..fPJB..JoH..3jh..fTJB..ZoH..Hlh..fZJB..toH..Hmh..fdJB..BpH..nnh..fiJB..RpH..noh..fnJB..ppH..3ph..frJB..5pH..3qh..fvJB..JqH..3rh..fzJB..ZqH..Hth..f5JB..tqH..Huh..f9JB..9qH..Hvh..fBKB..NrH..Hwh..fFKB..hrH..nxh........+nhpq9CSL27O.B..9XZwE7C...vO......vhRA..KpE..rnX..........fOpp5p5sH.......POHhXh................htH..n5h..fqKB..xtH..n6h..fuKB..BuH..n7h..fyKB..RuH..n8h..f3KB..puH..39h..f7KB..5uH.............................................tvH..HCi..fNLB..9vH..HDi..fRLB..NwH..HEi..fVLB..hwH..nFi..faLB..xwH..nGi..ffLB..JxH...........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....DD........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D.........P.....D3pi..frNB..55H..3qi..fvNB..J6H..3ri..fzNB..Z6H..Hti..f5NB..t6H..Hui..f9NB..96H..Hvi..fBOB..N7H..Hwi..fFOB..h7H..nxi..fKOB..x7H..nyi..fOOB..B8H..nzi..fSOB..R8H..n0i..fXOB..p8H..31i..fbOB..58H..H3i..fhOB..N9H..H4i..flOB..h9H..n5i..fqOB..x9H..n6i..fuOB..B+H..n7i..fyOB..R+H..n8i..f3OB..p+H..39i..f7OB..5+H..3+i..f.PB..J.I..3.j..fDPB..Z.I..HBj..fJPB..t.I..HCj..fNPB..9.I..HDj..fRPB..NAI..HEj..fVPB..hAI..nFj..faPB..xAI..nGj..ffPB..JBI..3Hj..fjPB..ZBI..HJj..fpPB..tBI..HKj..ftPB..9BI..HLj..fxPB..NCI..HMj..f1PB..hCI..nNj..f6PB..xCI..nOj..f+PB..BDI.......vOpn5p+vDyM+Cf..fOlVbA+....7C......DoL..Pj5...Q5C..........3ipptpTQB.......zChHlH...............fdQB..BFI..nXj..fiQB..RFI..nYj..fnQB..pFI..3Zj..frQB..5FI..3aj..fvQB..JGI..3bj..fzQB............................................fCRB..RHI..ngj..fHRB..pHI..3hj..fLRB..5HI..3ij..fPRB..JII..3jj..fTRB..ZII..Hlj..fZRB...........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AA........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....A.........D.....AJRI..3Hk..fjTB..ZRI..HJk..fpTB..tRI..HKk..ftTB..9RI..HLk..fxTB......f.....RZK7abjhlD+P0mr6KonIwOYcFn9FGonIwO75EJ9ZwO+6iyZ1YuwQNqY6y29vBOS+Hk97sOrvSbudgJ97sOrvCpJxbOaG9l8FW4KmPOIWpI9V9xIzSVmApui01.2ZgOYcFn9F2.2ZgO9NhT9VZK73SSrUguwgDog4ysoEauS+Hk9baZw1abyunu9baZw16lcRsOWFBE9F2PuptOQ43S9Nzqp6SVmApuwMzqp6isjdvux3Ml9XKoG7abf7rY9XKoG7qDC6iOgAT79F2.2ZgOVcyz9NvsV3SVmApui01.jmkOzZ1I+x1mysgOzZ1I+FGHhNhO1w5L+RLY73y7dsyuwc1IU4SbQLzuPFbd9DWDC8abHvZi9DWDC8KR24oOnwtO+FGhB8pOfcrN+Rx+y5CXGqyuw8UHA6CXGqyu0oqw9DWDC8KaBfo49DWDC8ab.ll29rwAy769QJsOmaSK+FWc5ZrO.....7iJpt5OLwby+.H..3ioEWvO....+....XDZw8KrD6Sga73uu...........9nppqB6K+N......8fHhIB...............zSVmApuwU9xIzycKOtuMkI18HEMI7abSYhM9ftff7qLdioOnKHH+FGqmysOnKHH+1ia.7SRosvuwQJZR7CUexN..........................................3CK7L8iT5y29vBOw85Ep3y29vBOnpHy8rc3a1abkubB8jbol3a4KmPOYcFn9NVaCbqE9j0Yf5abCbqE936HR4K..viO....A....P...jVr....A....PP..L+h....A....P...bYH....A....P...36P....A....P...ntO....A....PP..bvu....A....P...LrO....A....P...X0M....A....P...LVa....A....P...bVS....A....P...nLQ....A....P...DMI....A....P...3y4....A....P...7ab....A....P...LiO....A....P...DpJ....A....P...Hm0....A....P...Xt5....A....P...76K....A....PP..ngO....A....P...jzu....A....P...D2E....A....P...3CY....A....P....zu....A....P...7f+....A....P...jwK....A....P...fQe....A....P...3yd....A....P...7ab....A....PvHyBrOlqdN+F2HyBrOlSDP+5guI6CYJSzuwkQxR6S3OkzuXz229D9SI86XkcM.8qT.+3VaBr1n9bYaG5Ka+Xv49jmMG3KaIwCx9bYHT1KaMDJg9DXsQ4KapCL.9PJZR1Ka4Yyg8L75E3KarwAS9jmMG5KaSR7f8n3TL6Kau+f+8Dl4p6KaMDJg9.Uxk5KajFNx9fdBp6KaZt5497hqK66XkgM.8csE+3VaZ3H+9ftff7KazEDD+ftff7KaGNx+9T9xI7abzEDD+jrzo6KcAAwOYcFn9FGcAAwO75EJ9dK756iyZ1YuwUnWU6y29vBOzEDj97sOrvSbs92P97sOrvig8UfO7M4E8xVq+MbOe6CK7vF1w5LOe6CK7v1fMgaOAO0m8FG1w5LO.B+I9hcrNySVmApuwgcrNyycKOtuP+0w8HEMI7abUlXK9ftff76zORoOnKHH+FmgphrOnKHH+ZPPp6yAFPwui010rShOsQqL9xVdiorOZ3H+9FGjTZqO1R5A+N8iT5isjdvuwIlLd4isjdvuSYhM9DF.....+nhpq9CSL27O.B..9XZwE7C...vO....jv909JCss4i8sMlu..........fOpp5pCp298B....POHhXh................55ysoEau77Dz9bYHT3abiCl49DkiO463fYtOYcFn9F23fYtO+aXx9ZOma6CGlLtuiUV1.7GiU7iascxKA1iW1B..........................................9DMIw2abk8na9baZw1KonIoO2lVr8FGWDCqO2lVr8txpA6yFZ+duwsejR6CPkbgu6Goz9HxYM4Ka6Goz93ksc...nYD...P.....D...+PF...P.....DD..F2K...P.....D...OkL...P.....D...9vB...P.....D...ZaG...P.....DD..8rK...P.....D...9NF...P.....D...ctM...P.....D...9rE...P.....D...p7K...P.....D...m7B...P.....D...93E...P.....D...K4K...P.....D...N9D...P.....D...2lF...P.....D...wwE...P.....D...9rA...P.....D...W3K...P.....D...QJM...P.....DD..dYK...P.....D...9FG...P.....D...o6C...P.....D...9vB...P.....D...F8M...P.....D...e6C...P.....D...8dB...P.....D...ZIM...P.....D...6EJ...P.....D...+xF...P.....DLVYaCveLVwOt01IuDXOdYaG+xlOMBiOdYaG+xlOMBiOx+yR9FmOMBiO1+FF9FkiO4SzjDeuwU1it4ysoEaujhlj9baZw1abbQLr9baZw16JqFrOan868F29QJsO.UxE9tejR6iHm0jursejR6iW11wurglQE7iW11wurglQE7CYJSjuwglQE7yjwabuX4d59baZwzabg+Tx97sOrvC3VKoOe6CK7DmQeeiOe6CK7nsc32y29vRuwcxKA1ytFGbum7Rf8.JNE46XsIjMz5ydgpxurQJZR5SzqpyurAk8n4ydgpxurke9q2ydgpxurcYaG5CpJxzurgzcd5CpJxzurwtrs6ydgpxuiUF2.7GiU7iascxKA1iW11wur4Siv3iW11wur4Siv3i7+rjuw4Siv3i8ugguQ43S9DMIw2abk8na9baZw1KonIoO2lVr8FGWDCqO2lVr8txpA6yFZ+duwsejR6CPkbgu6Goz9HxYM4Ka6Goz93ksc7KanYTA+3ksc7KanYTA+PlxD4abnYTA+LY7F2KVtmtO2B....vOpn5p+vDyM+Cf..fOlVbA+....7C....91oMOr3y287xIw0K..........3ipptZQ9NVa.....zChHlH...............fOjoLQ+FWUq+kOlSDP+V05e4i4pmyuwU05e4y4PNyu8wfS9DSFu7abk1BO9rWnp76oEKhO6EpJ+FWpckfO6EpJ...........................................Q.8aYO5dOjoLQ+FWpckfOg+TR+dZwh3S3Okzui0lVfasOg+TR+FWVTNtOg+TR+V7fr6CYJSzuwEyb06i4DAzu..P89....D.....A...jy....D.....AA.PVTB...D.....A..vdgB...D.....A..vueA...D.....A...t9....D.....AA.PN+B...D.....A..PHAC...D.....A..P3OA...D.....A..vXkA...D.....A..vn9....D.....A.........D.....A...aZA...D.....A..PB6....D.....A..fsc....D.....A...a9B...D.....A..fA+....D.....A..vVr....D.....A..vurA...D.....A..PPPB...D.....AA.fE1C...D.....A..furA...D.....A..PrNA...D.....A..fyZB...D.....A..vurA...D.....A..fYt....D.....A..fKH....D.....A..v8NB...D.....A..fOAC...D.....A..fuwA...D.....AcHmU97EpJ3abs92P97EpJ3KHhNhOHvZC9NVafH5H9bPic6KafH5H9DMIw4abH2.P9fRHt46xpAkOnDha9FWfH5nOnDha9tCLf5yHfOnuwQ+0w5SruBouzecr9TYhs5abzecr97Ind6a2lVjOeBp29FGI+OiOeBp29Bhni3yAM1suiU12.zYHw6iasEXsQ4yDaU5trMNXl4CI+OauwI69C5ibolZuPKCg9HWpo1abv6hq9HWpo1K7t3pOK94O9FG7t3pOPrBV9pPup5S94uluwQxRm5i3H+mukDOn9ffTG5abkbol9.5uN5KYcFnOCgRn9FGQGEkOmC4r9RzQQ4SCNeruwQzQQ4i8n6ruogib9zHrb6abGSYh9PBdp66wTlnOL+h99F2wTlnOtOrB+ZoSW4i6Cqvuw041z3i6CqvunysI97kSD7abx3MF9DZr66qLdigOMkI19xlLdigOukG24vVmaSSOukG24vVmaSSOQ1P69FWmaSSO88sB+R+0w1y.2Zwuw0PnD3ShNJxurwAS9jnih7abEogi.....7iJpt5OLwby+.H..3ioEWvO....+....TZb+Jve9vaKq668QC..........9nppq160d6K.....8fHhIB...............3iG9lruwcmmf5i6Vdru6sOr9zFZ+5abEmh49fasj5awoXtOjoLQ9FWwoXtOrwAy8FkiO6y6hqSuw4s735S4Km...........................................rtOt0Fri0oOC1DN8FGpQXoODC6q7NB3C5CoUV8twoSWi4S4Km.Oo86N9T9xIvSbACn38T9xIvyx8zXOsXQ47FG..7MO....A....P...Xfu....A....PP..7Ar....A....P...X+a....A....P...DWe....A....P...3iK....A....PP..zqu....A....P...L+B....A....P...f7r....A....P...D2U....A....P...3SX....A....P...ntu....A....P...Tcy....A....P...n8a....A....P...D2W....A....P...3i7....A....P...jvt....A....P...7Tx....A....P...rbB....A....P...DI3....A....PP..3C1....A....P...z6X....A....P....Tb....A....P...7In....A....P...3qs....A....P...bfO....A....P...nYu....A....P...fCl....A....P...3yh....A....P...Hxu....A....PvuroBiQ5yhLKvuiUV3.7OFq6iasA6Xc5yfMgSuwgZDV5Cwv9Jui.9f9PZkUuab5z0X9T9xIvSZ+tiOkubB7DWv.JdOkubB7rbOM1SKVTNuwU05eyyjDOXuUs927.fVF3abUs927zgbV4a.e.aOBRcf9F2kgPgO1+Fl9VjYA5i8ugouw022J5i8ugouap7k93RMU5abap7k9fo485qgphjOXZdu9FWEyufOXZdu9pQEF2SAHOqurw2jW1yAM1suwckUC3SXlqtuNx+S9Dl4p6abU4Km9Dl4p6q1IUqOnRtz9F2WU2rOuKtt99U0M6yFZ+lur8U0M6yx8zfuw8U0M6yIbQTuQlE39H+OKyabjyZ19T9xIr6X9EsOXGqS6DW3OkrOkubB7Hwv95S4Km.OwopHy5S4Km.OQBdp9T9xIvab24In9fcrNyKri0oOC1DN8NVaWiCl9j3AY4ab.Emh97Ind4acMNnOeBpW9FmsjdfOeBpW9ZKoG3CmVrfuwYKoG3S5nnYuQ43S9jNJZ1abWiCl9jNJZ1603fI.....+nhpq9CSL27O.B..9XZwE7C...vO....Bv7h9f3Dr86Up4CZ..........fOpp5pvNVm9.....POHhXh................6FmNcMlOkubB7j1u63S4Km.OwELfh2S4Km.OK2Si8zhEkyabUs927LIwC1aUq+MO.nkA9FWUq+MOcHmU9FvGvB..........................................JeoOtTSk9F2lJeoOXZdu9ZnpH4Cll2quwUw7K3Cll2quZTgw8T.xy5Ka7M4k8bPic6abWY0.9Dl4p6qi7+jOgA..9FG...P.....D...05C...P.....DD..hqK...P.....D...eUM...P.....D...9bB...P.....D...7FG...P.....DD..Q6C...P.....D...Km....P.....D...pJB...P.....D...kuL...P.....D...7BK...P.....D...3fI...P.....D...eBJ...P.....D...wYK...P.....D...9vI...P.....D...Z1K...P.....D...3fI...P.....D...22B...P.....D...+xF...P.....D...MgC...P.....DD..KxL...P.....D...+xF...P.....D...dUM...P.....D...XrN...P.....D...8FG...P.....D...C5C...P.....D...Km....P.....D...ACH...P.....D...sXA...P.....D...8VE...P.....DT05eySGxYkuA7Ar8HH0A5abWFBE9X+aX5aQlEnO1+Fl9FWeeqnO1+Fl9toxW5iK0TouwsoxW5Cll2quFpJR9fo485abUL+B9fo485qFUXbOEf7r9xFeSdYOGzX29F2UVMfOgYt595H+O4SXlqtuwUkub5SXlqtuZmTs9fJ4R6abeUcy97t3556WU2rOan8a9x1WU2rOK2SC9F2WU2rOmvEQ8FYVf6i7+rLuwQNqY6S4Kmvti4Wz9fcrNsSbg+Tx9T9xIviDC6qOkubB7DmphLqOkubB7DI3o5S4Km.uwcmmf5C1w5LuvNVm9LXS3z6XscMNX5ShGjkuwATbJ5ymf5ku003f97Ind4ab1R5A97Ind4qsjdfObZwB9FmsjdfOoihl8FkiO4S5nnYuwcMNX5S5nnYuWiCl9H88s36XsMIwC3ygi7uur8EpJ1ygi7uuwIVAa1igWuvunpHy8LfhS7abu+f+8.HOa7KHhNhO.xyF+FGn3TjO.xyF+Bxyl4CdWbwuwAsLD5CbxKwur8Nh9.m7R7abnFgk9.....vOpn5p+vDyM+Cf..fOlVbA+....7C...ffC86BgcuOm1ab+V...........3ippt5H+6ab.....zChHlH...............fOTu8A+95Ep3C0aevuwo0rS3C0aevuSR7.9b3H+66XkQN.+iw593VavNVm9LXS3zabnFgk9PLrux6HfOnOjVY0..........................................fEkyabUs927LIwC1aUq+MO.nkA9FWUq+MOcHmU9FvGv1ifTGnuwcYHT3i8ugouEYVf9X+aX5ab88sh9X+aX56l..fOt....D.....A..Pu9B...D.....AA.v7K....D.....A...xyB...D.....A..PbWA...D.....A..fOgA...D.....AA.f59B...D.....A..P0MC...D.....A..f1uA...D.....A..PbeA...D.....A..fOxC...D.....A..PB6B...D.....A..vSIC...D.....A..vxI....D.....A..PjfC...D.....A..fOXC...D.....A..PuiA...D.....A...PwA...D.....A..vmfB...D.....A..fu1B...D.....AA.vA9....D.....A..fl8B...D.....A...NXB...D.....A..POwC...D.....A..PH+B...D.....A..fni....D.....A..PER....D.....A..PRCC...D.....A..POKB...D.....A..vuwA...D.....A4m+Z8DDQG7abfH5H8b+tK7KHhNRO1WgD+FGHhNRO1+FF+le9q0yb0ywuwkNJZ1S75ExukiOy8Dudg76XsA7t35S75Exuw86aE6S75Exuq7ky9LW8b7abV5z09X+aX7qkNcsO1WgD+FmkNcsO2u6B+txWN6SPDcvuw86aE6yhLKvu.uKt9rHyB7ab.e.q9rHyB7aw7KpOAQzA+FmxwmoO2u6B+pb7Y5i8UHwuwob7Y5i8ugwuEyun9LW8b7ab.e.q9Dudg7Kv6hqOwqWH+NVYkCv+XrtOt0Fri0oOC1DN8FGpQXoODC6q7NB3C5CoUV8twoSWi4S4Km.Oo86N9T9xIvSbACn38T9xIvyx8zXOsXQ47FWUq+MOSR7f8V05eyC.ZYfuwU05eySGxYkuA7Ar8HH0A5abWFBE9X+aX5aQlEnO1+Fl9FWeeqnO1+Fl9toxW5iK0TouwsoxW5Cll2quFpJR9fo485abUL+B9fo485qFUXbOEf7r9xFeSdYOGzX29F2UVMfOgYt595H+O4SXlqtuwUkub5SX.....7iJpt5OLwby+.H..3ioEWvO....+....zb0ewVC8rrOU+Ub9B..........9nppqtLuwQN.....8fHhIB...............T9xIviDC6qOkubB7DmphLqOkubB7DI3o5S4Km.uwcmmf5C1w5LuvNVm9LXS3z6XscMNX5ShGjkuwATbJ5ymf5E..........................................fhl8FkiO4S5nnYuwcMNX5S5nnYuWiCl9H88s36XsYbcY4iFnKvuw8sOr3iFnKvuvgIC9nnII7abCPd18rOYO7K..jcO....A....P...r1H....A....PP..3fY....A....P....hU....A....P...7Kb....A....P...HpO....A....PP..jwu....A....P...Huj....A....P...nA5....A....P...LVa....A....P...TgQ....A....P...nQH....A....P...TlX....A....P...3C2....A....P...7ab....A....P...LnO....A....P...j8H....A....P...Tl4....A....P...3Sh....A....P...3ku....A....PP..7R.....A....P...XwB....A....P...LLF....A....P...3S5....A....P...36X....A....P...7ug....A....P...3CB....A....P...zab....A....P...XvO....A....P...Trn....A....PPurctMs7ShZWQuwMJGe7S4Km.OKxr.+T9xIvSbMBK29T9xIvSSkurOHcmm7xFpJxrO.....vlKoGpO.....D2nb7oOFDjZ7lNJZ5SmaSSuwkzvQ5ykgPIuCp2d9zIB3sabCPdV9T9xIvi1IUiOkubB7DGe.qcOkubB7.fVF1iuiHMuw8sOrxiqRAXue6Cq7.fVF3abe6Cq7DYVf4a0T4aO.nkg9F2mysgO1+Fl9xuU74i8ugouwonII5i8ugouS+Hk93RMU5abS+Hk9XaG9568zHjO11gu9FG.ZYfO11gu9xeJ41SAHOqur8EpJ1yAM1suwAYv42SXlqtu+aXR9Dl4p6abtjdn9Dl4p6anDhqOx+yx9F2ZPisOgYt59l5.C7SXlqtuwAHOa7SXlqtu6EpJ+r4PN6abxRmN+vEwv5qrzoyOpCLf9FmrzoyO4m+Z9ReL37S2lVjui0l4wMvOXZdu9FW+h9tOXZdu9ZCse6y3m+puwkdnP6ShNJpuQb2x9naSK5KaP81G+naSK5abU4KG+fo485q4wMPRtkFcfTjcuwVczk1atAvmrGsOsqD09xF..........fk.........A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7....................D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7.........."
													}
,
													"fileref" : 													{
														"name" : "GRM Evolution",
														"filename" : "GRM Evolution_20161002.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "2b48a4e3e3c891f6454cbef2e3c425b1"
													}

												}
 ]
										}

									}
,
									"style" : "",
									"text" : "vst~",
									"varname" : "vst~",
									"viewvisibility" : 0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 103.5, 269.567017, 49.0, 22.0 ],
									"style" : "",
									"text" : "r line4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 253.031738, 537.0, 99.0, 22.0 ],
									"style" : "",
									"text" : "send~ DelayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 137.206299, 537.0, 97.0, 22.0 ],
									"style" : "",
									"text" : "send~ DelayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-217",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 571.5, 153.0, 73.0, 20.0 ],
									"style" : "",
									"text" : "Feedback"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-218",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 565.5, 175.0, 101.0, 22.0 ],
									"style" : "",
									"text" : "prepend 15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-214",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 399.608765, 145.88324, 73.0, 20.0 ],
									"style" : "",
									"text" : "Del distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-198",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 328.594055, 134.88324, 67.0, 33.0 ],
									"style" : "",
									"text" : "Amp distrib"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-197",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 259.594055, 134.88324, 62.0, 33.0 ],
									"style" : "",
									"text" : "Horizontal"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-195",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 193.5, 128.38324, 46.0, 33.0 ],
									"style" : "",
									"text" : "Vertical"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-175",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 253.031738, 177.0, 64.562317, 22.0 ],
									"style" : "",
									"text" : "prepend 9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-164",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 179.094086, 177.0, 67.21759, 22.0 ],
									"style" : "",
									"text" : "prepend 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-153",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 392.5, 177.0, 87.217529, 22.0 ],
									"style" : "",
									"text" : "prepend 12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-144",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 319.594055, 177.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "prepend 11"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 34.5, 134.88324, 67.0, 20.0 ],
									"style" : "",
									"text" : "receive~ mic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 227.311676, 333.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-94",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 179.094086, 333.567017, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 205.311676, 333.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 158.811676, 333.567017, 22.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-98",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 34.5, 101.016296, 99.0, 23.0 ],
									"style" : "",
									"text" : "plug Delays.vst"
								}

							}
, 							{
								"box" : 								{
									"autosave" : 1,
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-99",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 8,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
									"patching_rect" : [ 158.811676, 269.567017, 113.5, 22.0 ],
									"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_invisible" : 1,
											"parameter_longname" : "vst~[12]",
											"parameter_shortname" : "vst~[2]",
											"parameter_type" : 3
										}

									}
,
									"saved_object_attributes" : 									{
										"annotation_name" : "",
										"parameter_enable" : 1
									}
,
									"snapshot" : 									{
										"filetype" : "C74Snapshot",
										"version" : 2,
										"minorversion" : 0,
										"name" : "snapshotlist",
										"origin" : "vst~",
										"type" : "list",
										"subtype" : "Undefined",
										"embed" : 1,
										"snapshot" : 										{
											"pluginname" : "Delays.vst",
											"plugindisplayname" : "GRM Delays",
											"pluginsavedname" : "GRM Delays",
											"pluginsaveduniqueid" : 0,
											"version" : 1,
											"isbank" : 0,
											"isbase64" : 1,
											"blob" : "17972.CMlaKA....fQPMDZ....AbjTDUF.CXPA....EjjaoQGHDUFagkG..........................TD9L....bjbsAxQRQTY....mwuP..P.....xfyMwzzPtfC.....8j.z.zyvLDyO....+....................7Cf..vOLwby.....zChHlH.i8FN+.H..................................................n1Xt3hKtv1Y0AkKt.kKt3hKt3hKt3hK5MDZHw1LOAma0.2JtfjKtbC..........................................................bjctLSZmcGTz7jKB4hK43FbvEmPt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3xJqrxJqjVQUUELD4hKt3hKt3B.........A....H....v.....D....T....fA....G...t3B...P.....D...XEE...P.....DD..DkF...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...tXG...P.....D...t3B...P.....D...t3B...P.....D...tvD...P.....D...E4B...P.....D...t3B...P.....DD..t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...NED...P.....D...A4B...P.....D...t3B...P.....D...tPD...P.....D...i4B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..tPD...P.....DD..tDD...P.....D3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt.kPt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDTPt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKtP1Xt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKtX2JqLjKt3BQt3hKt3RPtbCc2UjPqvVMSgUPt3hKt3hKt3hKt3hKt3hKtDCUwYFTvfWUDIjKt3hKtP0bhUzYj4hKt3BTUszUQgjKt3hKt3hKt3hKD4hKt3hKAYUaScFQtA...H......8j.z.zyvLDyO....+....................7Cf..vOLwby+.H..3hKt.E...............................................vSt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hK..........................................................fKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hK..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...TpA...D.....A..PPwA...D.....A..fKt....D.....A..fKt....D.....A..fcqA...D.....A...MwA...D.....A..PXt....D.....A..fKt....D.....A..fKnA...D.....AA.Pay....D.....A..fK2....D.....A..vJt....D.....A..fKt....D.....A..fYBA...D.....A..fKt....D.....A..PNtA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fSt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....AA.fK4....D.....AA.fKt....D.....AA.fKt....D.....At3hKt3hKt3hKt3hbvAGb0LTYgIiPm4hKt3hKpgDZHEyPt3hKt3hKt3hKt3hKt3hKPslKt3hKt3hKt3hPt3hKt3hKt3hKtHmcGgDQB4hKt3hct3hKt3hKxQEQvLTdUMlUH4hK5k0Yt3hKtDjKt3hKP4hKtPDTt3hKt3RPt3hKt.ETt3BQP4hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BQxQkKt3hKA4hKt3BTP4hKxYmYt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3xMqrhKt3hKA4hKt3BTt3hKtHzYt3hKtDjKt3hKP4hKtH2Pm4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK....B.....vOO3kN9Hyxs3yGEf9O....................+.H..7CSL27O.B..t3hKt................................................3hKt.ETt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B..........................................................3hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.E..3hK....A....P...3BT....A....PP..DDQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..vzQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjQ....A....P...3hK....A....P...3hK....A....P...3hK....A....PP..3hP....A....P...PjZ....A....P...DDQ....A....P...DDQ....A....P...DjY....A....P...7VP....A....P...3hK....A....P...nEd....A....P...3hK....A....P...r1J....A....P...3hK....A....P...3hK....A....P...3hK....A....P...HlL....A....P...jSV....A....P...3hK....A....PP..3hK....A....PP..3hK....A....PP..3hK....A....PfcxkzPJcyJ4f0YD4xMoUjTtX2SVgkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQtfWbEYzQHQkUXIlZF4hKtXVPt3hKt3hKt3hKDAkKCUjPtXCbIkmcvDFNQ4hKt3hKt3BdqEibPs1btQjPWECZRgjKt3hKtrhanAWb4LzTLIyMO4hPt3xJtgmbWgyPt3hK18jKt3hKCAmcyHlSHIjd3ojZxLDQt3hKt3hKt3hKt3hYOAGb0.mYXwFRDIjKt3hKP8DRngEZt3hKt3hKt3hKt3hKt3hKtDjKt3hKL4hKt3hKtfUdxXkcqMmaDIEVwflTHcVVKozYAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQP4hKt3hKtHGbvA2MSkGS4o2St3hKlshKt3hKt3hKt3hKt3hKt3hKt3hKt3B...f......7yBOnqOxrbK+bkN7+C...................vO.B..+vDyM+Cf..fKt3hK................................................t3hKt3hKt3BTq4hKt3hKt3hKtHjKt3hKt3hKt.kKt3hKtPjKt3hKt...........................................................t3hKt3hKt3BTt3hKt3BQt3hKmcWPt3hKP4hKt3hKDQjKt3hKt3hKt.kKt3hKtPjKt3hV3wjKt3BTt3hKt3BQt...TED...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....DD..D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...DAE...P.....D...A4B...P.....D...t3F...P.....D...t3B...P.....D...t.E...P.....DD..A4B...P.....DD..tbC...P.....DD..t3B...P.....D3hKt.kKt3hKt3hKD4zYA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hK5IzYA4hKt3BTt3hKt3hKt3hKt3hKt3hKt3hKt3BTq4hKt3hKt3hKtHjKt3hKt3hKt3hKxY2QHQjPt3hKtXmKt3hKt3hdxsBMBI2PqAERv3DUBcFU1.URDYDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPlciVmcWPBsVQFczaA4hKtfjKt3hKt3hKt3hKAQjKYU0P1QmV3wTQxYjbT4hKt3hKtX2ct0lRDM1QNclc5M1LDIzTxb1TH4hKt3hKt3hKt3hKt3hKt.0JqrxJq3hKt3hKt3hKt3hKocSPBc1MwXmSDIDTCI1aMYSSxMzYlYCTIQjP0MzZPgzMNQkPmYlKt3hKt....H......+rvC55iLK2xOWoC+97DVZ................7Cf..vOLwby+.H..L0YxPE...............................................fQGgDUVgkXpYjKt3hct3hKt3hKt3hKtPDTt7VaJ4hMvkTdtwVPRk2P..........................................................vJqjyPt3hKtX2St3hKt3hKt3hKyjFbvQmVtLSLSciKt3hKtn2PngDaH4hKt3hKt3hKt3hKt3hKt3hKt3hKl4hK..fKt....D.....A..fKt....D.....AA..RgA...D.....A..vZOA...D.....A..PPDA...D.....AA.PbvA...D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PdPA...D.....A..PU4A...D.....A..fKt....D.....AA.fKt....D.....A..fKDA...D.....A...QBA...D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..PPt....D.....A..PPt....D.....A..fKt....D.....A..fKDA...D.....A...aCA...D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....AA.fKt....D.....AtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAEjKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDDQt3hKtPjKt3hKP4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hK1IEVt3hKt3BQt3hKt3hKtDDQP4hKt3hKD4hKt3hKt3RPDAkKt3hKtPjK....B.....vOK7ft9HyxszS6Ex3O.QOh................+.H..7CSL27O.B..t3hKt................................................7jKt3hKt3hKt3hK1IjcwIERS4RcJcVPyD2Rjc2Q1EmTH4hKt3hKt3B..........................................................3hKtHGb2gVPwfjcRgzXo4xRmoWSBIGQFAkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDjY2n0Y2EjPqUD..7VP....A....P...3hK....A....PP..nEd....A....P...3hK....A....P....Eb....A....PP..3hK....A....PP..3hK....A....P...3hK....A....P...HmQ....A....P...Dkd....A....P...XmT....A....P...3hK....A....P...3hK....A....P...3hK....A....P...rzS....A....P...PjT....A....P...DDQ....A....P...DDQ....A....PP..DDQ....A....P...3xM....A....P...rhK....A....P...3hK....A....P...3hK....A....P...XWN....A....P...jia....A....P...3hK....A....P...3hK....A....P...3hc....A....P...HGZ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...3hK....A....P....Gb....A....PP..3RN....A....PP..3hK....A....PP..3hK....A....PfKt3hKt3hKt3hKtHGbvAWMSokKt3hKt3hKt3hZHgFRwLjKt3hKt3hKt3hKt3hKt3hKt3hKt3hK1cWXMIjbwn2MGMiVSkWYlEWSMsRPt3hKt3hKt3hKt3hKt3hK5sxJqrxSt3hKt3hKt3hKA4hKt3BTt3hKxszYt3hKtDjKt3hKPAkKt3hSm4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.ETt3hbKclKt3hKA4hKt3BTt3hKDAkKt3hKtDjKt3hKP4hKtPDTt3hKt3RPt3hKt.kKt3hXnclKt3hKA4hKt3BTt3hKxIkTt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B...f.8fL2Z9SZ1YnOxrbK9LhQz9CN169OpX5C8z9RjC....vO.B..+vDyM+Cf..fKt3hK................................................t3hKPAkKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt...........................................................t3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKPA..t3B...P.....D...t.E...P.....DD..tYD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..qTD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...2QD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...hQD...P.....D...H0F...P.....D...pQC...P.....D...ywF...P.....DD..xLE...P.....D...EQC...P.....D...i4B...P.....D...jYF...P.....D...yTE...P.....D...jkG...P.....D...icF...P.....D...xsF...P.....D...RMC...P.....D...Z8F...P.....D...MID...P.....D...q0F...P.....D...tjD...P.....D...ZUD...P.....D...0PE...P.....D...WgE...P.....DD..tcF...P.....DD..CsD...P.....DD..hAE...P.....DXDYHUCRR0FTFwDQ4kTbIMiK54lctLGShIWVsYlLto1YLoGTKIiK1UjLPQjTxUkMtbTdhkWMTQlZsoTLUgDcK8jVDoFNmcmV4QSP3TlYMQDMioFTZYlTRUVav.kQ1M2bgQ2cWMjTDMSXwglLPkybhsRT0XSLD4hKt3hK43FbvEWNCMESxbyStHjKtjCVZcWQ2LjKt3hcO4hKt3Bb24TY4LjKQsTLwjWTGo2J4PkaGMSNCYlKtX1SvAWMvYUTrkyQA4hKt3BTOgDZXglKt3hKt3hKt3hKt3hKt3hKgMCSu0lUJQ2av7jLrUENNsRXpMFcBgSZq31LicGV3XFa1nWdI81YjYVQsMlPuQWbzImdXwTLjMkbQM2QxwDZK4hKt3hKxAGbvUyT4wTd58jKt3hYq3hKt3hKt3hKt3hKt3hKt3hKt3hKt....HPOHysl+jlcF5iLK2xOHkFE9THoA8iJl9POsuD4+.H..7Cf..vOLwby+.H..3hKt3B...............................................PSlgiRLozS0.yYSIzaOESYzsRaA4hKvPlKt3hKP4hKt3hKD4hKtbVc..........................................................fKt3hKt3hKt.kKt3hKtPjKt3xYFgjKt3BTt3hKt3BQD4hKAA2Rt3hKP4hKt3hKD4hKtTWQM4hKt.kKt3hKtPjK..vJXA...D.....A...Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....AA..Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...QPA...D.....A..PPt....D.....A..fK5A...D.....A..fKt....D.....A..fKPA...D.....AA.PPt....D.....AA.fKDA...D.....AA.fKt....D.....At3hKP4hKt3hKt3hZocWPt3hKt.kKt3hKt3hKtDjKA4hKt3BTt3hKt3hKtbyJqDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKlkGNPYmKB4Rdjc2LtvDSXYmKUEETAMEU1AEMDQUSwPTZBYmYlEzPTUTPzwTTtnGQ4gjcDgVPCQUQAMjclYDTEoGTEEjTPQWTWo0btTkV3UTLXQSPRM0YMczXqkzQHMTQTwDStjlPFwDTUQjcBMzPHk1R1QzTFYmXmEzPTUTPJYmKDAUQ5AUQAIETzE0UZMmKUoEdEECVzTTdCYmdlEzPTUTPKYmYAkzc5EVbMoGT1MycE0DQ40jcDMESxPzTNACUowDMtASQMwTdMYGQSwjLDMkSvPUZLQiaEwTPBkESO4xTCYDSPUEQTYGQFgTZLITTp0jPEklP1YlYAMDU....B3yPcOqOMWAC9Hyxs7CPzi3OwcR6................+.H..7CSL27O.B..tY2PCA...............................................nVX5slUKA0ZrI1YMYEY2ciKL0DV14RUQYmPLgETRwDNwn0PMQESY4B..........................................................vjTFglYAEDRt3hKt3hKq3BRt3xLoAGbzUSZBMlX14hKt3hK5MDZHwFRt3hKt3hKt3hKt3hKt3hK1kTZhwlK3TF..rBb....A....P...LES....A....PP..HCU....A....P...XjS....A....P...bTP....A....PP..3hK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...3hK....A....P....Gb....A....P...3hK....A....P...3hK....A....P...PDQ....A....P....EV....A....P...DDV....A....P...TiK....A....P...3hK....A....PP..3hK....A....P...3BQ....A....P...LiP....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...XjK....A....P...DjK....A....P...3hK....A....P...3BQ....A....P...XlP....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..3BQ....A....PP..3hK....A....PfKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPA4hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAQjKt3hKD4hKt3BTt3hKt3BQt3hKt3hKtrlTX4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hKAQDTt3hKt3BQt3hKt3hKtDDQP4hKt3hKD4B...f......zSBPCfOCyPL+....7C...POSFsc..........vO.B..+vDyM+Cf..fKt3hK................................................t3hKt3hKAQDTybWP1oGUP41bxA2LWUWXrwlZnkSPt3hKGElP4XlKLA..........................................................4XVUG4hKt3hKt3hKtzVXBkiYt3hYHgEdKwlYOgkKOgCRgYmRMYCQRImdxMER2IUdNclcmwjMDIDS3I2THYGR4A..t3B...P.....D...t3B...P.....DD..t3B...P.....D...t3B...P.....D...tXF...P.....DD..t4D...P.....DD..xME...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...AQD...P.....D...AQD...P.....D...AQD...P.....D...uUD...P.....D...t3B...P.....D...tPD...P.....D...mMG...P.....D...l4B...P.....DD..t3B...P.....D...t3B...P.....D...q3B...P.....D...t3B...P.....D...AQE...P.....D...L4B...P.....D...43F...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...toF...P.....D...t3B...P.....DD..tjC...P.....DD..t3B...P.....DD..t3B...P.....D3hKt3hKt3hKt3hKxAGbvUyTtDDQP4hKt3hKtnFRngTLC4hKt3hKt3hKt3hKt3hKt3RXt3hKtLjKt3hKt3hKt3BTtDDRC4jKL0TQDoDS1b1Yz71JvgmVlEmKt3hcXk0QPgjKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTP4hKtQlat3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hK14zYt3hKtDjKt3hKPAkKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BSNclKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt....H......8j.z.3iloc5O....9L7tbC...............7Cf..vOLwby+.H..3hKt3B...............................................fKt3BTP4hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK..........................................................fKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BT..fKt....D.....A..fKPA...D.....AA..QFA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PPDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PPFA...D.....A..fX0A...D.....A...auA...D.....A..fKt....D.....AA.fKDA...D.....A...S3....D.....A..fKt....D.....A..faxA...D.....A..vXBA...D.....A...U0A...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PZSA...D.....A...QBA...D.....A..fKt....D.....A..fTqA...D.....A..fKt....D.....AA.fKDA...D.....AA.PPDA...D.....AA.PPDA...D.....AP4RPDAkKAQDTZ4VYHIDTAQGQyYWZgUEdNkDLi8Dc44RRg4DN4gUQJ8jLVQTVPQVTBEDSl8zTpM0S0LiM4QkKggidsY2M0M0ZwfiMlQ1UhkmbBI0UCUEMnIUUUwVNuoldHkWLRwjSUcyYgAmS3cSXAQ2aYYDRt3hKt3xJtgFbwkyPSwjL27jKB4hK4fkV2UzMC4hKtX2St3hKtHGaxYWLT8FTZYyYzXiKvIiKt3hKt3hKt3hKl8DbvUCbm4xUiUWPt3hKt.0SHgFVn4hKt3hKt3hKt3hKt3hKt3BRU0DQOoFaDoGNqg1YsMDNp0DLKESNoIiX5U0Yq8zRrAkUSwlZyXWXxXUbscWdAokc2ACVJgVSnYTNucCa3bVZt3hKt3hbvAGb2LUdLkmdO4hKtX1Jt3hKt3hKt3hKt3hKt3hKt3hKt3hK....B.....fOrPD.+X.Fh8C...fON2D4................+.H..7CSL27O.B..t3hKt................................................Xld3HkQBQlaEMDaFQ2aj4xXvf0Pt3Bd40jKt3BTt3hKt3BQt3hKIoG..........................................................3hKt3hKt3hKP4hKt3hKD4hKtXzQL4hKt.kKt3hKtPDQt3xLMMjKt3BTt3hKt3BQt3hKqXSSt3hKP4hKt3hKD4B..jDd....A....P...PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....PP..PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PDT....A....P...DjK....A....P...3BR....A....P...3hK....A....P...3BT....A....PP..DjK....A....PP..3hd....A....PP..3hK....A....PfKt3BTt3hKt3hKtf0R4DjKt3hKP4hKt3hKt3xLjAWPt3hKt.kKt3hKt3hKxwFRA4hKt3BTt3hKt3hKtfEbJEjKt3hKP4hKt3hKjImYo8jXxQzcA8Fa0MTSygyRMYjTngGaVsFbx3BdigDawcVcp4hKt3BTt3hKt3RPLIjQ1shYNYlKAgjYtnjb1kCTmIVMJIjKt3BQtvjRX4hPvgERR4jLCEzPH4BSAIDVLEkK4MjQLAUUDQkcDgjKCwjcHoVTBU0PLcGQAwzSXYmKUEkYKMkYtvjctLDS14xPLcGQocjc1cVPCQUQAMjcPUDTEoGTEEjTPQWTWo0btTkV3UTLXQSPnQUc3XzX2YmKLoDV14RUQ4xRLwjYLQmKSwTVtjWQFwDTUQjatLDTtTETCUEQHEDMFM1avHDUukzUXk1ZWwzStL0PFwD...f......3CKDAvOFfgX+3hhd4iyMQN...............vO.B..+vDyM+Cf...U4wTZ................................................SQjc2XVPCQUQAQGSAIjctLDS14xPLYGQSwDYtLzQFwDTUQDStLTUt...........................................................osDdXYWQ1oVPt3hKt3hKt3hKtLSZvAGcvEldqY0Rt3hKt3hdCgFRrgjKt3hKt3hKt3hKt3hKt3hcZMTSDwTVAA..FIF...P.....D...FYF...P.....DD..GwD...P.....D...uEG...P.....D...uEF...P.....DD..wAG...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...vAG...P.....D...t3B...P.....D...t3B...P.....D...hoD...P.....D...UAC...P.....D...CQD...P.....D...icC...P.....D...t3B...P.....DD..t3B...P.....D...tPD...P.....D...qLD...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...JED...P.....D...A4B...P.....D...t3B...P.....D...tPD...P.....D...24B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..tPD...P.....DD..t3B...P.....D3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDTPt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPD4hKt3BQt3xavHkKt3hKtPjKt3hKt3hK5UkUt3hKt3BQt3hKt3hKtTUTP4hKt3hKD4hKt3hKt3hQLAkKt3hKtPjKt3hKt3hKAQiUt3hKt3BQt....H......9vBQ.7iAXH1OtnnW93VBZC...............7Cf..vOLwby+.H..3hKt3B...............................................fXGMSTg8FaZQCcxgTXS0FawkCbQsDSxAmLDwTXlMVZiIEYz7zb2oVM...........................................................TtDzMOwjSXYmKUAycCEDQ1sBQPYmKB4hKmYGUnETRxgVPAAETtn0Tr4RPP4hQBgUPT4hKP4hKtfkKqDjKt3BR..fKBA...D.....A..fctA...D.....AA..QLA...D.....A..fYxA...D.....A..fLOA...D.....AA.PRLA...D.....AA.vQGA...D.....A..PRrA...D.....A...Yx....D.....A..fYKA...D.....A..PM2A...D.....A..fcHA...D.....A..fPtA...D.....A..fKt....D.....A...Rz....D.....A..fSt....D.....A..PQLA...D.....A...RXA...D.....AA.vcLA...D.....A..fK2....D.....A..vJt....D.....A..fKt....D.....A..vPAA...D.....A..vPt....D.....A..PNtA...D.....A..fKt....D.....A..fKt....D.....A..fKXA...D.....A...S3....D.....A..vchA...D.....A...LHA...D.....A..vcDA...D.....A..fVAA...D.....A...bvA...D.....AA.fK4....D.....AA.fKt....D.....AA.fKt....D.....At3hKt3hKt3hKt3hbvAGb0jlXm0jUj4hKt3hKpgDZHEyPt3hKt3hKt3hKt3hKt3hK1UjQLAUUD4lKCAkKUA0PUQDRAQiQi8FLBQ0aIcEVos1UL8jKSMjQLAUUDImKCYjZD4hKxECTt3hKtDjKt3hKP4hKtvTPD4hKt3RPt3hKt.ETt3hKUokKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BQ2HiKt3hKA4hKt3BTP4hKxUjYt3hKtDjKt3hKP4hKtfDTq3hKt3RPt3hKt.kKt3BT1PjKt3hKA4hKt3BTt3hK1oVQt3hKtDjKt3hKP4hKtvTdt4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK....B.....fOrPD.+X.Fh4ySXowOI7E6................+.H..7CSL27O.B..t3hKt................................................3hKt.ETt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B..........................................................3hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.E..3hK....A....P...3BT....A....PP..XFQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..nWQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PDQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...nFQ....A....P...DEa....A....P...XUR....A....P...vjT....A....PP..TTY....A....P...PUa....A....P...X1J....A....P...vjP....A....P...HCS....A....P...LiS....A....P...3hK....A....P...3hK....A....P...3xM....A....P...rxJ....A....P...3hK....A....P...3hK....A....P...3hY....A....P...3hK....A....P...rxJ....A....P...3hK....A....PP..3BQ....A....PP..3hK....A....PP..3ha....A....P.QXIUYn8zMybiSwLjMTwzPwEzXTYTcFg1aiUEL1TSdjkiZ03lRw.CVxYkKt3hKtXlKt3hKtHmKtM0Yt3hKt3BQUkTZ2UmcJ0lM2PTMzTTaw.WNrkmMqrxJqrxJqrxJqLjKt3hK1sxJqrxJt3hKt3hYt3hKt3hKt3hKtrhanAWb4LzTLIyMO4hPt3RNXo0cEcyPt3hK18jKt3hKyjWbzr1QNsjPiomVWQzb23hKt3hKt3hKt3hYOAGb0.mKt3hKt3hKt3hKP8DRngEZt3hKt3hKt3hKt3hKt3hKtnzb4PiSzQSYZMScyHTS4LkKt3hKtfTZScSTig0akEVS2IjTAM1JYoGMn4jKt3hKtLUS1QFdB4hKt3hKB4hKt3hKt3hKtHGbvA2MSkGS4o2St3hKlshKt3hKt3hKt3hKt3hKt3hKt3hKt3B...f......3CKDAvOFfgX97DVZ7SBew9O.B............vO.B..+vDyM+Cf..fKt3hK................................................TwTPuoWRukiVt81TZMiVwLmbv4hKt3lbD4hKt.kKt3hKtPjKt3hSMA..........................................................t3hKt3hKt3BTt3hKt3BQt3hKt3hKt3hKP4hKt3hKDQjKt3hKt3hKt.kKt3hKtPjKt3hKtPjKt3BTt3hKt3BQt...0PE...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....DD..D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...DAE...P.....D...A4B...P.....D...tXF...P.....D...t3B...P.....D...t.E...P.....DD..A4B...P.....DD..t3B...P.....DD..t3B...P.....D3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKLYlMA4hKt3BTt3hKtXGYtnmRGUULJUVTwXyMWEjKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKH4hKt3BTq3RMTgjKt3hKtLGUZgiL3j0X1LkdRETZxoFNKwjVwUkTAUlKHYiM1XTZOMjSoYySB4TSi0jKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt.UQAUCUH4hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt....HfNbCH.9zR+.7iAXHlOOgkF+jvWr+Cf........+.H..7Cf..vOLwby+.H..3hKt3B...............................................fKtDjKt3hKPsTP0PERt3hKt3RPt3hKt3hKt3hKt3hKt3hKt3hKt3hK..........................................................fKtDjKt3hKt3hKt3hKt3hKt3hKyjFbvQmRt3hKt3hKt3hKtn2PngDaH4hKt3hKt3hKt3hKt3hKt3hKt3hKtDjK..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKPA...D.....AA.PbvA...D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PMTA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..PPt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....AA.fKt....D.....AtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAEjKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDDQt3hKtPjKt3hKP4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hKt3BTt3hKt3BQt3hKt3hKt3hKP4hKt3hKD4hKt3hKt3xJqTlKt3hKtPjKI4VZzABQkwVX4AfKt3BQt3hKt3hKtTCU...........4B........D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7.........."
										}
,
										"snapshotlist" : 										{
											"current_snapshot" : 0,
											"entries" : [ 												{
													"filetype" : "C74Snapshot",
													"version" : 2,
													"minorversion" : 0,
													"name" : "GRM Delays",
													"origin" : "Delays.vst",
													"type" : "VST",
													"subtype" : "MidiEffect",
													"embed" : 1,
													"snapshot" : 													{
														"pluginname" : "Delays.vst",
														"plugindisplayname" : "GRM Delays",
														"pluginsavedname" : "GRM Delays",
														"pluginsaveduniqueid" : 0,
														"version" : 1,
														"isbank" : 0,
														"isbase64" : 1,
														"blob" : "17972.CMlaKA....fQPMDZ....AbjTDUF.CXPA....EjjaoQGHDUFagkG..........................TD9L....bjbsAxQRQTY....mwuP..P.....xfyMwzzPtfC.....8j.z.zyvLDyO....+....................7Cf..vOLwby.....zChHlH.i8FN+.H..................................................n1Xt3hKtv1Y0AkKt.kKt3hKt3hKt3hK5MDZHw1LOAma0.2JtfjKtbC..........................................................bjctLSZmcGTz7jKB4hK43FbvEmPt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3xJqrxJqjVQUUELD4hKt3hKt3B.........A....H....v.....D....T....fA....G...t3B...P.....D...XEE...P.....DD..DkF...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...tXG...P.....D...t3B...P.....D...t3B...P.....D...tvD...P.....D...E4B...P.....D...t3B...P.....DD..t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...NED...P.....D...A4B...P.....D...t3B...P.....D...tPD...P.....D...i4B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..tPD...P.....DD..tDD...P.....D3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt.kPt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDTPt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKtP1Xt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKtX2JqLjKt3BQt3hKt3RPtbCc2UjPqvVMSgUPt3hKt3hKt3hKt3hKt3hKtDCUwYFTvfWUDIjKt3hKtP0bhUzYj4hKt3BTUszUQgjKt3hKt3hKt3hKD4hKt3hKAYUaScFQtA...H......8j.z.zyvLDyO....+....................7Cf..vOLwby+.H..3hKt.E...............................................vSt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hK..........................................................fKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hK..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...TpA...D.....A..PPwA...D.....A..fKt....D.....A..fKt....D.....A..fcqA...D.....A...MwA...D.....A..PXt....D.....A..fKt....D.....A..fKnA...D.....AA.Pay....D.....A..fK2....D.....A..vJt....D.....A..fKt....D.....A..fYBA...D.....A..fKt....D.....A..PNtA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fSt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....AA.fK4....D.....AA.fKt....D.....AA.fKt....D.....At3hKt3hKt3hKt3hbvAGb0LTYgIiPm4hKt3hKpgDZHEyPt3hKt3hKt3hKt3hKt3hKPslKt3hKt3hKt3hPt3hKt3hKt3hKtHmcGgDQB4hKt3hct3hKt3hKxQEQvLTdUMlUH4hK5k0Yt3hKtDjKt3hKP4hKtPDTt3hKt3RPt3hKt.ETt3BQP4hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BQxQkKt3hKA4hKt3BTP4hKxYmYt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3xMqrhKt3hKA4hKt3BTt3hKtHzYt3hKtDjKt3hKP4hKtH2Pm4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK....B.....vOO3kN9Hyxs3yGEf9O....................+.H..7CSL27O.B..t3hKt................................................3hKt.ETt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B..........................................................3hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.E..3hK....A....P...3BT....A....PP..DDQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..vzQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjQ....A....P...3hK....A....P...3hK....A....P...3hK....A....PP..3hP....A....P...PjZ....A....P...DDQ....A....P...DDQ....A....P...DjY....A....P...7VP....A....P...3hK....A....P...nEd....A....P...3hK....A....P...r1J....A....P...3hK....A....P...3hK....A....P...3hK....A....P...HlL....A....P...jSV....A....P...3hK....A....PP..3hK....A....PP..3hK....A....PP..3hK....A....PfcxkzPJcyJ4f0YD4xMoUjTtX2SVgkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQtfWbEYzQHQkUXIlZF4hKtXVPt3hKt3hKt3hKDAkKCUjPtXCbIkmcvDFNQ4hKt3hKt3BdqEibPs1btQjPWECZRgjKt3hKtrhanAWb4LzTLIyMO4hPt3xJtgmbWgyPt3hK18jKt3hKCAmcyHlSHIjd3ojZxLDQt3hKt3hKt3hKt3hYOAGb0.mYXwFRDIjKt3hKP8DRngEZt3hKt3hKt3hKt3hKt3hKtDjKt3hKL4hKt3hKtfUdxXkcqMmaDIEVwflTHcVVKozYAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQP4hKt3hKtHGbvA2MSkGS4o2St3hKlshKt3hKt3hKt3hKt3hKt3hKt3hKt3B...f......7yBOnqOxrbK+bkN7+C...................vO.B..+vDyM+Cf..fKt3hK................................................t3hKt3hKt3BTq4hKt3hKt3hKtHjKt3hKt3hKt.kKt3hKtPjKt3hKt...........................................................t3hKt3hKt3BTt3hKt3BQt3hKmcWPt3hKP4hKt3hKDQjKt3hKt3hKt.kKt3hKtPjKt3hV3wjKt3BTt3hKt3BQt...TED...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....DD..D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...DAE...P.....D...A4B...P.....D...t3F...P.....D...t3B...P.....D...t.E...P.....DD..A4B...P.....DD..tbC...P.....DD..t3B...P.....D3hKt.kKt3hKt3hKD4zYA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hK5IzYA4hKt3BTt3hKt3hKt3hKt3hKt3hKt3hKt3BTq4hKt3hKt3hKtHjKt3hKt3hKt3hKxY2QHQjPt3hKtXmKt3hKt3hdxsBMBI2PqAERv3DUBcFU1.URDYDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPlciVmcWPBsVQFczaA4hKtfjKt3hKt3hKt3hKAQjKYU0P1QmV3wTQxYjbT4hKt3hKtX2ct0lRDM1QNclc5M1LDIzTxb1TH4hKt3hKt3hKt3hKt3hKt.0JqrxJq3hKt3hKt3hKt3hKocSPBc1MwXmSDIDTCI1aMYSSxMzYlYCTIQjP0MzZPgzMNQkPmYlKt3hKt....H......+rvC55iLK2xOWoC+97DVZ................7Cf..vOLwby+.H..L0YxPE...............................................fQGgDUVgkXpYjKt3hct3hKt3hKt3hKtPDTt7VaJ4hMvkTdtwVPRk2P..........................................................vJqjyPt3hKtX2St3hKt3hKt3hKyjFbvQmVtLSLSciKt3hKtn2PngDaH4hKt3hKt3hKt3hKt3hKt3hKt3hKl4hK..fKt....D.....A..fKt....D.....AA..RgA...D.....A..vZOA...D.....A..PPDA...D.....AA.PbvA...D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PdPA...D.....A..PU4A...D.....A..fKt....D.....AA.fKt....D.....A..fKDA...D.....A...QBA...D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..PPt....D.....A..PPt....D.....A..fKt....D.....A..fKDA...D.....A...aCA...D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....AA.fKt....D.....AtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAEjKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDDQt3hKtPjKt3hKP4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hK1IEVt3hKt3BQt3hKt3hKtDDQP4hKt3hKD4hKt3hKt3RPDAkKt3hKtPjK....B.....vOK7ft9HyxszS6Ex3O.QOh................+.H..7CSL27O.B..t3hKt................................................7jKt3hKt3hKt3hK1IjcwIERS4RcJcVPyD2Rjc2Q1EmTH4hKt3hKt3B..........................................................3hKtHGb2gVPwfjcRgzXo4xRmoWSBIGQFAkKAQDTtDDQP4RPDAkKAQDTtDDQP4RPDAkKAQDTtDjY2n0Y2EjPqUD..7VP....A....P...3hK....A....PP..nEd....A....P...3hK....A....P....Eb....A....PP..3hK....A....PP..3hK....A....P...3hK....A....P...HmQ....A....P...Dkd....A....P...XmT....A....P...3hK....A....P...3hK....A....P...3hK....A....P...rzS....A....P...PjT....A....P...DDQ....A....P...DDQ....A....PP..DDQ....A....P...3xM....A....P...rhK....A....P...3hK....A....P...3hK....A....P...XWN....A....P...jia....A....P...3hK....A....P...3hK....A....P...3hc....A....P...HGZ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...3hK....A....P....Gb....A....PP..3RN....A....PP..3hK....A....PP..3hK....A....PfKt3hKt3hKt3hKtHGbvAWMSokKt3hKt3hKt3hZHgFRwLjKt3hKt3hKt3hKt3hKt3hKt3hKt3hK1cWXMIjbwn2MGMiVSkWYlEWSMsRPt3hKt3hKt3hKt3hKt3hK5sxJqrxSt3hKt3hKt3hKA4hKt3BTt3hKxszYt3hKtDjKt3hKPAkKt3hSm4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.ETt3hbKclKt3hKA4hKt3BTt3hKDAkKt3hKtDjKt3hKP4hKtPDTt3hKt3RPt3hKt.kKt3hXnclKt3hKA4hKt3BTt3hKxIkTt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B...f.8fL2Z9SZ1YnOxrbK9LhQz9CN169OpX5C8z9RjC....vO.B..+vDyM+Cf..fKt3hK................................................t3hKPAkKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt...........................................................t3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKPA..t3B...P.....D...t.E...P.....DD..tYD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..qTD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...2QD...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...hQD...P.....D...H0F...P.....D...pQC...P.....D...ywF...P.....DD..xLE...P.....D...EQC...P.....D...i4B...P.....D...jYF...P.....D...yTE...P.....D...jkG...P.....D...icF...P.....D...xsF...P.....D...RMC...P.....D...Z8F...P.....D...MID...P.....D...q0F...P.....D...tjD...P.....D...ZUD...P.....D...0PE...P.....D...WgE...P.....DD..tcF...P.....DD..CsD...P.....DD..hAE...P.....DXDYHUCRR0FTFwDQ4kTbIMiK54lctLGShIWVsYlLto1YLoGTKIiK1UjLPQjTxUkMtbTdhkWMTQlZsoTLUgDcK8jVDoFNmcmV4QSP3TlYMQDMioFTZYlTRUVav.kQ1M2bgQ2cWMjTDMSXwglLPkybhsRT0XSLD4hKt3hK43FbvEWNCMESxbyStHjKtjCVZcWQ2LjKt3hcO4hKt3Bb24TY4LjKQsTLwjWTGo2J4PkaGMSNCYlKtX1SvAWMvYUTrkyQA4hKt3BTOgDZXglKt3hKt3hKt3hKt3hKt3hKgMCSu0lUJQ2av7jLrUENNsRXpMFcBgSZq31LicGV3XFa1nWdI81YjYVQsMlPuQWbzImdXwTLjMkbQM2QxwDZK4hKt3hKxAGbvUyT4wTd58jKt3hYq3hKt3hKt3hKt3hKt3hKt3hKt3hKt....HPOHysl+jlcF5iLK2xOHkFE9THoA8iJl9POsuD4+.H..7Cf..vOLwby+.H..3hKt3B...............................................PSlgiRLozS0.yYSIzaOESYzsRaA4hKvPlKt3hKP4hKt3hKD4hKtbVc..........................................................fKt3hKt3hKt.kKt3hKtPjKt3xYFgjKt3BTt3hKt3BQD4hKAA2Rt3hKP4hKt3hKD4hKtTWQM4hKt.kKt3hKtPjK..vJXA...D.....A...Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....AA..Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...Qt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...QPA...D.....A..PPt....D.....A..fK5A...D.....A..fKt....D.....A..fKPA...D.....AA.PPt....D.....AA.fKDA...D.....AA.fKt....D.....At3hKP4hKt3hKt3hZocWPt3hKt.kKt3hKt3hKtDjKA4hKt3BTt3hKt3hKtbyJqDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKlkGNPYmKB4Rdjc2LtvDSXYmKUEETAMEU1AEMDQUSwPTZBYmYlEzPTUTPzwTTtnGQ4gjcDgVPCQUQAMjclYDTEoGTEEjTPQWTWo0btTkV3UTLXQSPRM0YMczXqkzQHMTQTwDStjlPFwDTUQjcBMzPHk1R1QzTFYmXmEzPTUTPJYmKDAUQ5AUQAIETzE0UZMmKUoEdEECVzTTdCYmdlEzPTUTPKYmYAkzc5EVbMoGT1MycE0DQ40jcDMESxPzTNACUowDMtASQMwTdMYGQSwjLDMkSvPUZLQiaEwTPBkESO4xTCYDSPUEQTYGQFgTZLITTp0jPEklP1YlYAMDU....B3yPcOqOMWAC9Hyxs7CPzi3OwcR6................+.H..7CSL27O.B..tY2PCA...............................................nVX5slUKA0ZrI1YMYEY2ciKL0DV14RUQYmPLgETRwDNwn0PMQESY4B..........................................................vjTFglYAEDRt3hKt3hKq3BRt3xLoAGbzUSZBMlX14hKt3hK5MDZHwFRt3hKt3hKt3hKt3hKt3hK1kTZhwlK3TF..rBb....A....P...LES....A....PP..HCU....A....P...XjS....A....P...bTP....A....PP..3hK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...3hK....A....P....Gb....A....P...3hK....A....P...3hK....A....P...PDQ....A....P....EV....A....P...DDV....A....P...TiK....A....P...3hK....A....PP..3hK....A....P...3BQ....A....P...LiP....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...XjK....A....P...DjK....A....P...3hK....A....P...3BQ....A....P...XlP....A....P...3hK....A....P...3hK....A....P...3BQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..3BQ....A....PP..3hK....A....PfKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPA4hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAQjKt3hKD4hKt3BTt3hKt3BQt3hKt3hKtrlTX4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hKAQDTt3hKt3BQt3hKt3hKtDDQP4hKt3hKD4B...f......zSBPCfOCyPL+....7C...POSFsc..........vO.B..+vDyM+Cf..fKt3hK................................................t3hKt3hKAQDTybWP1oGUP41bxA2LWUWXrwlZnkSPt3hKGElP4XlKLA..........................................................4XVUG4hKt3hKt3hKtzVXBkiYt3hYHgEdKwlYOgkKOgCRgYmRMYCQRImdxMER2IUdNclcmwjMDIDS3I2THYGR4A..t3B...P.....D...t3B...P.....DD..t3B...P.....D...t3B...P.....D...tXF...P.....DD..t4D...P.....DD..xME...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...AQD...P.....D...AQD...P.....D...AQD...P.....D...uUD...P.....D...t3B...P.....D...tPD...P.....D...mMG...P.....D...l4B...P.....DD..t3B...P.....D...t3B...P.....D...q3B...P.....D...t3B...P.....D...AQE...P.....D...L4B...P.....D...43F...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...toF...P.....D...t3B...P.....DD..tjC...P.....DD..t3B...P.....DD..t3B...P.....D3hKt3hKt3hKt3hKxAGbvUyTtDDQP4hKt3hKtnFRngTLC4hKt3hKt3hKt3hKt3hKt3RXt3hKtLjKt3hKt3hKt3BTtDDRC4jKL0TQDoDS1b1Yz71JvgmVlEmKt3hcXk0QPgjKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTP4hKtQlat3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hK14zYt3hKtDjKt3hKPAkKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BSNclKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt....H......8j.z.3iloc5O....9L7tbC...............7Cf..vOLwby+.H..3hKt3B...............................................fKt3BTP4hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK..........................................................fKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BT..fKt....D.....A..fKPA...D.....AA..QFA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PPDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PPFA...D.....A..fX0A...D.....A...auA...D.....A..fKt....D.....AA.fKDA...D.....A...S3....D.....A..fKt....D.....A..faxA...D.....A..vXBA...D.....A...U0A...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PZSA...D.....A...QBA...D.....A..fKt....D.....A..fTqA...D.....A..fKt....D.....AA.fKDA...D.....AA.PPDA...D.....AA.PPDA...D.....AP4RPDAkKAQDTZ4VYHIDTAQGQyYWZgUEdNkDLi8Dc44RRg4DN4gUQJ8jLVQTVPQVTBEDSl8zTpM0S0LiM4QkKggidsY2M0M0ZwfiMlQ1UhkmbBI0UCUEMnIUUUwVNuoldHkWLRwjSUcyYgAmS3cSXAQ2aYYDRt3hKt3xJtgFbwkyPSwjL27jKB4hK4fkV2UzMC4hKtX2St3hKtHGaxYWLT8FTZYyYzXiKvIiKt3hKt3hKt3hKl8DbvUCbm4xUiUWPt3hKt.0SHgFVn4hKt3hKt3hKt3hKt3hKt3BRU0DQOoFaDoGNqg1YsMDNp0DLKESNoIiX5U0Yq8zRrAkUSwlZyXWXxXUbscWdAokc2ACVJgVSnYTNucCa3bVZt3hKt3hbvAGb2LUdLkmdO4hKtX1Jt3hKt3hKt3hKt3hKt3hKt3hKt3hK....B.....fOrPD.+X.Fh8C...fON2D4................+.H..7CSL27O.B..t3hKt................................................Xld3HkQBQlaEMDaFQ2aj4xXvf0Pt3Bd40jKt3BTt3hKt3BQt3hKIoG..........................................................3hKt3hKt3hKP4hKt3hKD4hKtXzQL4hKt.kKt3hKtPDQt3xLMMjKt3BTt3hKt3BQt3hKqXSSt3hKP4hKt3hKD4B..jDd....A....P...PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....PP..PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....PP..3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PjK....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PDT....A....P...DjK....A....P...3BR....A....P...3hK....A....P...3BT....A....PP..DjK....A....PP..3hd....A....PP..3hK....A....PfKt3BTt3hKt3hKtf0R4DjKt3hKP4hKt3hKt3xLjAWPt3hKt.kKt3hKt3hKxwFRA4hKt3BTt3hKt3hKtfEbJEjKt3hKP4hKt3hKjImYo8jXxQzcA8Fa0MTSygyRMYjTngGaVsFbx3BdigDawcVcp4hKt3BTt3hKt3RPLIjQ1shYNYlKAgjYtnjb1kCTmIVMJIjKt3BQtvjRX4hPvgERR4jLCEzPH4BSAIDVLEkK4MjQLAUUDQkcDgjKCwjcHoVTBU0PLcGQAwzSXYmKUEkYKMkYtvjctLDS14xPLcGQocjc1cVPCQUQAMjcPUDTEoGTEEjTPQWTWo0btTkV3UTLXQSPnQUc3XzX2YmKLoDV14RUQ4xRLwjYLQmKSwTVtjWQFwDTUQjatLDTtTETCUEQHEDMFM1avHDUukzUXk1ZWwzStL0PFwD...f......3CKDAvOFfgX+3hhd4iyMQN...............vO.B..+vDyM+Cf...U4wTZ................................................SQjc2XVPCQUQAQGSAIjctLDS14xPLYGQSwDYtLzQFwDTUQDStLTUt...........................................................osDdXYWQ1oVPt3hKt3hKt3hKtLSZvAGcvEldqY0Rt3hKt3hdCgFRrgjKt3hKt3hKt3hKt3hKt3hcZMTSDwTVAA..FIF...P.....D...FYF...P.....DD..GwD...P.....D...uEG...P.....D...uEF...P.....DD..wAG...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...vAG...P.....D...t3B...P.....D...t3B...P.....D...hoD...P.....D...UAC...P.....D...CQD...P.....D...icC...P.....D...t3B...P.....DD..t3B...P.....D...tPD...P.....D...qLD...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...JED...P.....D...A4B...P.....D...t3B...P.....D...tPD...P.....D...24B...P.....D...t3B...P.....D...t3B...P.....D...tPD...P.....D...t3B...P.....D...t3B...P.....DD..t3B...P.....DD..tPD...P.....DD..t3B...P.....D3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDTPt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPD4hKt3BQt3xavHkKt3hKtPjKt3hKt3hK5UkUt3hKt3BQt3hKt3hKtTUTP4hKt3hKD4hKt3hKt3hQLAkKt3hKtPjKt3hKt3hKAQiUt3hKt3BQt....H......9vBQ.7iAXH1OtnnW93VBZC...............7Cf..vOLwby+.H..3hKt3B...............................................fXGMSTg8FaZQCcxgTXS0FawkCbQsDSxAmLDwTXlMVZiIEYz7zb2oVM...........................................................TtDzMOwjSXYmKUAycCEDQ1sBQPYmKB4hKmYGUnETRxgVPAAETtn0Tr4RPP4hQBgUPT4hKP4hKtfkKqDjKt3BR..fKBA...D.....A..fctA...D.....AA..QLA...D.....A..fYxA...D.....A..fLOA...D.....AA.PRLA...D.....AA.vQGA...D.....A..PRrA...D.....A...Yx....D.....A..fYKA...D.....A..PM2A...D.....A..fcHA...D.....A..fPtA...D.....A..fKt....D.....A...Rz....D.....A..fSt....D.....A..PQLA...D.....A...RXA...D.....AA.vcLA...D.....A..fK2....D.....A..vJt....D.....A..fKt....D.....A..vPAA...D.....A..vPt....D.....A..PNtA...D.....A..fKt....D.....A..fKt....D.....A..fKXA...D.....A...S3....D.....A..vchA...D.....A...LHA...D.....A..vcDA...D.....A..fVAA...D.....A...bvA...D.....AA.fK4....D.....AA.fKt....D.....AA.fKt....D.....At3hKt3hKt3hKt3hbvAGb0jlXm0jUj4hKt3hKpgDZHEyPt3hKt3hKt3hKt3hKt3hK1UjQLAUUD4lKCAkKUA0PUQDRAQiQi8FLBQ0aIcEVos1UL8jKSMjQLAUUDImKCYjZD4hKxECTt3hKtDjKt3hKP4hKtvTPD4hKt3RPt3hKt.ETt3hKUokKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3BQ2HiKt3hKA4hKt3BTP4hKxUjYt3hKtDjKt3hKP4hKtfDTq3hKt3RPt3hKt.kKt3BT1PjKt3hKA4hKt3BTt3hK1oVQt3hKtDjKt3hKP4hKtvTdt4hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hK....B.....fOrPD.+X.Fh4ySXowOI7E6................+.H..7CSL27O.B..t3hKt................................................3hKt.ETt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3B..........................................................3hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.E..3hK....A....P...3BT....A....PP..XFQ....A....P...3hK....A....P...3hK....A....PP..3hK....A....PP..nWQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...PDQ....A....P...3hK....A....P...3hK....A....P...3hK....A....P...nFQ....A....P...DEa....A....P...XUR....A....P...vjT....A....PP..TTY....A....P...PUa....A....P...X1J....A....P...vjP....A....P...HCS....A....P...LiS....A....P...3hK....A....P...3hK....A....P...3xM....A....P...rxJ....A....P...3hK....A....P...3hK....A....P...3hY....A....P...3hK....A....P...rxJ....A....P...3hK....A....PP..3BQ....A....PP..3hK....A....PP..3ha....A....P.QXIUYn8zMybiSwLjMTwzPwEzXTYTcFg1aiUEL1TSdjkiZ03lRw.CVxYkKt3hKtXlKt3hKtHmKtM0Yt3hKt3BQUkTZ2UmcJ0lM2PTMzTTaw.WNrkmMqrxJqrxJqrxJqLjKt3hK1sxJqrxJt3hKt3hYt3hKt3hKt3hKtrhanAWb4LzTLIyMO4hPt3RNXo0cEcyPt3hK18jKt3hKyjWbzr1QNsjPiomVWQzb23hKt3hKt3hKt3hYOAGb0.mKt3hKt3hKt3hKP8DRngEZt3hKt3hKt3hKt3hKt3hKtnzb4PiSzQSYZMScyHTS4LkKt3hKtfTZScSTig0akEVS2IjTAM1JYoGMn4jKt3hKtLUS1QFdB4hKt3hKB4hKt3hKt3hKtHGbvA2MSkGS4o2St3hKlshKt3hKt3hKt3hKt3hKt3hKt3hKt3B...f......3CKDAvOFfgX97DVZ7SBew9O.B............vO.B..+vDyM+Cf..fKt3hK................................................TwTPuoWRukiVt81TZMiVwLmbv4hKt3lbD4hKt.kKt3hKtPjKt3hSMA..........................................................t3hKt3hKt3BTt3hKt3BQt3hKt3hKt3hKP4hKt3hKDQjKt3hKt3hKt.kKt3hKtPjKt3hKtPjKt3BTt3hKt3BQt...0PE...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....DD..D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....DD..t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...D4B...P.....D...t3B...P.....D...t3B...P.....D...t3B...P.....D...DAE...P.....D...A4B...P.....D...tXF...P.....D...t3B...P.....D...t.E...P.....DD..A4B...P.....DD..t3B...P.....DD..t3B...P.....D3hKt.kKt3hKt3hKt3hKA4hKt3BTt3hKt3hKt3hKtDjKt3hKP4hKt3hKt3hKt3RPt3hKt.kKt3hKt3hKLYlMA4hKt3BTt3hKtXGYtnmRGUULJUVTwXyMWEjKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKH4hKt3BTq3RMTgjKt3hKtLGUZgiL3j0X1LkdRETZxoFNKwjVwUkTAUlKHYiM1XTZOMjSoYySB4TSi0jKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt.UQAUCUH4hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt3hKt....HfNbCH.9zR+.7iAXHlOOgkF+jvWr+Cf........+.H..7Cf..vOLwby+.H..3hKt3B...............................................fKtDjKt3hKPsTP0PERt3hKt3RPt3hKt3hKt3hKt3hKt3hKt3hKt3hK..........................................................fKtDjKt3hKt3hKt3hKt3hKt3hKyjFbvQmRt3hKt3hKt3hKtn2PngDaH4hKt3hKt3hKt3hKt3hKt3hKt3hKtDjK..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKt....D.....A..fKPA...D.....AA.PbvA...D.....AA.fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A...bvA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..PMTA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..PPt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....A..fKt....D.....A..fKDA...D.....A..fKt....D.....A..fKt....D.....AA.fKt....D.....AA.fKDA...D.....AA.fKt....D.....AtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKAEjKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDjKt3hKt3hKt3BQt3hKt3RPt3hKt3hKt3hKD4hKt3hKA4hKt3hKt3hKtPjKt3hKtDDQt3hKtPjKt3hKP4hKt3hKD4hKt3hKt3hKt.kKt3hKtPjKt3hKt3hKt3BTt3hKt3BQt3hKt3hKt3hKP4hKt3hKD4hKt3hKt3xJqTlKt3hKtPjKI4VZzABQkwVX4AfKt3BQt3hKt3hKtTCU...........4B........D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7...............A....D.....O..............P.....A....vC..............D....P.....7.........."
													}
,
													"fileref" : 													{
														"name" : "GRM Delays",
														"filename" : "GRM Delays_20180519.maxsnap",
														"filepath" : "~/Documents/Max 7/Snapshots",
														"filepos" : -1,
														"snapshotfileid" : "a8ca35a864d2452692e816a809d6c9fb"
													}

												}
 ]
										}

									}
,
									"style" : "",
									"text" : "vst~",
									"varname" : "vst~[10]",
									"viewvisibility" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-120", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-106", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-107", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-108", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-113", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-113", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-114", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-116", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-117", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-120", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-120", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-120", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-114", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-123", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-218", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-153", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-153", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-175", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-218", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-144", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-175", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-31", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 171.0, 268.0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-90", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberB-1",
								"default" : 								{
									"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1870.09729, 956.958252, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 710.876587, 324.958252, 50.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p effets",
					"varname" : "effets"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2536.844238, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 259.375244, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2544.094238, 337.078705, 45.0, 22.0 ],
					"style" : "",
					"text" : "store 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2479.754883, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 202.285889, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2421.656738, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 144.187744, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2486.838379, 337.078705, 45.0, 22.0 ],
					"style" : "",
					"text" : "store 4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-83",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3045.371094, 621.124512, 97.0, 23.0 ],
					"style" : "",
					"text" : "storagewindow",
					"varname" : "message[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2379.741211, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 102.272217, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2332.718994, 305.067139, 29.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 1035.067139, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2431.838379, 337.078705, 45.0, 22.0 ],
					"style" : "",
					"text" : "store 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2379.741211, 337.078705, 45.0, 22.0 ],
					"style" : "",
					"text" : "store 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2332.718994, 337.078705, 45.0, 22.0 ],
					"style" : "",
					"text" : "store 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 357.0, 234.0, 1165.0, 781.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1121.831055, 182.784302, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1173.831055, 295.784302, 29.5, 22.0 ],
									"style" : "",
									"text" : "2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "preset", "int", "preset", "int" ],
									"patching_rect" : [ 1149.831055, 406.784302, 100.0, 40.0 ],
									"preset_data" : [ 										{
											"number" : 1,
											"data" : [ 6, "obj-12", "gain~", "list", 0, 10.0, 6, "obj-25", "gain~", "list", 0, 10.0, 6, "obj-28", "gain~", "list", 0, 10.0, 6, "obj-30", "gain~", "list", 0, 10.0, 6, "obj-32", "gain~", "list", 0, 10.0, 6, "obj-34", "gain~", "list", 0, 10.0, 6, "obj-36", "gain~", "list", 0, 10.0, 6, "obj-38", "gain~", "list", 0, 10.0, 6, "obj-126", "gain~", "list", 86, 10.0, 6, "obj-124", "gain~", "list", 86, 10.0, 6, "obj-164", "gain~", "list", 0, 0.0, 6, "obj-162", "gain~", "list", 0, 10.0, 6, "obj-169", "gain~", "list", 60, 0.0, 6, "obj-167", "gain~", "list", 60, 10.0, 6, "obj-121", "gain~", "list", 0, 10.0, 6, "obj-119", "gain~", "list", 0, 10.0, 6, "obj-58", "gain~", "list", 0, 10.0, 6, "obj-52", "gain~", "list", 0, 10.0, 5, "<invalid>", "nodes", "nodenumber", 9, 13, "<invalid>", "nodes", "xplace", 0.933353, 0.482025, 0.716814, 0.766962, 0.145742, 0.247788, 0.855457, 0.225664, 0.482025, 13, "<invalid>", "nodes", "yplace", 0.876075, 0.123862, 0.884956, 0.238938, 0.16811, 0.722714, 0.380531, 0.561947, 0.477845, 13, "<invalid>", "nodes", "nsize", 0.266814, 0.363451, 0.231416, 0.333186, 0.226283, 0.412123, 0.437198, 0.438673, 0.240266, 13, "<invalid>", "nodes", "setactive", 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, "<invalid>", "number", "int", 86 ]
										}
, 										{
											"number" : 2,
											"data" : [ 6, "obj-12", "gain~", "list", 0, 10.0, 6, "obj-25", "gain~", "list", 0, 10.0, 6, "obj-28", "gain~", "list", 0, 10.0, 6, "obj-30", "gain~", "list", 0, 10.0, 6, "obj-32", "gain~", "list", 0, 10.0, 6, "obj-34", "gain~", "list", 0, 10.0, 6, "obj-36", "gain~", "list", 0, 10.0, 6, "obj-38", "gain~", "list", 0, 10.0, 6, "obj-126", "gain~", "list", 0, 10.0, 6, "obj-124", "gain~", "list", 0, 10.0, 6, "obj-164", "gain~", "list", 0, 0.0, 6, "obj-162", "gain~", "list", 0, 10.0, 6, "obj-169", "gain~", "list", 0, 0.0, 6, "obj-167", "gain~", "list", 0, 10.0, 6, "obj-121", "gain~", "list", 0, 10.0, 6, "obj-119", "gain~", "list", 0, 10.0, 6, "obj-58", "gain~", "list", 0, 10.0, 6, "obj-52", "gain~", "list", 0, 10.0, 5, "<invalid>", "nodes", "nodenumber", 9, 13, "<invalid>", "nodes", "xplace", 0.933353, 0.482025, 0.716814, 0.766962, 0.145742, 0.247788, 0.855457, 0.225664, 0.482025, 13, "<invalid>", "nodes", "yplace", 0.876075, 0.123862, 0.884956, 0.238938, 0.16811, 0.722714, 0.380531, 0.561947, 0.477845, 13, "<invalid>", "nodes", "nsize", 0.266814, 0.363451, 0.231416, 0.333186, 0.226283, 0.412123, 0.437198, 0.438673, 0.240266, 13, "<invalid>", "nodes", "setactive", 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, "<invalid>", "number", "int", 0 ]
										}
 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1136.831055, 319.784302, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 1136.831055, 255.901978, 69.0, 22.0 ],
									"style" : "",
									"text" : "sel 6 13 14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 230.471375, 102.011566, 94.0, 22.0 ],
									"style" : "",
									"text" : "receive~ TapeR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 114.471375, 105.201843, 92.0, 22.0 ],
									"style" : "",
									"text" : "receive~ TapeL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1169.678955, 845.0, 120.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_DelayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1131.831055, 817.0, 118.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_DelayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1133.678955, 487.215698, 51.0, 22.0 ],
									"style" : "",
									"text" : "r node9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1133.678955, 517.215698, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 120."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1217.437744, 548.0, 99.0, 22.0 ],
									"style" : "",
									"text" : "receive~ DelayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1130.623535, 548.0, 97.0, 22.0 ],
									"style" : "",
									"text" : "receive~ DelayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1202.623535, 609.0, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1180.623535, 609.0, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[9]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1157.678955, 609.0, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1133.678955, 609.0, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[10]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 194.285461, 815.117676, 114.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 81.471375, 815.117676, 112.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 957.935913, 820.666687, 126.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 838.082764, 820.666687, 124.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 650.367126, 841.117615, 112.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_LFOR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 646.033752, 802.450989, 110.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_LFOL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 435.20752, 845.0, 149.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_BrookesideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 409.072571, 815.666687, 147.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_BrookesideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 981.082764, 379.117676, 127.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 940.908936, 401.784393, 125.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 756.842468, 419.784302, 126.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 746.509094, 379.117676, 124.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 581.0, 415.784302, 102.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_criR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 552.082764, 382.450958, 100.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_criL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 414.471375, 419.784302, 108.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 376.623596, 391.784302, 106.0, 22.0 ],
									"style" : "",
									"text" : "send~ node_psyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 136.623596, 493.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-155",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 136.623596, 523.0, 39.0, 22.0 ],
									"style" : "",
									"text" : "* 130"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 221.471375, 629.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-119",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 199.471375, 629.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[15]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-120",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 174.471375, 629.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 152.471375, 629.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[14]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 249.437683, 558.333374, 91.0, 22.0 ],
									"style" : "",
									"text" : "receive~ DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 136.623596, 558.333374, 89.0, 22.0 ],
									"style" : "",
									"text" : "receive~ DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 827.082764, 494.333374, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-171",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 827.082764, 524.333374, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 120."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-166",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 907.082764, 607.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-167",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 885.082764, 607.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[20]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-168",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 860.082764, 607.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-169",
									"interp" : 0.0,
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 838.082764, 607.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[21]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-161",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 692.033691, 612.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-162",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 670.033691, 612.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[18]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-163",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 645.033691, 612.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-164",
									"interp" : 0.0,
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 623.033691, 612.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[19]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-158",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 631.082764, 494.333374, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 631.082764, 524.333374, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 130."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-156",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 451.70752, 491.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-157",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 451.70752, 521.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 120."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-152",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 929.908936, 62.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-153",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 929.908936, 92.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 130."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-144",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 731.31958, 62.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-145",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 731.31958, 92.0, 39.0, 22.0 ],
									"style" : "",
									"text" : "* 130"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-146",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 549.082764, 62.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-147",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 549.082764, 92.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 120."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-148",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 378.471375, 62.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "r node1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-149",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 378.471375, 92.0, 42.0, 22.0 ],
									"style" : "",
									"text" : "* 130."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-127",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 516.842468, 489.0, 55.0, 33.0 ],
									"style" : "",
									"text" : "long\nAcquatic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 522.70752, 607.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 498.70752, 607.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[17]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 473.70752, 607.450928, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-126",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 451.70752, 607.450928, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[16]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 987.908936, 67.0, 60.666664, 33.0 ],
									"style" : "",
									"text" : "Direct\nDistortion"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 807.81958, 83.0, 55.0, 33.0 ],
									"style" : "",
									"text" : "long\nAcquatic"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 941.408936, 567.0, 106.0, 22.0 ],
									"style" : "",
									"text" : "receive~ BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 821.555786, 567.0, 104.0, 22.0 ],
									"style" : "",
									"text" : "receive~ BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 706.501953, 563.784302, 90.0, 22.0 ],
									"style" : "",
									"text" : "receive~ LFOR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 623.033691, 563.784302, 88.0, 22.0 ],
									"style" : "",
									"text" : "receive~ LFOL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 488.675781, 553.0, 127.0, 22.0 ],
									"style" : "",
									"text" : "receive~ BrookesideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 408.70752, 553.0, 125.0, 22.0 ],
									"style" : "",
									"text" : "receive~ BrookesideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1046.387451, 119.45105, 104.0, 22.0 ],
									"style" : "",
									"text" : "receive~ InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 933.573242, 119.45105, 103.0, 22.0 ],
									"style" : "",
									"text" : "receive~ InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 824.81958, 119.450989, 103.0, 22.0 ],
									"style" : "",
									"text" : "receive~ KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 735.81958, 119.450989, 101.0, 22.0 ],
									"style" : "",
									"text" : "receive~ KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 630.082764, 122.784302, 79.0, 22.0 ],
									"style" : "",
									"text" : "receive~ criR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 549.082764, 119.450989, 77.0, 22.0 ],
									"style" : "",
									"text" : "receive~ criL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 462.230286, 122.784302, 86.0, 22.0 ],
									"style" : "",
									"text" : "receive~ psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 375.416077, 122.784302, 84.0, 22.0 ],
									"style" : "",
									"text" : "receive~ psyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1009.908936, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 987.908936, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[8]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 962.908936, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 940.908936, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[7]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 803.471436, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 781.471436, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[6]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 757.471436, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 735.471436, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[5]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 613.082764, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 591.082764, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[4]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 571.082764, 187.117676, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 549.082764, 187.117676, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[3]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 447.416077, 183.784302, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 425.416077, 183.784302, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[2]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 402.471375, 183.784302, 19.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 378.471375, 183.784302, 22.0, 140.0 ],
									"style" : "",
									"varname" : "gain~[1]"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-121", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-120", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-124", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-124", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-126", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-126", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-126", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-145", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-145", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-147", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-146", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-149", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-148", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-149", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-153", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-152", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-153", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-155", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-154", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-155", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-157", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-156", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-157", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-163", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-167", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-167", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-169", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-171", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-171", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberB-1",
								"default" : 								{
									"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1864.51416, 1139.823364, 45.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p node"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1380.733276, 962.720825, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1174.203369, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1358.733276, 962.720825, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1152.203369, 840.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[BubbleR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1333.733276, 962.720825, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1127.203369, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"interp" : 0.0,
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1311.733276, 962.720825, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1105.203369, 840.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[BubbleL]"
				}

			}
, 			{
				"box" : 				{
					"active" : 					{
						"gain~[Contact]" : 0,
						"gain~[DirectL]" : 0,
						"gain~[DirectR]" : 0,
						"gain~[MainL]" : 0,
						"gain~[MainR]" : 0,
						"gain~[Voix]" : 0,
						"live.gain~[Poeme]" : 0,
						"livegain~[Harpe1]" : 0,
						"livegain~[Harpe2]" : 0,
						"bp.Compressor[1]" : 0,
						"bp.Compressor[1]::Attack" : 0,
						"bp.Compressor[1]::Bypass" : 0,
						"bp.Compressor[1]::Input" : 0,
						"bp.Compressor[1]::Output" : 0,
						"bp.Compressor[1]::Ratio" : 0,
						"bp.Compressor[1]::Release" : 0,
						"bp.Compressor[1]::Threshold" : 0,
						"bp.Compressor[1]::slider[2]" : 0,
						"bp.Compressor[1]::slider[3]" : 0
					}
,
					"autorestore" : "interpol4.json",
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2280.268066, 706.871582, 244.0, 23.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 500, 489 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage interpol4 @savemode 0",
					"varname" : "interpol4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1210.121948, 962.720825, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1003.592041, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1188.121948, 962.720825, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 981.592041, 840.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[BrookeR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1163.121948, 962.720825, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 956.592041, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1141.121948, 966.720825, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 934.592041, 844.720825, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[BrookeL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1060.827148, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 854.297241, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1038.827148, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 832.297241, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DirtyR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1013.827148, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 807.297241, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 991.827148, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.297241, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DirtyL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 232.09726, 1186.666626, 57.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 298.542023, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1475.444824, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 278.208679, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1455.11145, 840.720825, 22.0, 140.0 ],
					"style" : "",
					"varname" : "gain~[ReverbR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 253.59726, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1430.5, 840.720825, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 232.09726, 960.710205, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1409.0, 840.720825, 22.0, 140.0 ],
					"style" : "",
					"varname" : "gain~[ReverbL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 34.350647, 1183.333252, 57.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 130.208694, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1227.648193, 110.638428, 29.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 108.208679, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1205.648071, 110.638428, 32.0, 298.0 ],
					"style" : "",
					"varname" : "gain~[DirectR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 85.263916, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1182.703369, 110.638428, 29.0, 298.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 63.763916, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1161.203369, 110.638428, 32.0, 298.0 ],
					"style" : "",
					"varname" : "gain~[DirectL]"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 232.09726, 922.333374, 92.5, 22.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[3]",
							"parameter_shortname" : "vst~",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"annotation_name" : "",
						"parameter_enable" : 1
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshotlist" : 						{
							"current_snapshot" : -1,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Altiverb 6",
									"origin" : "Altiverb 6.vst",
									"type" : "VST",
									"subtype" : "MidiEffect",
									"embed" : 0,
									"snapshot" : 									{
										"pluginname" : "Altiverb 6.vst",
										"plugindisplayname" : "Altiverb 6",
										"pluginsavedname" : "Altiverb 6",
										"pluginsaveduniqueid" : 0,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "11958.CMlaKA....fQPMDZ....ADjUxUC..j.X....A........................................3hd77CdswFH1Ulbyk1at0iHw3BLh.RYtM1ajklam0iHUQkQsfiH+3iB7DBQOMDUYAUQf.GaoMGcf.UUBwTRCAhHs7xKAAGbrU1KuPDUDABTLkzTTARLt.yKuTjSh.hHnQGcvoyKub2c24RXvAGak4xXu01KDQEQy8BTx8FbkIGc4wTZyQWKw3BLtPFcjIhOJvCbrk1bzAhckI2bo8la8HRLt.iH9n.Ojk1Xz4iBIvyZkkmOPElbsMGOurVY44iBIvSXxIWX44iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9DVX0.COuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOPIWYyUFcf3TXsUFOuLGcxklam4iBIjPB7rVY44SZtABbxU1bkQGHl8FajUlb77xZkkmOJjPBIvSZtQWYmUlb9.COujlazU1YkImOJjPBIvyZkkmOtUVYjMGHyElckwyKqUVd9nPBIj.Oo4FckcVYx4SL77RZtQWYmUlb9nPBIj.OqUVd9.mbkMWYzABbgQGZ77xZkkmOJjPBIvybzIWZtclO77xbzIWZtclOJjPBIvyZkkmO1Ulbyk1atwyKqUVd9nPBIj.Oo4FckcVYx4iL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9DVX1.COuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOAwFafLDZ041ZywyKyQmbo41Y9nPBIj.OqUVd9XWYxMWZu4FOurVY44iBIjPB7jlazU1YkImOxvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SXgYSM77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9LkagA2bn8Fc77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4CL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9DVX2.COuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOAUGcu0VXzk1atABTxU1bkQGHI4FYkgGOuLGcxklam4iBIjPB7rVY44iUgwVckABSu41Y77xZkkmOJjPBIvSZtQWYmUlb9.COujlazU1YkImOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOEI0StwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SQgIGa4AhTkYFakMFco8layAxStwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOwvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CQxwjc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9PTZxU1XzAxQgkla77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOv3BL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9PjbCwFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclODklbkMFcfLzar8lb77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOwvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQRwjc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTXxwVdRUlYfbTXo4FOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4CLt.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOEIEQrwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SQgIGa4IUYlABQkwVX4wyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CUrwjc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9PUXowFHGEVZtwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CUrQDa77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9PUXowFHDUFagkGOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4CLt.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclODU1XMwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4iTkYWYxIFHTkVakwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclODU1XwvyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4CSucGHDEVavwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclODU1XxvyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SSoQFHDEVavwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclODU1XyvyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4CRocFZfPTXsAGOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9TTTBwFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOEEEHBE1byABSkYWYrwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQQQkX77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTTfPkbkIFakABSkYWYrwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQQEiY77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTTfvzafXjbkEGOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SLx.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOEEULwwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SQQABSuART77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOw3hL0vyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQQEyY77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTTfvzafbTXo4FOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4CLt.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOEEkLlwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SQQABRoAhQxUVb77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOx.CLvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQQISb77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTTffTZfDEOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SLtHSM77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9TTTxbFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOEEEHHkFHGEVZtwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SSykja77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9zTXyQWYxARRtABSkYWYrwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SSy8Dc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9zTXyQWYxAxS0QGHLUlckwFOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4CLt.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOFIGS1wyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SSgMGckIGHFI2atQGHLUlckwFOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4CLt.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclORUFS1wyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SSgMGckIGHRUVXxABSkYWYrwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CQxcEc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9Pjb48xUkQGHMkFd77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOwvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yPzIDa77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9LTYtQWYxAhPrUVYjwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9zRLzPCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOLYlPrwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4CSlUFHBwVYkQFOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SKwPCM77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LUaVwFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSEVavwVYfvTY1UFa77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOsDCL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LEbLgGOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSAWYgsVYxABSkYFcffEOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SKwvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yTvIEd77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9LEbkE1ZkIGHRk1YnQGHXwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOSA2P3wyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yTvUVXqUlbfLTYtQWYxABV77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOv3BL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LEbqkGOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSAWYgsVYxARV77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOwvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQw8ja77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9TTTf7ja77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4SL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9.EcSgFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSkldkwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DCLvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SQtQFY77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9PUXowFHCUGc77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOsDiLvvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CagQma77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9vTXzUlaikGHM8FYkwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOwvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yTP8ja77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9LEcgcVYf.0aykFco8layAxStwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOvvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yTP0jb77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9vTZtsFHEQVZzwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOwvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CQCIWL77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9vza2AxPx81by8lckIGOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4yLx.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclODMjbxvyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4CRocFZfLjbuM2buYWYxwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9HCMv.COuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOC0VPmwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yPg0FHA41YrUFOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4iL43RLvjyL4jyM4TSMyHiLxbCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOC0FT4wyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yPg0FHYwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9.iKwPCLv.CLv.CL0jiMvPiMzTCOuHWYgwlOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOC0FT5wyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yPg0FHZwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9zBLtHCLv.CLv.CLxjCNvHyLxHCM77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LTaRgFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOCEVafHER77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOsLCMtHCLv.CLvbiMxjyL4PSMyvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yPsIkc77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9LTXsAhTVwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfXDauEFc77xZkkmOJjPBIvibkEFa9DiLtbyLyLCLvHCL4.CM0PSL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LTaZ0FOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSMlbuwFafn0au0FOuLGcxklam4iBIjPB7rVY44iUgwVckAhQr8VXzwyKqUVd9nPBIj.OxUVXr4SL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9b2XZ0FOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOWElckAhVu8Va77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOwvyKxUVXr4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4yci8zb77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9bUX1UFHOYlYyUFc77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHFw1agQGOurVY44iBIjPB7HWYgwlOv3BL77hbkEFa9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9PkXVcGOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOTElXVkVY2wyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOvvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SSy0DY77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9jjTSMlbkUlafzza0MWYfzzajUFOuLGcxklam4iBIjPB7rVY44iUgwVckABSu41Y77xZkkmOJjPBIvSZtQWYmUlb9.COujlazU1YkImOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOiQGagwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yPu4Fcx8FafDDYpU2bz0VYtQGHM8FYkwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOvvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4CQi8ja77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9PTYiEVdf7ja77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4CL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9HUY1MGOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclORUlckI2bkwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOvvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SZxwFY77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9jjTfvzagQFHM8FYkwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOvvyKo4FckcVYx4iBIj.OuPVZiQmOJjPB7PVZiQmOJjPBIvyZkkmOIQDOurVY44iBIjPB7LGcxklam4SZxwFd77xbzIWZtclOJjPBIvyZkkmONEVakwyKqUVd9nPBIj.OyQmbo41Y9jjTfvzagQFHgMGHgUGd77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4CL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9LEaIIEOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOSUFakMFckQFHIIEOuLGcxklam4iBIjPB7rVY44SZxkFY77xZkkmOJjPBIvybzIWZtclOv.CLs.CLvzBLv.SKv.CLs.CLvzBLv.COuLGcxklam4iBIjPB7rVY44SZxAWXzgFOurVY44iBIjPB7LGcxklam4SZzUVay8xPgQGZkQlbgw1buTxPgUlafzBHSEVZtQWKEQWZk4lak8xXk4FckIGHs.xXgIGYo8VZjMGH.ARLxzFOuLGcxklam4iBIjPB7rVY44ickI2bo8la77xZkkmOJjPBIvSZtQWYmUlb9HCOujlazU1YkImOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOhkGbywyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4iP4AWXyMGOuLGcxklam4iBIjPB7rVY44iUgwVckABSu41Y77xZkkmOJjPBIvSZtQWYmUlb9.COujlazU1YkImOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclOoI2bnwyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4yTn81cfbUX1UFOuLGcxklam4iBIjPB7rVY44iUgwVckABSu41Y77xZkkmOJjPBIvSZtQWYmUlb9HCOujlazU1YkImOJjPB77BYoMFc9nPBIvCYoMFc9nPBIj.OqUVd9jDQ77xZkkmOJjPBIvybzIWZtclO1UlbywyKyQmbo41Y9nPBIj.OqUVd93TXsUFOurVY44iBIjPB7LGcxklam4SP0QVZuUTXyUlUkI2bo8la77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4SL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44iPx81cyUlbfLEcgQWcywyKqUVd9nPBIj.Ojk1Xz4iBIjPBIvyZkkmOBI2a2MWYxAxTiI2arwlXgIGOurVY44iBIjPBIvCYoMFc9nPBIjPBIvyZkkmOC8lazUlazABRkk1YnQGOurVY44iBIjPBIj.OxUVXr4CLt.COuHWYgwlOJjPBIjPB7rVY44iUoMWZhwVYfP0avwyKqUVd9nPBIjPBIvibkEFa9.iKvvyKxUVXr4iBIjPBIvyKjk1Xz4iBIjPBIvyZkkmOCgVZrQFHN8FYkMGOurVY44iBIjPBIvSXxIWX44iBIjPBIj.Ojk1Xz4iBIjPBIjPB7rVY44iSg0VY77xZkkmOJjPBIjPBIvybzIWZtclOoQWYsMGOuLGcxklam4iBIjPBIjPB7rVY44yTkwVYiQWYjwyKqUVd9nPBIjPBIj.Oo4FckcVYx4CL77RZtQWYmUlb9nPBIjPBIj.OqUVd9LEcgQWcywyKqUVd9nPBIjPBIj.Oo4FckcVYx4yL77RZtQWYmUlb9nPBIjPBIvyKjk1Xz4iBIjPBIvyKgImbgkmOJjPBIj.OqUVd9jjTfvTZyQGHSMlbuwFahElb77xZkkmOJjPBIj.Ojk1Xz4iBIjPBIj.OqUVd9LzatQWYtQGHHUVZmgFc77xZkkmOJjPBIjPB7HWYgwlOv3BL77hbkEFa9nPBIjPBIvyZkkmOVk1boIFakABUuAGOurVY44iBIjPBIj.OxUVXr4CLt.COuHWYgwlOJjPBIj.OuPVZiQmOJjPBIj.OqUVd93TXsUFOurVY44iBIjPBIvybzIWZtclO77xbzIWZtclOJjPBIj.OqUVd9LUYrU1XzUFY77xZkkmOJjPBIj.Oo4FckcVYx4SL77RZtQWYmUlb9nPBIjPB7rVY44yTzEFc0MGOurVY44iBIjPBIvSZtQWYmUlb9.COujlazU1YkImOJjPBIvyKjk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9nmd0.COuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOIIkPx81cyUlbfLEcgQWcywyKyQmbo41Y9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9PzbMQFOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclODk1bvwVX4ARSuQVY77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4CL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9PjbO4FOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclODklbkMFcf7ja77xbzIWZtclOJjPBIvyZkkmOVEFa0UFHL8lamwyKqUVd9nPBIj.Oo4FckcVYx4SL77RZtQWYmUlb9nPBIvyKjk1Xz4iBIj.Ojk1Xz4iBIjPB7rVY44SRDwyKqUVd9nPBIj.OyQmbo41Y9PEaO4FOuLGcxklam4iBIjPB7rVY44iSg0VY77xZkkmOJjPBIvybzIWZtclOTEVZrAxStwyKyQmbo41Y9nPBIj.OqUVd9XUXrUWYfvzatcFOurVY44iBIjPB7jlazU1YkImOwvyKo4FckcVYx4iBIj.OuPVZiQmOJj.OuDlbxEVd9nPB7rVY44CU4AWY77xZkkmOJj.OyQmbo41Y9zTZyMWZu4FHPIWYyUFc77xbzIWZtclOJj.OqUVd9XUYxMWZu4FOurVY44iBIvSZtQWYmUlb9DCOujlazU1YkImOJvyKjk1Xz4iB77Bbrk1bz4iB"
									}
,
									"fileref" : 									{
										"name" : "Altiverb 6",
										"filename" : "Altiverb 6_20170523.maxsnap",
										"filepath" : "~/Documents/Max 7/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "faa64da1f66cd197b114c64f0a42e949"
									}

								}
 ]
						}

					}
,
					"style" : "",
					"text" : "vst~",
					"varname" : "vst~",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 388.788879, 922.333374, 98.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DelayR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 385.529907, 898.333374, 96.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DelayL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 451.97467, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 107.522217, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 429.97467, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 85.522217, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DelayR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 407.029907, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 62.577454, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"interp" : 20.0,
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 385.529907, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 41.077454, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[DelayL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1311.733276, 921.996826, 106.0, 22.0 ],
					"style" : "",
					"text" : "receive~ BubbleR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1311.733276, 897.996826, 104.0, 22.0 ],
					"style" : "",
					"text" : "receive~ BubbleL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1163.121948, 921.996826, 127.0, 22.0 ],
					"style" : "",
					"text" : "receive~ BrookesideR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1141.121948, 897.996826, 125.0, 22.0 ],
					"style" : "",
					"text" : "receive~ BrookesideL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 991.827148, 917.571899, 91.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DirtyR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 991.827148, 898.333374, 89.0, 22.0 ],
					"style" : "",
					"text" : "receive~ DirtyL",
					"varname" : "receive~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 837.103027, 922.333374, 104.0, 22.0 ],
					"style" : "",
					"text" : "receive~ InsectsR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 837.103027, 898.333374, 103.0, 22.0 ],
					"style" : "",
					"text" : "receive~ InsectsL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 685.406494, 922.333374, 103.0, 22.0 ],
					"style" : "",
					"text" : "receive~ KrystalR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 685.406494, 898.333374, 101.0, 22.0 ],
					"style" : "",
					"text" : "receive~ KrystalL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 537.657471, 922.333374, 86.0, 22.0 ],
					"style" : "",
					"text" : "receive~ psyR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 537.657471, 898.333374, 84.0, 22.0 ],
					"style" : "",
					"text" : "receive~ psyL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 906.103027, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 699.57312, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 884.103027, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 677.57312, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[InsectsR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 859.103027, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 652.57312, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 837.103027, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 630.57312, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[InsectsL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 753.406494, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 546.876587, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 731.406494, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 524.876587, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[KrystalR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 707.406494, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 500.876587, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 685.406494, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 478.876587, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[KrystalL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 606.602173, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 400.072266, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 584.602173, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 378.072266, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[PsyR]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 561.657471, 963.057373, 19.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 355.127563, 841.057373, 19.0, 140.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"interpinlet" : 1,
					"maxclass" : "gain~",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 537.657471, 963.057373, 22.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 331.127563, 841.057373, 22.0, 140.0 ],
					"relative" : 1,
					"style" : "",
					"varname" : "gain~[PsyL]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 259.140503, 735.666687, 66.0, 22.0 ],
					"style" : "",
					"text" : "send~ mic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 280.0, 330.0, 1122.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 527.800415, 54.731964, 130.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 507.300385, 10.0, 128.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-434",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1126.0, 122.781036, 131.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_delayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-437",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 1126.0, 146.781036, 129.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_delayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 757.0, 411.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-110",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 932.311646, 844.932983, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-112",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 932.311646, 806.932983, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-115",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "preset", "int", "preset", "int" ],
									"patching_rect" : [ 931.311646, 891.5, 100.0, 40.0 ],
									"preset_data" : [ 										{
											"number" : 1,
											"data" : [ 6, "obj-161", "gain~", "list", 79, 10.0, 6, "obj-160", "gain~", "list", 80, 10.0, 6, "obj-159", "gain~", "list", 114, 10.0, 6, "obj-158", "gain~", "list", 111, 10.0, 5, "obj-152", "slider", "float", 0.0, 5, "obj-150", "number", "int", 0, 5, "obj-57", "number", "int", 0, 5, "obj-59", "flonum", "float", 100.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 5, "obj-72", "flonum", "float", 100.0, 5, "obj-78", "flonum", "float", 0.0, 5, "obj-82", "flonum", "float", 0.0, 5, "obj-146", "number", "int", 79, 5, "obj-140", "flonum", "float", 0.0, 5, "obj-133", "flonum", "float", 0.0, 5, "obj-170", "toggle", "int", 0, 5, "obj-171", "toggle", "int", 0, 5, "obj-178", "toggle", "int", 1, 5, "obj-88", "toggle", "int", 0, 6, "obj-95", "gain~", "list", 0, 10.0, 6, "obj-96", "gain~", "list", 0, 10.0, 6, "obj-98", "gain~", "list", 0, 10.0, 6, "obj-97", "gain~", "list", 0, 10.0, 5, "obj-8", "toggle", "int", 0, 6, "obj-5", "gain~", "list", 0, 10.0, 5, "obj-102", "toggle", "int", 0, 6, "obj-83", "gain~", "list", 0, 10.0 ]
										}
, 										{
											"number" : 2,
											"data" : [ 6, "obj-161", "gain~", "list", 81, 10.0, 6, "obj-160", "gain~", "list", 80, 10.0, 6, "obj-159", "gain~", "list", 114, 10.0, 6, "obj-158", "gain~", "list", 111, 10.0, 5, "obj-152", "slider", "float", 100.0, 5, "obj-150", "number", "int", 0, 5, "obj-57", "number", "int", 100, 5, "obj-59", "flonum", "float", 100.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 5, "obj-72", "flonum", "float", 100.0, 5, "obj-78", "flonum", "float", 0.0, 5, "obj-82", "flonum", "float", 100.0, 5, "obj-146", "number", "int", 81, 5, "obj-140", "flonum", "float", 0.0, 5, "obj-133", "flonum", "float", 0.0, 5, "obj-170", "toggle", "int", 0, 5, "obj-171", "toggle", "int", 0, 5, "obj-178", "toggle", "int", 1, 5, "obj-88", "toggle", "int", 0, 6, "obj-95", "gain~", "list", 0, 10.0, 6, "obj-96", "gain~", "list", 0, 10.0, 6, "obj-98", "gain~", "list", 0, 10.0, 6, "obj-97", "gain~", "list", 0, 10.0, 5, "obj-8", "toggle", "int", 0, 6, "obj-5", "gain~", "list", 0, 10.0, 5, "obj-102", "toggle", "int", 0, 6, "obj-83", "gain~", "list", 0, 10.0 ]
										}
, 										{
											"number" : 24,
											"data" : [ 6, "obj-96", "gain~", "list", 0, 10.0, 6, "obj-95", "gain~", "list", 0, 10.0, 6, "obj-6", "gain~", "list", 0, 10.0, 6, "obj-5", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "obj-102", "gain~", "list", 0, 10.0, 6, "obj-101", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "obj-20", "gain~", "list", 0, 10.0, 6, "obj-19", "gain~", "list", 0, 10.0, 6, "obj-31", "gain~", "list", 0, 10.0, 6, "obj-30", "gain~", "list", 0, 10.0, 6, "obj-37", "gain~", "list", 1, 10.0, 6, "obj-36", "gain~", "list", 1, 10.0, 6, "obj-40", "gain~", "list", 0, 10.0, 6, "obj-39", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0, 6, "<invalid>", "gain~", "list", 0, 10.0 ]
										}
 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 828.0, 886.0, 58.0, 22.0 ],
									"style" : "",
									"text" : "autopattr",
									"varname" : "u655001215"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 1079.0, 828.0, 57.0, 22.0 ],
									"style" : "",
									"text" : "dac~ 1 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-94",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1213.0, 828.0, 70.0, 22.0 ],
									"style" : "",
									"text" : "send~ HP4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1141.329956, 828.0, 70.0, 22.0 ],
									"style" : "",
									"text" : "send~ HP3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 234.67572, 74.0, 87.0, 22.0 ],
									"style" : "",
									"text" : "120, 40 20000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 52.956924, 128.114349, 63.0, 22.0 ],
									"style" : "",
									"text" : "delay 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 127.956924, 74.0, 87.0, 22.0 ],
									"style" : "",
									"text" : "40, 120 25000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 127.956924, 104.114349, 43.0, 22.0 ],
									"style" : "",
									"text" : "line 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 507.300385, 320.996674, 45.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-117",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 494.800385, 639.0, 70.0, 22.0 ],
									"style" : "",
									"text" : "send~ HP4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-116",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 297.17572, 639.0, 70.0, 22.0 ],
									"style" : "",
									"text" : "send~ HP3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 469.800385, 102.451004, 103.0, 22.0 ],
									"style" : "",
									"text" : "receive~ poemeL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-114",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 651.338745, 97.333389, 105.0, 22.0 ],
									"style" : "",
									"text" : "receive~ poemeR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 292.838745, 1191.0, 98.0, 22.0 ],
									"style" : "",
									"text" : "send~ GeneralR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 75.456909, 1184.0, 96.0, 22.0 ],
									"style" : "",
									"text" : "send~ GeneralL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-113",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 659.859436, 460.833313, 19.852783, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-111",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 478.779175, 460.833313, 19.852783, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 621.022034, 786.068604, 129.0, 22.0 ],
									"style" : "",
									"text" : "receive~ verb_harpeR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 624.022034, 707.068604, 127.0, 22.0 ],
									"style" : "",
									"text" : "receive~ verb_harpeL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 621.022034, 758.068604, 133.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_harpeR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 624.022034, 678.068604, 131.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_harpeL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 263.800415, 857.5, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 263.800415, 822.333313, 45.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 336.17572, 372.0, 45.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 678.068604, 131.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_delayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 689.637207, 129.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_delayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 630.838745, 460.833313, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 629.338745, 341.333313, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 629.338745, 379.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 455.205566, 460.833313, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 453.705566, 341.333313, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 453.705566, 379.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 227.300415, 947.833252, 31.0, 22.0 ],
									"style" : "",
									"text" : "130"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 336.17572, 411.0, 31.0, 22.0 ],
									"style" : "",
									"text" : "120"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 294.338745, 1000.333252, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-98",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 76.956909, 1003.833252, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 245.45694, 460.833313, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 27.623592, 460.833313, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 263.800415, 891.5, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 27.0, 959.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 402.0, 963.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 118.779175, 1003.833252, 19.852783, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 328.623596, 1006.833252, 21.0, 134.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 781.068604, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 737.568604, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 731.068604, 115.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_criR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 762.637207, 113.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_criL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 755.068604, 139.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 786.637207, 137.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 702.068604, 121.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 713.637207, 119.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_psyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 925.5, 130.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo2R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 921.068604, 128.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo2L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 901.5, 130.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 897.068604, 128.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 877.5, 126.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 879.068604, 124.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 853.5, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 861.068604, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 832.333313, 156.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrooksideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 834.637207, 154.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrooksideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 424.0, 808.333313, 127.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.0, 810.637207, 125.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-180",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 889.522034, 451.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-181",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 848.522034, 451.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 845.272034, 417.66333, 69.0, 22.0 ],
									"style" : "",
									"text" : "sel 1 18 26"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-183",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 845.272034, 386.996643, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-178",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 917.240295, 503.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-179",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 917.240295, 540.666687, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-226",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 183.16333, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-227",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 159.16333, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-224",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 104.114349, 139.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-225",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 80.114349, 137.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-222",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 146.781067, 115.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_criR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-223",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 122.781036, 113.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_criL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-219",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 54.731964, 121.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-218",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 30.731964, 119.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_psyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-177",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 63.212219, 148.333389, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-176",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 23.623592, 137.333344, 29.5, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-174",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 11.290259, 89.996666, 69.0, 22.0 ],
									"style" : "",
									"text" : "sel 1 17 23"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-173",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.290259, 59.330002, 85.0, 22.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-171",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 243.95694, 341.333313, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 243.95694, 379.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 23.623592, 349.333313, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-168",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 23.623592, 387.0, 41.0, 22.0 ],
									"style" : "",
									"text" : "gate~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-129",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1322.5, 313.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-130",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1811.75, 217.0, 37.0, 20.0 ],
									"style" : "",
									"text" : "sel 21"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-131",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1811.75, 306.0, 44.0, 22.0 ],
									"style" : "",
									"text" : "80000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1811.75, 273.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-133",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1811.75, 379.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-134",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1811.75, 356.0, 46.0, 22.0 ],
									"style" : "",
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-135",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1811.75, 331.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "0, 100 $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-136",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1566.25, 226.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "sel 5"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-137",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1566.25, 356.0, 46.0, 22.0 ],
									"style" : "",
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-138",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1566.25, 331.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "100, 0 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-139",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1566.25, 250.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-140",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1566.25, 379.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-141",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1566.25, 306.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "* 10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-142",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1566.25, 282.0, 64.0, 20.0 ],
									"style" : "",
									"text" : "random 600"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-143",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1089.0, 582.0, 29.5, 20.0 ],
									"style" : "",
									"text" : "110"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-144",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1019.0, 550.0, 39.0, 20.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-146",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1407.0, 615.0, 50.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-147",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1039.0, 589.0, 29.5, 20.0 ],
									"style" : "",
									"text" : "80"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1189.0, 285.0, 39.0, 20.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1737.75, 217.0, 37.0, 20.0 ],
									"style" : "",
									"text" : "sel 20"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1484.25, 217.0, 37.0, 20.0 ],
									"style" : "",
									"text" : "sel 19"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 1363.75, 217.0, 37.0, 20.0 ],
									"style" : "",
									"text" : "sel 18"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1363.75, 160.784302, 72.0, 20.0 ],
									"style" : "",
									"text" : "r cue_number"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-87",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1189.0, 313.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-86",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1290.0, 313.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "3"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-85",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1257.5, 313.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "2"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1223.75, 313.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-82",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1408.25, 461.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-81",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1408.25, 413.0, 73.0, 20.0 ],
									"style" : "",
									"text" : "switch 4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-76",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1737.75, 297.0, 51.0, 22.0 ],
									"style" : "",
									"text" : "150000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1737.75, 264.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-78",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1737.75, 370.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1737.75, 347.0, 46.0, 22.0 ],
									"style" : "",
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1737.75, 322.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "100, 0 $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1484.25, 347.0, 46.0, 22.0 ],
									"style" : "",
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-75",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1484.25, 322.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "0, 100 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1484.25, 241.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-72",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1484.25, 370.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1484.25, 297.0, 32.5, 20.0 ],
									"style" : "",
									"text" : "* 10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1484.25, 273.0, 64.0, 20.0 ],
									"style" : "",
									"text" : "random 600"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1443.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1413.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-64",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1363.75, 273.0, 51.0, 22.0 ],
									"style" : "",
									"text" : "130000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1363.75, 241.0, 20.0, 20.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"format" : 6,
									"id" : "obj-59",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1363.75, 346.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1363.75, 323.0, 46.0, 22.0 ],
									"style" : "",
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-61",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1363.75, 298.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "0, 100 $1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"htricolor" : [ 0.87, 0.82, 0.24, 1.0 ],
									"id" : "obj-57",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1247.0, 507.0, 42.0, 22.0 ],
									"style" : "",
									"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"tricolor" : [ 0.75, 0.75, 0.75, 1.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"htricolor" : [ 0.87, 0.82, 0.24, 1.0 ],
									"id" : "obj-150",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1145.0, 447.0, 42.0, 22.0 ],
									"style" : "",
									"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"tricolor" : [ 0.75, 0.75, 0.75, 1.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-151",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 1213.0, 481.0, 27.0, 19.0 ],
									"style" : "",
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-152",
									"maxclass" : "slider",
									"numinlets" : 1,
									"numoutlets" : 1,
									"orientation" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1213.0, 459.0, 76.0, 20.0 ],
									"size" : 101.0,
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-153",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1382.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1352.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-155",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1322.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-157",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numleds" : 20,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1290.0, 789.068604, 31.0, 98.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-158",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1167.944458, 632.0, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1192.5, 632.0, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-160",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1146.5, 632.0, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-161",
									"interpinlet" : 1,
									"maxclass" : "gain~",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1127.5, 632.0, 20.0, 140.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1126.0, 389.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-162",
									"maxclass" : "newobj",
									"numinlets" : 10,
									"numoutlets" : 10,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 1,
											"architecture" : "x86",
											"modernui" : 1
										}
,
										"rect" : [ 10.0, 59.0, 749.0, 426.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 118.0, 147.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 199.0, 143.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 276.0, 117.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 342.0, 146.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 408.0, 178.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 465.0, 109.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-7",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 526.0, 140.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-8",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 588.0, 169.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-9",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 660.0, 198.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"id" : "obj-10",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 32.0, 161.0, 55.0, 17.0 ],
													"style" : "",
													"text" : "delay~ 0 0"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-11",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 127.0, 251.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-12",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 212.0, 244.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-13",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 673.0, 247.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-14",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 268.0, 238.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-15",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 338.0, 240.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 401.0, 238.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-17",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 458.0, 242.0, 16.0, 16.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-18",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 513.0, 246.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-19",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 587.0, 245.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-20",
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 52.0, 252.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-21",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 119.0, 68.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-22",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 523.0, 70.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-23",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 205.0, 70.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-24",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 280.0, 69.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-25",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "list" ],
													"patching_rect" : [ 345.0, 67.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-26",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 406.0, 64.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-27",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 667.0, 59.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-28",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 592.0, 70.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-29",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 464.0, 74.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-30",
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "signal" ],
													"patching_rect" : [ 42.0, 72.0, 15.0, 15.0 ],
													"style" : ""
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-20", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-21", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-22", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-25", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-26", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-28", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-19", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"disabled" : 0,
													"hidden" : 0,
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 1126.0, 589.0, 201.0, 19.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "p ls-delayss"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.156863, 0.8, 0.54902, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-164",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 1124.0, 545.0, 113.0, 22.0 ],
									"style" : "",
									"text" : "matrix~ 1 4 smooth"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-165",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 1145.0, 472.0, 27.0, 19.0 ],
									"style" : "",
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-166",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1126.0, 417.5, 157.0, 19.0 ],
									"style" : "",
									"text" : "define_loudspeakers 2 -45 -20 20 45"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-167",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "list", "int", "int", "int", "float" ],
									"patching_rect" : [ 1124.0, 507.0, 50.5, 22.0 ],
									"style" : "",
									"text" : "vbap 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-238",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 472.0, 130.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo2R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-239",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 448.0, 128.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo2L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-236",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 425.996643, 130.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1R"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-237",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 401.996643, 128.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_Evo1L"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-234",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 377.996643, 126.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-235",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 353.996643, 124.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-232",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 329.329956, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-233",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 305.329956, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-230",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 284.829956, 156.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrooksideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-231",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 260.829956, 154.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrooksideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-228",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 236.829956, 127.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-229",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 939.240234, 212.829956, 125.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 323.633698, 67.000008, 133.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DelayR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 323.633698, 28.333389, 131.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DelayL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 469.800385, 196.784317, 127.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 469.800385, 153.451004, 125.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_DirtyL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 651.338745, 271.0, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 651.338745, 220.549026, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BubbleL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 651.338745, 187.000015, 126.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 651.338745, 148.333389, 124.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_LFOL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 469.800385, 256.549011, 162.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrookesideR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 469.800385, 227.215714, 160.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_BrookesideL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 205.17572, 167.784409, 140.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 215.842392, 148.333389, 138.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_InsectsL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 310.509064, 284.829956, 139.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 265.95694, 224.117691, 137.0, 22.0 ],
									"style" : "",
									"text" : "receive~ norm_KrystalL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 52.956924, 267.784302, 114.0, 22.0 ],
									"style" : "",
									"text" : "receive~ node_criR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 52.956924, 234.450943, 112.0, 22.0 ],
									"style" : "",
									"text" : "receive~ node_criL"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 52.956924, 196.784317, 120.0, 22.0 ],
									"style" : "",
									"text" : "receive~ node_psyR"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 52.956924, 167.784409, 118.0, 22.0 ],
									"style" : "",
									"text" : "receive~ node_psyL"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-166", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-110", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-110", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-114", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-129", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-130", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-130", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-132", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 4 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-133", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-134", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-135", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-139", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-140", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-137", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-138", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-142", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-139", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-141", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-141", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-142", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-158", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-143", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-143", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-143", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-147", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-144", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-147", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-165", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-150", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-151", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-151", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-151", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-152", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-152", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-154", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-153", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-155", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-160", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-161", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-146", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-161", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-157", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-161", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-158", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-160", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-161", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-162", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 4 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-164", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-165", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-165", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-166", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-164", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-167", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-168", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-171", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-174", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-173", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-176", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-174", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-174", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-174", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-170", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-171", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-176", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-170", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-171", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-178", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-181", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-180", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-182", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-181", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-182", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-181", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-183", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-218", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-219", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-222", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-223", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-224", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-225", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-226", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-227", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-228", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-229", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-230", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-231", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-232", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-233", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-234", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-235", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-236", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-237", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-238", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-239", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-434", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-179", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-437", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-116", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-177", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-62", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-181", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-139", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-74", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-152", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-113", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-117", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-152", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-168", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-92", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-92", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-116", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-117", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-97", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-97", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "", -1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-174", 2 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "multislider001",
								"default" : 								{
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
									"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "myteststyle",
								"default" : 								{
									"accentcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-2",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-3",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-2",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-3",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-2",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-3",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberB-1",
								"default" : 								{
									"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "panelGold-1",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.764706, 0.592157, 0.101961, 0.25 ],
										"color1" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "texteditGold",
								"default" : 								{
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
									"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1864.51416, 1109.17395, 89.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p spatialisation",
					"varname" : "spatialisation"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-106",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1807.909058, 1387.84668, 123.0, 123.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 590.517578, 118.111099, 102.111053, 102.111053 ],
					"prototypename" : "helpfile",
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"grad1" : [ 0.466667, 0.254902, 0.607843, 1.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"id" : "obj-70",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 844.368469, 1225.113403, 568.012878, 608.668823 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"grad1" : [ 0.862745, 0.741176, 0.137255, 1.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"id" : "obj-456",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -2.06364, 775.965454, 1770.0, 446.069092 ],
					"presentation" : 1,
					"presentation_rect" : [ -5.0, 7.898438, 834.0, 268.069092 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"grad1" : [ 0.0, 0.533333, 0.168627, 1.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"id" : "obj-457",
					"ignoreclick" : 0,
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -2.06364, -14.774536, 2238.864258, 788.73999 ],
					"presentation" : 1,
					"presentation_rect" : [ 825.409363, 7.898438, 760.367004, 775.69165 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"grad1" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"id" : "obj-455",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -13.419647, 1222.422485, 858.0, 607.178223 ],
					"presentation" : 1,
					"presentation_rect" : [ -5.0, 272.077515, 834.0, 511.512573 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"grad1" : [ 0.011765, 0.396078, 0.752941, 1.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"id" : "obj-458",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2238.800537, -18.073364, 2269.819336, 792.73999 ],
					"presentation" : 1,
					"presentation_rect" : [ -5.0, 785.590088, 1590.776367, 286.73999 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-121", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-126", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-320", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-321", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-158", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-158", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-169", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-462", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-182", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-193", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-196", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-198", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-208", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-210", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-217", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 23 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 22 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 23 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-217", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-460", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-221", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-251", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-452", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-451", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-387", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-249", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-387", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-249", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-251", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-254", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-253", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-256", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-258", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-262", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-263", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-270", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-277", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1500.990112, 1375.773926, 1482.170288, 1375.773926, 1482.170288, 1282.365356, 1500.990112, 1282.365356 ],
					"source" : [ "obj-276", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-277", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-280", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-451", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-501", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-280", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-284", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-299", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-439", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-286", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-289", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-292", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-290", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-459", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-459", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-489", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-290", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-291", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-295", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-276", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-297", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-299", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-311", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-314", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-302", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-303", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-304", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-305", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-305", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-372", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-306", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-310", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-309", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-310", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-311", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-311", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-308", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-311", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-307", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-312", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-312", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-314", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1499.954468, 1521.092773, 1476.992554, 1521.092773, 1476.992554, 1425.78064, 1499.954468, 1425.78064 ],
					"source" : [ "obj-313", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-314", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-313", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-314", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-315", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-316", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-405", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-316", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-317", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-329", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-330", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-328", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-331", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-327", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-332", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-333", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-323", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-334", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-324", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-335", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-358", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-336", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-322", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-338", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-322", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-339", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-375", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-348", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-349", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-349", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-414", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-355", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-352", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-357", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-353", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-350", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-354", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-351", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-355", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-357", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-356", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-371", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1777.010864, 1364.030396, 1758.763184, 1364.030396, 1758.763184, 1308.611206, 1777.010864, 1308.611206 ],
					"source" : [ "obj-361", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-371", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-363", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-362", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-364", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-359", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-365", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-360", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-366", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-365", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-368", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-375", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-369", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-361", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-371", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-348", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-372", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-369", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-372", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-369", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-372", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-375", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1951.638062, 1364.030396, 1933.390137, 1364.030396, 1933.390137, 1308.611206, 1951.638062, 1308.611206 ],
					"source" : [ "obj-376", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-377", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-376", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-378", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-363", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-379", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-377", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-379", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-381", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-312", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-386", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-387", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-388", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-391", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-389", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-391", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-389", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-392", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-389", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-387", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-390", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-391", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-392", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-366", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-393", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-407", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-394", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-408", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-395", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-394", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-400", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-395", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-406", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-413", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-409", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-347", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-415", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-410", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-409", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-411", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-410", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-336", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-414", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-417", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-416", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-416", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-419", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-420", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-444", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2134.90332, 1514.314941, 2114.097412, 1514.314941, 2114.097412, 1410.906494, 2134.90332, 1410.906494 ],
					"source" : [ "obj-422", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-424", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-286", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-425", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-444", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-428", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-421", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-444", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-422", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-444", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-453", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2135.90332, 1362.367065, 2112.941162, 1362.367065, 2112.941162, 1263.814087, 2135.90332, 1263.814087 ],
					"source" : [ "obj-449", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-453", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-450", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-445", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-453", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-449", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-453", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-459", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-459", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 598.750977, 384.205566, 268.640503, 384.205566 ],
					"source" : [ "obj-459", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-459", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-465", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-463", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-473", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-465", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-471", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-466", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-474", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-466", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-469", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1820.5, 1673.564575, 1797.537842, 1673.564575, 1797.537842, 1582.156128, 1820.5, 1582.156128 ],
					"source" : [ "obj-467", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-469", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-468", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-454", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-469", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-467", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-469", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-492", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-473", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-496", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-494", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-495", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-315", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-495", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-317", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-495", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-495", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-496", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-499", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-497", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-498", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-498", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-499", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-524", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-503", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-503", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-505", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-509", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-507", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-511", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-509", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-513", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-509", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-513", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-509", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-523", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-511", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-523", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-513", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-517", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-515", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-518", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-516", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-516", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-517", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-486", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-518", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-433", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-520", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-438", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-520", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-520", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-519", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-521", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-520", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-522", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-524", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-523", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-524", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-541", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-542", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-544", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-543", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-544", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-543", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-545", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-543", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-546", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-550", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-546", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-548", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-547", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-545", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-545", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-545", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-546", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-546", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-546", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-548", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-549", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-549", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-550", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-552", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-551", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-544", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-552", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-553", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-434", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-437", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-67", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-68", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-78", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-293", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-459", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-374", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-491", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-363", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "", -1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-461::obj-52" : [ "Threshold[1]", "Threshold", 0 ],
			"obj-461::obj-28" : [ "Attack[1]", "Attack", 0 ],
			"obj-1::obj-90::obj-7" : [ "vst~[15]", "vst~[2]", 0 ],
			"obj-464::obj-93" : [ "battery.life", "battery.life", 0 ],
			"obj-464::obj-270" : [ "tilt.enable", "tilt.enable", 0 ],
			"obj-249" : [ "amxd~", "amxd~", 0 ],
			"obj-1::obj-106::obj-7" : [ "vst~[17]", "vst~[2]", 0 ],
			"obj-1::obj-1" : [ "vst~", "vst~", 0 ],
			"obj-464::obj-191" : [ "roll.value", "roll.value", 0 ],
			"obj-1::obj-107::obj-7" : [ "vst~[18]", "vst~[2]", 0 ],
			"obj-464::obj-69" : [ "tilt.min", "tilt.min", 0 ],
			"obj-464::obj-183" : [ "pan.value", "pan.value", 0 ],
			"obj-459::obj-2" : [ "Output", "Output", 0 ],
			"obj-464::obj-178" : [ "select.mode", "select.mode", 0 ],
			"obj-459::obj-78" : [ "Ratio", "Ratio", 0 ],
			"obj-461::obj-35" : [ "slider[5]", "slider[2]", 0 ],
			"obj-464::obj-60" : [ "roll.max", "roll.max", 0 ],
			"obj-464::obj-61" : [ "roll.min", "roll.min", 0 ],
			"obj-464::obj-267" : [ "pan.enable", "pan.enable", 0 ],
			"obj-374" : [ "battery.life[1]", "battery.life", 0 ],
			"obj-1::obj-105::obj-7" : [ "vst~[16]", "vst~[2]", 0 ],
			"obj-158" : [ "live.gain~[Harpe]", "live.gain~[Harpe]", 0 ],
			"obj-1::obj-108::obj-7" : [ "vst~[19]", "vst~[2]", 0 ],
			"obj-72::obj-27" : [ "live.gain~", "live.gain~", 0 ],
			"obj-78" : [ "live.gain~[Harpe2]", "live.gain~", 0 ],
			"obj-459::obj-47" : [ "Release", "Release", 0 ],
			"obj-464::obj-214" : [ "tilt.invert", "tilt.invert", 0 ],
			"obj-461::obj-12" : [ "Bypass[1]", "Bypass", 0 ],
			"obj-464::obj-117" : [ "choose.haptic", "choose.haptic", 0 ],
			"obj-461::obj-78" : [ "Ratio[1]", "Ratio", 0 ],
			"obj-464::obj-147" : [ "led.blue", "blue", 0 ],
			"obj-464::obj-263" : [ "roll.enable", "roll.enable", 0 ],
			"obj-1::obj-43" : [ "vst~[21]", "vst~[2]", 0 ],
			"obj-474" : [ "battery.life[2]", "battery.life", 0 ],
			"obj-464::obj-64" : [ "pan.max", "pan.max", 0 ],
			"obj-1::obj-7::obj-7" : [ "vst~[20]", "vst~[2]", 0 ],
			"obj-464::obj-65" : [ "pan.min", "pan.min", 0 ],
			"obj-459::obj-44" : [ "Input", "Input", 0 ],
			"obj-68" : [ "vst~[3]", "vst~", 0 ],
			"obj-463" : [ "recenter[1]", "recenter", 0 ],
			"obj-459::obj-52" : [ "Threshold", "Threshold", 0 ],
			"obj-464::obj-94" : [ "recenter", "recenter", 0 ],
			"obj-461::obj-34" : [ "slider[4]", "slider[3]", 0 ],
			"obj-461::obj-47" : [ "Release[1]", "Release", 0 ],
			"obj-464::obj-116" : [ "trigger.haptic", "trigger.haptic", 0 ],
			"obj-464::obj-143" : [ "led.green", "green", 0 ],
			"obj-464::obj-196" : [ "tilt.value", "tilt.value", 0 ],
			"obj-464::obj-215" : [ "pan.invert", "pan.invert", 0 ],
			"obj-459::obj-28" : [ "Attack", "Attack", 0 ],
			"obj-464::obj-106" : [ "resend.data", "resend.data", 0 ],
			"obj-459::obj-35" : [ "slider[2]", "slider[2]", 0 ],
			"obj-461::obj-2" : [ "Output[1]", "Output", 0 ],
			"obj-1::obj-99" : [ "vst~[12]", "vst~[2]", 0 ],
			"obj-461::obj-44" : [ "Input[1]", "Input", 0 ],
			"obj-464::obj-138" : [ "led.red", "red", 0 ],
			"obj-464::obj-221" : [ "roll.invert", "roll.invert", 0 ],
			"obj-464::obj-68" : [ "tilt.max", "tilt.max", 0 ],
			"obj-459::obj-12" : [ "Bypass", "Bypass", 0 ],
			"obj-208" : [ "live.gain~[Poeme]", "live.gain~[Poeme]", 0 ],
			"obj-459::obj-34" : [ "slider[3]", "slider[3]", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "Altiverb 6_20170523.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "interpol4.json",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "GRM Delays_20180519.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "GRM Evolution_20161002.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Psychotica.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Guitar Rig 5.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Guitar Rig 5_20161002_3.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Guitar Rig 5_20161002_4.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Guitar Rig 5_20161002_5.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Guitar Rig 5_20161002_6.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Krystal.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Insects.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Dirty.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Brookside.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Bubble.maxpat",
				"bootpath" : "~/Documents/Brice/Harpe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Dual Harmonizer.amxd.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Dual Harmonizer.amxd_20170210.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Dual Harmonizer.amxd[1].maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Dual Harmonizer.amxd",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.bal2~.maxpat",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.vdelay~.maxpat",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bp.Compressor.maxpat",
				"bootpath" : "C74:/packages/BEAP/clippings/BEAP/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "vbap.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "PAt_style0",
				"default" : 				{
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"clearcolor" : [ 1.0, 0.947758, 0.687073, 1.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"color" : [ 0.952941, 0.564706, 0.098039, 1.0 ],
					"fontname" : [ "Arial" ],
					"bgcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
					"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : [ 12.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "grey comment",
				"default" : 				{
					"clearcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
					"fontname" : [ "Inconsolata" ],
					"textcolor" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "multislider001",
				"default" : 				{
					"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "myteststyle",
				"default" : 				{
					"accentcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-2",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-3",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-2",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-3",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-2",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-3",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberB-1",
				"default" : 				{
					"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "panelGold-1",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.764706, 0.592157, 0.101961, 0.25 ],
						"color1" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "textbutton004",
				"default" : 				{
					"selectioncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
					"fontname" : [ "Inconsolata" ],
					"centerjust" : [ 0 ],
					"bgcolor" : [ 0.858824, 0.866667, 0.878431, 1.0 ],
					"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "default",
				"multi" : 0
			}
, 			{
				"name" : "texteditGold",
				"default" : 				{
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "umenu001",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color" : [ 0.862745, 0.870588, 0.878431, 0.82 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"color" : [ 0.4478, 0.484701, 0.47346, 1.0 ],
					"fontname" : [ "Inconsolata" ],
					"fontface" : [ 0 ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "chiba",
				"multi" : 0
			}
, 			{
				"name" : "umenu002",
				"default" : 				{
					"textjustification" : [ 1 ],
					"textcolor_inverse" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.501961, 0.501961, 0.501961, 1.0 ],
						"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"color" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
					"fontname" : [ "Inconsolata" ],
					"elementcolor" : [ 0.901961, 0.901961, 0.901961, 1.0 ],
					"fontsize" : [ 13.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "umenu003",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"fontname" : [ "Inconsolata" ],
					"fontsize" : [ 16.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "umenu004",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color1" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"color2" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
						"color" : [ 0.65098, 0.65098, 0.65098, 0.898039 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"fontname" : [ "Inconsolata" ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "chiba",
				"multi" : 0
			}
 ]
	}

}
