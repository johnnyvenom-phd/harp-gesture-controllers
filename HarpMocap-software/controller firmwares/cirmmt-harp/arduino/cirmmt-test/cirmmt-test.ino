#include <Wire.h>
#include "lib/MPU9250.h"
#include "Adafruit_DRV2605.h"

#include <WiFiUdp.h>
#include <ESP8266WiFi.h>

#include "lib/OSCMessage.h"
// #include <OSCMessage.h>

// extern "C" {
// #include "user_interface.h"
// }

WiFiUDP udp;

// Define the SSID and password of network
const char* ssid = "bodysuit";
const char* password = "bodysuit";

// For device #100: 
#define LISTEN_PORT 5000  // Port for incoming OSC messages
#define SEND_PORT 58038   // Port for outgoing OSC messages
IPAddress home(192, 168, 1, 100); // Static IP address of module
IPAddress broadcast(192, 168, 1, 255);
IPAddress mask(255, 255, 255, 0);

// Static IP of receiving client
IPAddress sendIP(192, 168, 1, 111);




void setup() {

  Serial.begin(38400);
  delay(500);

  // Setup the wifi connection
  WiFi.disconnect();
  delay(200);
  WiFi.begin(ssid, password);         // Connect to Wifi network
  WiFi.config(home, broadcast, mask); // Obtain a static IP address
  delay(200);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    // if (SERIAL_DEBUG) {
    //   Serial.print(".");
    // }
  }

  udp.begin(LISTEN_PORT);

}


void loop() {


    OSCMessage msg("/q");
    msg.add(1);
    udp.beginPacket(sendIP, SEND_PORT);
    msg.send(udp);
    udp.endPacket();

  yield();
  delay(50); // added this wait function, have to research if it does any good
}
