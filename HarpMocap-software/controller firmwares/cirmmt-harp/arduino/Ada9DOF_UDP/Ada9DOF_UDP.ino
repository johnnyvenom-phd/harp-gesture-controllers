// Libraries for the Adafruit 9DOF sensor:
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_9DOF.h>
#include <Adafruit_L3GD20_U.h>

// For WiFi and UDP
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// #include <OSCMessage.h>
#include "lib/OSCMessage.h"

#define LISTEN_PORT 5000 // Port for incoming OSC messages
#define SEND_PORT 58038 // Port for outgoing OSC messages

/* my home network */
const char* ssid = "Johnny Venom Base";
const char* password = "doublebass";

IPAddress deviceIP(192, 168, 2, 100); // Static IP address for device
IPAddress gateway(192, 168, 2, 255); // Broadcast address

/* bodysuit network
const char* ssid = "bodysuit";
const char* password = "bodysuit";

IPAddress deviceIP(192, 168, 1, 100); // Static IP address for device
IPAddress gateway(192, 168, 1, 255); // Broadcast address
*/
IPAddress subnet(255, 255, 255, 0); // Subnet mask
IPAddress clientIP(192, 168, 1, 111); // Static IP of client

char incomingPacket[255];
char replyPacket[] = "Messaged received."; 

WiFiUDP Udp; 
Adafruit_9DOF dof = Adafruit_9DOF();
Adafruit_L3GD20_Unified gyro = Adafruit_L3GD20_Unified(20);
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(30302);

void initSensors()
{
	if(!gyro.begin())
	{
	/* There was a problem detecting the L3GD20 ... check your connections */
	Serial.println("Ooops, no L3GD20 detected ... Check your wiring!");
	while(1);
	}

	if(!accel.begin())
	{
	/* There was a problem detecting the LSM303 ... check your connections */
	Serial.println(F("Ooops, no LSM303 detected ... Check your wiring!"));
	while(1);
	}
	
	if(!mag.begin())
	{
	/* There was a problem detecting the LSM303 ... check your connections */
	Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
	while(1);
	}
}

void setup()
{
	Serial.begin(115200);
	Serial.println();

	Serial.printf("Connecting to %s\n", ssid);
	WiFi.begin(ssid, password); 
	WiFi.config(deviceIP, gateway, subnet);

	Serial.print("Connecting");
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println();

	Serial.print("Connected, IP address: ");
	Serial.println(WiFi.localIP());

	initSensors();

	Udp.begin(LISTEN_PORT);
	Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), LISTEN_PORT);
}

void loop()
{
	int packetSize = Udp.parsePacket();
	if (packetSize)
	{
		Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
		int len = Udp.read(incomingPacket, 255);
		if (len > 0) {
			incomingPacket[len] = 0; 
		}
		Serial.printf("UDP packet contents: %s\n", incomingPacket);

		// Serial.println("Received from Max. Formatting to OSC.");		    
		// for(int i=0; i<10; i++){
		// 	delay(250);
		// 	Serial.print(".");
		// }
		// Serial.println();
		// Serial.println("Sending to Max over UDP.");		    

		OSCMessage msg("/info");
		msg.add(incomingPacket);

		clientIP = Udp.remoteIP();

		Udp.beginPacket(clientIP, SEND_PORT);
		msg.send(Udp); // send the bytes to the SLIP stream
		Udp.endPacket(); // mark the end of the OSC Packet
		msg.empty(); // free space occupied by message
	}

	sensors_event_t accel_event;
	sensors_event_t mag_event;
	sensors_vec_t orientation; 

	accel.getEvent(&accel_event);
	mag.getEvent(&mag_event);

	if (dof.fusionGetOrientation(&accel_event, &mag_event, &orientation))
	{
		OSCMessage msg_orientation("/rpy");
		msg_orientation.add(orientation.roll); 
		msg_orientation.add(orientation.pitch); 
		msg_orientation.add(orientation.heading);
		Udp.beginPacket(clientIP, SEND_PORT);
		msg_orientation.send(Udp);
		Udp.endPacket();
		msg_orientation.empty(); 
	}

	delay(20);

}
