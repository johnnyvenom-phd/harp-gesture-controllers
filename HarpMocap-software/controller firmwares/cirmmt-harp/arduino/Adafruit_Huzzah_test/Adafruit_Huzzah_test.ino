// For WiFi and UDP
//#include <ESP8266WiFi.h>
// #include <WiFiUdp.h>

// This sketch tests:
//    basic serial connection (passed) 
//    read sensor data (passed)
//    send digital and analog (PWM) signals (passed)
//    drive motor (failed)

const int pwmPin = 12;      // send PWM signal to driver
const int drvPin = 13;      // DRV2603 enable pin..
const int ledPin = 14;      // led feedback

// read an analog sensor (max voltage ~1.0V)
void readAnalog() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(10);
}

// drive motor with DRV2603
void pulse() {
  digitalWrite(drvPin, HIGH);
  analogWrite(pwmPin, 255);
  analogWrite(ledPin, 255);
  Serial.println("HIGH");
  delay(1000);

  analogWrite(pwmPin, 127);
  analogWrite(ledPin, 127);
  Serial.println("MED");
  delay(1000);
  
  analogWrite(pwmPin, 0);
  analogWrite(ledPin, 0);
  digitalWrite(drvPin, LOW);
  Serial.println("LOW");
  delay(1000);
}


void setup()
{
	Serial.begin(115200);
 
  pinMode(A0, INPUT);
  pinMode(pwmPin, OUTPUT);
  pinMode(drvPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
}

void loop()
{

  String outMessage = "";
  
	while (Serial.available()) {
    char inChar = Serial.read(); 
    if (inChar == 10) {
      Serial.println();
    } else {
      outMessage.concat(inChar); 
    }
	}

  if (outMessage != "") {
    Serial.print(outMessage);
  }

  readAnalog();
  pulse();

}
