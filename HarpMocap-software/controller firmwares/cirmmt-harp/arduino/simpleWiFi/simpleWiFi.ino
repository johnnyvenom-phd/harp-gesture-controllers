#include <ESP8266WiFi.h>


int timeCheck = 2000;
int lastCheck = 0;

void setup()
{
	Serial.begin(115200);
	Serial.println();

	WiFi.begin("bodysuit", "bodysuit");

	Serial.print("Connecting");
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println();

	Serial.print("Connected, IP address: ");
	Serial.println(WiFi.localIP());
}

void loop()
{
	// do stuff eventually. 

	if(millis() > lastCheck+timeCheck) {
	    Serial.print("Connected, IP address: ");
	    Serial.print(WiFi.localIP());
	    Serial.print(" at ");
	    Serial.print(millis());
	    Serial.println("ms elapsed.");

	    lastCheck = millis(); 
	}
}