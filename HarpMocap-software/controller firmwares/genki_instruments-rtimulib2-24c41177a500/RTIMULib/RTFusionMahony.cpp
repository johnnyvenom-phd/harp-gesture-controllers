////////////////////////////////////////////////////////////////////////////
//
//  This file is part of RTIMULib
//
//  Copyright (c) 2014-2015, richards-tech, LLC
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of
//  this software and associated documentation files (the "Software"), to deal in
//  the Software without restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//  Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include "RTFusionMahony.h"
#include "RTIMUSettings.h"

// Free parameters in the Mahony filter and fusion scheme
// Kp for proportional feedback, Ki for integral
#define Kp 2.0f * 5.0f
#define Ki 0.0f


RTFusionMahony::RTFusionMahony()
{
    reset();
}

RTFusionMahony::~RTFusionMahony()
{
}

void RTFusionMahony::reset()
{
    m_firstTime = true;
    m_fusionPose = RTVector3();
    m_fusionQPose.fromEuler(m_fusionPose);
    m_gyro = RTVector3();
    m_accel = RTVector3();
    m_compass = RTVector3();
    m_measuredPose = RTVector3();
    m_measuredQPose.fromEuler(m_measuredPose);
}


void RTFusionMahony::update()
{
  RTFLOAT gx = m_gyro.x();
  RTFLOAT gy = m_gyro.y();
  RTFLOAT gz = m_gyro.z();
  RTFLOAT ax = m_accel.x();
  RTFLOAT ay = m_accel.y();
  RTFLOAT az = m_accel.z();
  RTFLOAT mx = m_compass.x();
  RTFLOAT my = m_compass.y();
  RTFLOAT mz = m_compass.z();


  RTFLOAT q1 = m_stateQ.scalar(), q2 = m_stateQ.x(), q3 = m_stateQ.y(), q4 = m_stateQ.z();   // short name local variable for readability

  RTFLOAT norm;
  RTFLOAT hx, hy, bx, bz;
  RTFLOAT vx, vy, vz, wx, wy, wz;
  RTFLOAT ex, ey, ez;
  RTFLOAT pa, pb, pc;

  // Auxiliary variables to avoid repeated arithmetic
  RTFLOAT q1q1 = q1 * q1;
  RTFLOAT q1q2 = q1 * q2;
  RTFLOAT q1q3 = q1 * q3;
  RTFLOAT q1q4 = q1 * q4;
  RTFLOAT q2q2 = q2 * q2;
  RTFLOAT q2q3 = q2 * q3;
  RTFLOAT q2q4 = q2 * q4;
  RTFLOAT q3q3 = q3 * q3;
  RTFLOAT q3q4 = q3 * q4;
  RTFLOAT q4q4 = q4 * q4;

  // Normalise accelerometer measurement
  norm = sqrt(ax * ax + ay * ay + az * az);
  if (norm == 0.0f) return; // handle NaN
  norm = 1.0f / norm;        // use reciprocal for division
  ax *= norm;
  ay *= norm;
  az *= norm;

  // Normalise magnetometer measurement
  norm = sqrt(mx * mx + my * my + mz * mz);
  if (norm == 0.0f) return; // handle NaN
  norm = 1.0f / norm;        // use reciprocal for division
  mx *= norm;
  my *= norm;
  mz *= norm;

  // Reference direction of Earth's magnetic field
  hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
  hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
  bx = sqrt((hx * hx) + (hy * hy));
  bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

  // Estimated direction of gravity and magnetic field
  vx = 2.0f * (q2q4 - q1q3);
  vy = 2.0f * (q1q2 + q3q4);
  vz = q1q1 - q2q2 - q3q3 + q4q4;
  wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
  wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
  wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

  // Error is cross product between estimated direction and measured direction of gravity
  ex = (ay * vz - az * vy) + (my * wz - mz * wy);
  ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
  ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
  if (Ki > 0.0f) {
    m_eInt[0] += ex;      // accumulate integral error
    m_eInt[1] += ey;
    m_eInt[2] += ez;
  } else {
    m_eInt[0] = 0.0f;     // prevent integral wind up
    m_eInt[1] = 0.0f;
    m_eInt[2] = 0.0f;
  }

  // Apply feedback terms
  gx = gx + Kp * ex + Ki * m_eInt[0];
  gy = gy + Kp * ey + Ki * m_eInt[1];
  gz = gz + Kp * ez + Ki * m_eInt[2];

  // Integrate rate of change of quaternion
  pa = q2;
  pb = q3;
  pc = q4;
  q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * m_timeDelta);
  q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * m_timeDelta);
  q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * m_timeDelta);
  q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * m_timeDelta);

  // Normalise quaternion
  m_stateQ.setScalar(q1);
  m_stateQ.setX(q2);
  m_stateQ.setY(q3);
  m_stateQ.setZ(q4);
  m_stateQ.normalize();
}

void RTFusionMahony::newIMUData(RTIMU_DATA& data, const RTIMUSettings *settings)
{
    if (m_enableGyro)
        m_gyro = data.gyro;
    else
        m_gyro = RTVector3();
    m_accel = data.accel;
    m_compass = data.compass;
    m_compassValid = data.compassValid;

    if (m_firstTime) {
        m_lastFusionTime = data.timestamp;
        calculatePose(m_accel, m_compass, settings->m_compassAdjDeclination);

        m_stateQ.fromEuler(m_measuredPose);
        m_fusionQPose = m_stateQ;
        m_fusionPose = m_measuredPose;
        m_firstTime = false;
    } else {
        m_timeDelta = (RTFLOAT)(data.timestamp - m_lastFusionTime) / (RTFLOAT)1000000;
        m_lastFusionTime = data.timestamp;
        if (m_timeDelta <= 0)
            return;

        if (m_debug) {
            HAL_INFO("\n------\n");
            HAL_INFO1("IMU update delta time: %f\n", m_timeDelta);
        }

        calculatePose(data.accel, data.compass, settings->m_compassAdjDeclination);

        update();
        m_stateQ.toEuler(m_fusionPose);
        m_fusionQPose = m_stateQ;

        m_debug = true;
        if (m_debug) {
            HAL_INFO(RTMath::displayRadians("Measured pose", m_measuredPose));
            HAL_INFO(RTMath::displayRadians("RTQF pose", m_fusionPose));
            HAL_INFO(RTMath::displayRadians("Measured quat", m_measuredPose));
            HAL_INFO(RTMath::display("RTQF quat", m_stateQ));
         }
    }

    data.fusionPoseValid = true;
    data.fusionQPoseValid = true;
    data.fusionPose = m_fusionPose;
    data.fusionQPose = m_fusionQPose;
}
