////////////////////////////////////////////////////////////////////////////
//
//  This file is part of RTIMULib
//
//  Copyright (c) 2014-2015, richards-tech, LLC
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of
//  this software and associated documentation files (the "Software"), to deal in
//  the Software without restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//  Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include "RTFusionMadgwick.h"
#include "RTIMUSettings.h"


RTFusionMadgwick::RTFusionMadgwick()
{
    reset();
}

RTFusionMadgwick::~RTFusionMadgwick()
{
}

void RTFusionMadgwick::reset()
{
    m_firstTime = true;
    m_fusionPose = RTVector3();
    m_fusionQPose.fromEuler(m_fusionPose);
    m_gyro = RTVector3();
    m_accel = RTVector3();
    m_compass = RTVector3();
    m_measuredPose = RTVector3();
    m_measuredQPose.fromEuler(m_measuredPose);
}


float RTFusionMadgwick::fast_inv_sqrt(float x)
{
    unsigned int i = 0x5F1F1412 - (*(unsigned int*)&x >> 1);
    float tmp = *(float*)&i;
    return tmp * (1.69000231f - 0.714158168f * x * tmp * tmp);
}
//{
//  // See http://diydrones.com/forum/topics/madgwick-imu-ahrs-and-fast-inverse-square-root
// float halfx = 0.5f * x;
// float y = x;
// int32_t i = *(int32_t *)&y;
// i = 0x5f3759df - (i>>1);
// y = *(float*)&i;
// y = y * (1.5f - (halfx * y * y));
// return y;
//}


void RTFusionMadgwick::update(){
  float recipNorm;
  float s0, s1, s2, s3;
  float qDot1, qDot2, qDot3, qDot4;
  float hx, hy;
  float _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;
  RTFLOAT gx = m_gyro.x();
  RTFLOAT gy = m_gyro.y();
  RTFLOAT gz = m_gyro.z();
  RTFLOAT ax = m_accel.x();
  RTFLOAT ay = m_accel.y();
  RTFLOAT az = m_accel.z();
  RTFLOAT mx = m_compass.x();
  RTFLOAT my = m_compass.y();
  RTFLOAT mz = m_compass.z();
  RTFLOAT q0 = m_stateQ.scalar(), q1 = m_stateQ.x(), q2 = m_stateQ.y(), q3 = m_stateQ.z();   // short name local variable for readability


// Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
//if((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f)) {
//    MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az);
//    return;
//}

// Rate of change of quaternion from gyroscope
qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

    // Normalise accelerometer measurement
    recipNorm = fast_inv_sqrt(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Normalise magnetometer measurement
    recipNorm = fast_inv_sqrt(mx * mx + my * my + mz * mz);
    mx *= recipNorm;
    my *= recipNorm;
    mz *= recipNorm;

    // Auxiliary variables to avoid repeated arithmetic
    _2q0mx = 2.0f * q0 * mx;
    _2q0my = 2.0f * q0 * my;
    _2q0mz = 2.0f * q0 * mz;
    _2q1mx = 2.0f * q1 * mx;
    _2q0 = 2.0f * q0;
    _2q1 = 2.0f * q1;
    _2q2 = 2.0f * q2;
    _2q3 = 2.0f * q3;
    _2q0q2 = 2.0f * q0 * q2;
    _2q2q3 = 2.0f * q2 * q3;
    q0q0 = q0 * q0;
    q0q1 = q0 * q1;
    q0q2 = q0 * q2;
    q0q3 = q0 * q3;
    q1q1 = q1 * q1;
    q1q2 = q1 * q2;
    q1q3 = q1 * q3;
    q2q2 = q2 * q2;
    q2q3 = q2 * q3;
    q3q3 = q3 * q3;

    // Reference direction of Earth's magnetic field
    hx = mx * q0q0 - _2q0my * q3 + _2q0mz * q2 + mx * q1q1 + _2q1 * my * q2 + _2q1 * mz * q3 - mx * q2q2 - mx * q3q3;
    hy = _2q0mx * q3 + my * q0q0 - _2q0mz * q1 + _2q1mx * q2 - my * q1q1 + my * q2q2 + _2q2 * mz * q3 - my * q3q3;
    _2bx = sqrt(hx * hx + hy * hy);
    _2bz = -_2q0mx * q2 + _2q0my * q1 + mz * q0q0 + _2q1mx * q3 - mz * q1q1 + _2q2 * my * q3 - mz * q2q2 + mz * q3q3;
    _4bx = 2.0f * _2bx;
    _4bz = 2.0f * _2bz;

    // Gradient decent algorithm corrective step
    s0 = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) - _2bz * q2 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q3 + _2bz * q1) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q2 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s1 = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q1 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + _2bz * q3 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q2 + _2bz * q0) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q3 - _4bz * q1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s2 = -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q2 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + (-_4bx * q2 - _2bz * q0) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q1 + _2bz * q3) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q0 - _4bz * q2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s3 = _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) + (-_4bx * q3 + _2bz * q1) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q0 + _2bz * q2) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q1 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    recipNorm = fast_inv_sqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
    s0 *= recipNorm;
    s1 *= recipNorm;
    s2 *= recipNorm;
    s3 *= recipNorm;

    // Apply feedback step
    qDot1 -= m_beta * s0;
    qDot2 -= m_beta * s1;
    qDot3 -= m_beta * s2;
    qDot4 -= m_beta * s3;
}

// Integrate rate of change of quaternion to yield quaternion
q0 += qDot1 * m_timeDelta;
q1 += qDot2 * m_timeDelta;
q2 += qDot3 * m_timeDelta;
q3 += qDot4 * m_timeDelta;

// Normalise quaternion
m_stateQ.setScalar(q0);
m_stateQ.setX(q1);
m_stateQ.setY(q2);
m_stateQ.setZ(q3);
m_stateQ.normalize();
}


//{
//  RTFLOAT gx = m_gyro.x();
//  RTFLOAT gy = m_gyro.y();
//  RTFLOAT gz = m_gyro.z();
//  RTFLOAT ax = m_accel.x();
//  RTFLOAT ay = m_accel.y();
//  RTFLOAT az = m_accel.z();
//  RTFLOAT mx = m_compass.x();
//  RTFLOAT my = m_compass.y();
//  RTFLOAT mz = m_compass.z();


//  RTFLOAT q1 = m_stateQ.scalar(), q2 = m_stateQ.x(), q3 = m_stateQ.y(), q4 = m_stateQ.z();   // short name local variable for readability
//  RTFLOAT norm;
//  RTFLOAT hx, hy, _2bx, _2bz;
//  RTFLOAT s1, s2, s3, s4;
//  RTFLOAT qDot1, qDot2, qDot3, qDot4;

//  // Auxiliary variables to avoid repeated arithmetic
//  RTFLOAT _2q1mx;
//  RTFLOAT _2q1my;
//  RTFLOAT _2q1mz;
//  RTFLOAT _2q2mx;
//  RTFLOAT _4bx;
//  RTFLOAT _4bz;
//  RTFLOAT _2q1 = 2 * q1;
//  RTFLOAT _2q2 = 2 * q2;
//  RTFLOAT _2q3 = 2 * q3;
//  RTFLOAT _2q4 = 2 * q4;
//  RTFLOAT _2q1q3 = 2 * q1 * q3;
//  RTFLOAT _2q3q4 = 2 * q3 * q4;
//  RTFLOAT q1q1 = q1 * q1;
//  RTFLOAT q1q2 = q1 * q2;
//  RTFLOAT q1q3 = q1 * q3;
//  RTFLOAT q1q4 = q1 * q4;
//  RTFLOAT q2q2 = q2 * q2;
//  RTFLOAT q2q3 = q2 * q3;
//  RTFLOAT q2q4 = q2 * q4;
//  RTFLOAT q3q3 = q3 * q3;
//  RTFLOAT q3q4 = q3 * q4;
//  RTFLOAT q4q4 = q4 * q4;

//  // Normalise accelerometer measurement
//  norm = fast_inv_sqrt(ax * ax + ay * ay + az * az);
//  if (norm == 0) return; // handle NaN
//  ax *= norm;
//  ay *= norm;
//  az *= norm;

//  // Normalise magnetometer measurement
//  norm = fast_inv_sqrt(mx * mx + my * my + mz * mz);
//  if (norm == 0) return; // handle NaN
//  mx *= norm;
//  my *= norm;
//  mz *= norm;

//  // Reference direction of Earth's magnetic field
//  _2q1mx = 2 * q1 * mx;
//  _2q1my = 2 * q1 * my;
//  _2q1mz = 2 * q1 * mz;
//  _2q2mx = 2 * q2 * mx;
//  hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
//  hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
//  _2bx = (RTFLOAT)sqrt(hx * hx + hy * hy);
//  _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
//  _4bx = 2 * _2bx;
//  _4bz = 2 * _2bz;

//  // Gradient decent algorithm corrective step
//  s1 = -_2q3 * (2 * q2q4 - _2q1q3 - ax) + _2q2 * (2 * q1q2 + _2q3q4 - ay) - _2bz * q3 * (_2bx * (0.5 - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz);
//  s2 = _2q4 * (2 * q2q4 - _2q1q3 - ax) + _2q1 * (2 * q1q2 + _2q3q4 - ay) - 4 * q2 * (1 - 2 * q2q2 - 2 * q3q3 - az) + _2bz * q4 * (_2bx * (0.5 - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
//  s3 = -_2q1 * (2 * q2q4 - _2q1q3 - ax) + _2q4 * (2 * q1q2 + _2q3q4 - ay) - 4 * q3 * (1 - 2 * q2q2 - 2 * q3q3 - az) + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
//  s4 = _2q2 * (2 * q2q4 - _2q1q3 - ax) + _2q3 * (2 * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5 - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz);
//  norm = fast_inv_sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
//  s1 *= norm;
//  s2 *= norm;
//  s3 *= norm;
//  s4 *= norm;

//  // Compute rate of change of quaternion
//  qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - m_beta * s1;
//  qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - m_beta * s2;
//  qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - m_beta * s3;
//  qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - m_beta * s4;

//  // Integrate to yield quaternion
//  q1 += qDot1 * m_timeDelta;
//  q2 += qDot2 * m_timeDelta;
//  q3 += qDot3 * m_timeDelta;
//  q4 += qDot4 * m_timeDelta;

//  m_stateQ.setScalar(q1);
//  m_stateQ.setX(q2);
//  m_stateQ.setY(q3);
//  m_stateQ.setZ(q4);
//  m_stateQ.normalize();
//}

void RTFusionMadgwick::newIMUData(RTIMU_DATA& data, const RTIMUSettings *settings)
{
    if (m_enableGyro)
        m_gyro = data.gyro;
    else
        m_gyro = RTVector3();
    m_accel = data.accel;
    m_compass = data.compass;
    m_compassValid = data.compassValid;

    if (m_firstTime) {
        m_lastFusionTime = data.timestamp;
        calculatePose(m_accel, m_compass, settings->m_compassAdjDeclination);

        m_stateQ.fromEuler(m_measuredPose);
        m_fusionQPose = m_stateQ;
        m_fusionPose = m_measuredPose;
        m_firstTime = false;
    } else {
        m_timeDelta = (RTFLOAT)(data.timestamp - m_lastFusionTime) / (RTFLOAT)1000000;
        m_lastFusionTime = data.timestamp;
        if (m_timeDelta <= 0)
            return;

        if (m_debug) {
            HAL_INFO("\n------\n");
            HAL_INFO1("IMU update delta time: %f\n", m_timeDelta);
        }

        calculatePose(data.accel, data.compass, settings->m_compassAdjDeclination);

        update();
        m_stateQ.toEuler(m_fusionPose);
        m_fusionQPose = m_stateQ;

        m_debug = true;
        if (m_debug) {
            HAL_INFO(RTMath::displayRadians("Measured pose", m_measuredPose));
            HAL_INFO(RTMath::displayRadians("RTQF pose", m_fusionPose));
            HAL_INFO(RTMath::displayRadians("Measured quat", m_measuredPose));
            HAL_INFO(RTMath::display("RTQF quat", m_stateQ));
         }
    }

    data.fusionPoseValid = true;
    data.fusionQPoseValid = true;
    data.fusionPose = m_fusionPose;
    data.fusionQPose = m_fusionQPose;
}
