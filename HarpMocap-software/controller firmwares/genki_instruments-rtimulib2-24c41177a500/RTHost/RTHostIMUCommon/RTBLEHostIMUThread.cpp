////////////////////////////////////////////////////////////////////////////
//
//  This file is part of RTIMULib
//
//  Copyright (c) 2014, richards-tech
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of
//  this software and associated documentation files (the "Software"), to deal in
//  the Software without restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//  Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include "RTBLEHostIMUThread.h"
#include "RTHostIMUClient.h"
#include "RTIMUSettings.h"

#include "RTArduLinkUtils.h"

#include <QTextStream>
#include <QDateTime>

#include <QDebug>

RTBLEHostIMUThread::RTBLEHostIMUThread() : QObject()
{
    m_imu = NULL;
    m_settings = new RTIMUSettings();                       // just use default name (RTIMULib.ini) for settings file
    m_timer = -1;
            
    m_discoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    scan();
}

RTBLEHostIMUThread::~RTBLEHostIMUThread()
{

}

void RTBLEHostIMUThread::newIMU()
{
    m_lock.lock();

    if (m_imu != NULL) {
        delete m_imu;
        m_imu = NULL;
    }

    if (m_timer != -1) {
        killTimer(m_timer);
        m_timer = -1;
    }

    m_imu = new RTHostIMUClient(m_settings);
    m_lock.unlock();

    emit IMURunning();

    //  set up IMU

    m_imu->IMUInit();

    m_timer = startTimer(m_imu->IMUGetPollInterval());
}

void RTBLEHostIMUThread::initThread()
{
    //  create IMU. There's a special function call for this
    //  as it makes sure that the required one is created as specified in the settings.

    m_imu = new RTHostIMUClient(m_settings);
    emit IMURunning();

    //  set up IMU

    m_imu->IMUInit();

    //  poll at the rate suggested by the IMU

    m_timer = startTimer(m_imu->IMUGetPollInterval());
}

void RTBLEHostIMUThread::finishThread()
{
    if (m_timer != -1)
        killTimer(m_timer);

    m_timer = -1;

    if (m_imu != NULL)
        delete m_imu;

    m_imu = NULL;

    delete m_settings;
}


//----------------------------------------------------------
//
//  The following is some Qt threading stuff

void RTBLEHostIMUThread::resumeThread()
{
    m_thread = new QThread();
    moveToThread(m_thread);
    connect(m_thread, SIGNAL(started()), this, SLOT(internalRunLoop()));
    connect(this, SIGNAL(internalEndThread()), this, SLOT(cleanup()));
    connect(this, SIGNAL(internalKillThread()), m_thread, SLOT(quit()));
    connect(m_thread, SIGNAL(finished()), m_thread, SLOT(deleteLater()));
    connect(m_thread, SIGNAL(finished()), this, SLOT(deleteLater()));
    m_thread->start();
}

void RTBLEHostIMUThread::scan() {
    QTextStream out(stdout);
    out << "Starting scan" << endl;

    connect(m_discoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));
    m_discoveryAgent->start();
}

void RTBLEHostIMUThread::scanFinished() {
    QTextStream out(stdout);
    out << "Finished" << endl;

    QList<QBluetoothDeviceInfo> results = m_discoveryAgent->discoveredDevices();

    for(int i=0; i<results.length(); ++i) {
        QBluetoothDeviceInfo deviceInfo = results[i];
        out << deviceInfo.name() << ", " << deviceInfo.address().toString() << endl;

        if(deviceInfo.address().toString().compare(PERIPHERAL_ADDRESS) == 0) {
            connectTo(deviceInfo);
            break;
        }
    }
}

void RTBLEHostIMUThread::connectTo(QBluetoothDeviceInfo &deviceInfo) {
    m_controller = new QLowEnergyController(deviceInfo);

    connect(m_controller, SIGNAL(error(QLowEnergyController::Error)), this, SLOT(controllerError(QLowEnergyController::Error)));

    QTextStream out(stdout);
    out << "Connecting to " << deviceInfo.name() << endl;

    connect(m_controller, SIGNAL(connected()), this, SLOT(deviceConnected()));
    m_controller->setRemoteAddressType(QLowEnergyController::RandomAddress);

    m_controller->connectToDevice();
}

void RTBLEHostIMUThread::deviceConnected() {
    QTextStream out(stdout);
    out << "Device connected" << endl;

    connect(m_controller, SIGNAL(discoveryFinished()), this, SLOT(servicesDiscovered()));
    m_controller->discoverServices();
}

void RTBLEHostIMUThread::servicesDiscovered() {
    QTextStream out(stdout);

    QList<QBluetoothUuid> serviceUUIDs = m_controller->services();
    QList<QLowEnergyService*> services;

    out << "Discovered services: " << endl;
    QList<QBluetoothUuid>::iterator i;
    for(i=serviceUUIDs.begin(); i != serviceUUIDs.end(); ++i) {
        out << i->toString() << endl;
    }

    m_genkiService = m_controller->createServiceObject(QBluetoothUuid((quint16) 0xf000));

    connect(m_genkiService, SIGNAL(stateChanged(QLowEnergyService::ServiceState)), this, SLOT(serviceStateChanged(QLowEnergyService::ServiceState)));
    connect(m_genkiService, SIGNAL(characteristicChanged(QLowEnergyCharacteristic, QByteArray)), this, SLOT(newData(QLowEnergyCharacteristic, QByteArray)));
    connect(m_genkiService, SIGNAL(descriptorWritten(QLowEnergyDescriptor, QByteArray)), this, SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor, QByteArray)));

    m_genkiService->discoverDetails();
}

void RTBLEHostIMUThread::serviceStateChanged(QLowEnergyService::ServiceState state) {
    QTextStream out(stdout);

    switch(state) {
        case QLowEnergyService::ServiceDiscovered: {
            out << "Service discovered" << endl;
            const QLowEnergyCharacteristic imuChar = m_genkiService->characteristic(QBluetoothUuid((quint16) 0xf002));
            out << "Char valid? " << imuChar.isValid() << endl;
            out << "Char handle: " << imuChar.handle() << endl;

            const QLowEnergyDescriptor imuCCCD = imuChar.descriptor(QBluetoothUuid(QBluetoothUuid::ClientCharacteristicConfiguration));
            if(imuChar.isValid()) {
                out << "Listening, descriptor: " << imuCCCD.handle() << endl;
                m_genkiService->writeDescriptor(imuCCCD, QByteArray::fromHex("0100"));
            }

            break;
        }

        default:
            // Nothing
            break;
    }
}

void RTBLEHostIMUThread::newData(QLowEnergyCharacteristic charact, QByteArray received_data) {
    QTextStream out(stdout);

    switch(charact.handle()) {
        case 0x0016: {
            quint64 now = QDateTime::currentMSecsSinceEpoch();
            quint64 dt_ms = now - m_lastDataTime;
            m_lastDataTime = now;

            memcpy(&m_quaternionData, received_data.data()+1, sizeof(m_quaternionData));
            out << "New quaternion data: " << m_quaternionData[0] << " " << m_quaternionData[1] << " " << m_quaternionData[2] << " " << m_quaternionData[3] << ", dt = " << dt_ms << endl;

            break;
        }

        case 0x0019: {
            float raw_data[3];
            int datatype = received_data[0];

            if(datatype == 1) {
                memcpy(&m_rawAccData, received_data.data()+1, sizeof(m_rawAccData));

                out << "Raw accel data: " << m_rawAccData[0] << " " << m_rawAccData[1] << " " << m_rawAccData[2] << endl;
            }

            if(datatype == 2) {
                memcpy(&m_rawGyrData, received_data.data()+1, sizeof(m_rawGyrData));

                out << "Raw gyr data: " << m_rawGyrData[0] << " " << m_rawGyrData[1] << " " << m_rawGyrData[2] << endl;
            }

            if(datatype == 3) {
                memcpy(&m_rawMagData, received_data.data()+1, sizeof(m_rawMagData));

                out << "Raw mag data: " << m_rawMagData[0] << " " << m_rawMagData[1] << " " << m_rawMagData[2] << endl;
                RTIMU_DATA data;
                data.accel.fromArray(m_rawAccData);
                data.gyro.fromArray(m_rawGyrData);
                data.compass.fromArray(m_rawMagData);
                data.timestamp = 0.01;

                emit newIMUData(data);
            }

            break;
        }

        default:
            break;
    }
}

void RTBLEHostIMUThread::confirmedDescriptorWrite(QLowEnergyDescriptor desc, QByteArray data) {
    QTextStream out(stdout);

    out << "Descriptor written successfully: " << desc.uuid().toString() << endl;
}

void RTBLEHostIMUThread::controllerError(QLowEnergyController::Error error) {
    QTextStream out(stdout);

//    out << "Error: " << error.errorString() << endl;
}
