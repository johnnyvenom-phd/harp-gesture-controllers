#ifndef GENKI_BLE_RECEIVER_H
#define GENKI_BLE_RECEIVER_H

#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include "RTIMULibDefs.h"
#include "RTHostIMUGL.h"

#define PERIPHERAL_ADDRESS "D3:14:97:B5:C8:FE"

class GenkiBleReceiver : public QObject
{
    Q_OBJECT
public:
    GenkiBleReceiver();
    void scan();
    void connectTo(QBluetoothDeviceInfo &deviceInfo);

    void setIMUGL(RTHostIMUGL *);

private:
    QBluetoothDeviceDiscoveryAgent *m_discoveryAgent;
    QLowEnergyService *m_genkiService;
    QLowEnergyController *m_controller;


    quint64 m_lastDataTime;

    float m_quaternionData[4];

    float m_rawAccData[3];
    float m_rawGyrData[3];
    float m_rawMagData[3];

    RTHostIMUGL *m_imuGL;

private slots:
    void scanFinished();
    void deviceConnected();
    void servicesDiscovered();
    void serviceStateChanged(QLowEnergyService::ServiceState state);
    void newData(QLowEnergyCharacteristic characteristic, QByteArray data);
    void confirmedDescriptorWrite(QLowEnergyDescriptor desc, QByteArray received_data);
    void controllerError(QLowEnergyController::Error error);
};

#endif // GENKI_BLE_RECEIVER_H
