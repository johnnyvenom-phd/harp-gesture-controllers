#include "genki_ble_receiver.hpp"
#include "RTBLEHostIMUThread.h"

#include <QTextStream>
#include <QDateTime>


GenkiBleReceiver::GenkiBleReceiver()
{
    m_discoveryAgent = new QBluetoothDeviceDiscoveryAgent();
}

void GenkiBleReceiver::setIMUGL(RTHostIMUGL *imugl)
{
    m_imuGL = imugl;
}

void GenkiBleReceiver::scan() {
    QTextStream out(stdout);
    out << "Starting scan" << endl;

    connect(m_discoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));
    m_discoveryAgent->start();
}

void GenkiBleReceiver::scanFinished() {
    QTextStream out(stdout);
    out << "Finished" << endl;

    QList<QBluetoothDeviceInfo> results = m_discoveryAgent->discoveredDevices();

    for(int i=0; i<results.length(); ++i) {
        QBluetoothDeviceInfo deviceInfo = results[i];
        out << deviceInfo.name() << ", " << deviceInfo.address().toString() << endl;

        if(deviceInfo.address().toString().compare(PERIPHERAL_ADDRESS) == 0) {
            connectTo(deviceInfo);
            break;
        }
    }
}

void GenkiBleReceiver::connectTo(QBluetoothDeviceInfo &deviceInfo) {
    m_controller = new QLowEnergyController(deviceInfo);

    connect(m_controller, SIGNAL(error(QLowEnergyController::Error)), this, SLOT(controllerError(QLowEnergyController::Error)));

    QTextStream out(stdout);
    out << "Connecting to " << deviceInfo.name() << endl;

    connect(m_controller, SIGNAL(connected()), this, SLOT(deviceConnected()));
    m_controller->setRemoteAddressType(QLowEnergyController::RandomAddress);

    m_controller->connectToDevice();
}

void GenkiBleReceiver::deviceConnected() {
    QTextStream out(stdout);
    out << "Device connected" << endl;

    connect(m_controller, SIGNAL(discoveryFinished()), this, SLOT(servicesDiscovered()));
    m_controller->discoverServices();
}

void GenkiBleReceiver::servicesDiscovered() {
    QTextStream out(stdout);

    QList<QBluetoothUuid> serviceUUIDs = m_controller->services();
    QList<QLowEnergyService*> services;

    out << "Discovered services: " << endl;
    QList<QBluetoothUuid>::iterator i;
    for(i=serviceUUIDs.begin(); i != serviceUUIDs.end(); ++i) {
        out << i->toString() << endl;
    }

    m_genkiService = m_controller->createServiceObject(QBluetoothUuid((quint16) 0xf000));

    connect(m_genkiService, SIGNAL(stateChanged(QLowEnergyService::ServiceState)), this, SLOT(serviceStateChanged(QLowEnergyService::ServiceState)));
    connect(m_genkiService, SIGNAL(characteristicChanged(QLowEnergyCharacteristic, QByteArray)), this, SLOT(newData(QLowEnergyCharacteristic, QByteArray)));
    connect(m_genkiService, SIGNAL(descriptorWritten(QLowEnergyDescriptor, QByteArray)), this, SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor, QByteArray)));

    m_genkiService->discoverDetails();
}

void GenkiBleReceiver::serviceStateChanged(QLowEnergyService::ServiceState state) {
    QTextStream out(stdout);

    switch(state) {
        case QLowEnergyService::ServiceDiscovered: {
            out << "Service discovered" << endl;
            const QLowEnergyCharacteristic imuChar = m_genkiService->characteristic(QBluetoothUuid((quint16) 0xf002));
            out << "Char valid? " << imuChar.isValid() << endl;
            out << "Char handle: " << imuChar.handle() << endl;

            const QLowEnergyDescriptor imuCCCD = imuChar.descriptor(QBluetoothUuid(QBluetoothUuid::ClientCharacteristicConfiguration));
            if(imuChar.isValid()) {
                out << "Listening, descriptor: " << imuCCCD.handle() << endl;
                m_genkiService->writeDescriptor(imuCCCD, QByteArray::fromHex("0100"));
            }

            break;
        }

        default:
            // Nothing
            break;
    }
}

void GenkiBleReceiver::newData(QLowEnergyCharacteristic charact, QByteArray received_data) {
    QTextStream out(stdout);

    switch(charact.handle()) {
        case 0x0016: {
            quint64 now = QDateTime::currentMSecsSinceEpoch();
            quint64 dt_ms = now - m_lastDataTime;
            m_lastDataTime = now;

            memcpy(&m_quaternionData, received_data.data()+1, sizeof(m_quaternionData));
            out << "New quaternion data: " << m_quaternionData[0] << " " << m_quaternionData[1] << " " << m_quaternionData[2] << " " << m_quaternionData[3] << ", dt = " << dt_ms << endl;

            break;
        }

        case 0x0019: {
            float raw_data[3];
            int datatype = received_data[0];

            if(datatype == 1) {
                memcpy(&m_rawAccData, received_data.data()+1, sizeof(m_rawAccData));

                out << "Raw accel data: " << m_rawAccData[0] << " " << m_rawAccData[1] << " " << m_rawAccData[2] << endl;
            }

            if(datatype == 2) {
                memcpy(&m_rawGyrData, received_data.data()+1, sizeof(m_rawGyrData));

                out << "Raw gyr data: " << m_rawGyrData[0] << " " << m_rawGyrData[1] << " " << m_rawGyrData[2] << endl;
            }

            if(datatype == 3) {
                memcpy(&m_rawMagData, received_data.data()+1, sizeof(m_rawMagData));

                out << "Raw mag data: " << m_rawMagData[0] << " " << m_rawMagData[1] << " " << m_rawMagData[2] << endl;
                RTIMU_DATA data;
                data.accel.fromArray(m_rawAccData);
                data.gyro.fromArray(m_rawGyrData);
                data.compass.fromArray(m_rawMagData);

                emit newIMUData(data);

            }

            break;
        }

        default:
            break;
    }
}

void GenkiBleReceiver::confirmedDescriptorWrite(QLowEnergyDescriptor desc, QByteArray data) {
    QTextStream out(stdout);

    out << "Descriptor written successfully: " << desc.uuid().toString() << endl;
}

void GenkiBleReceiver::controllerError(QLowEnergyController::Error error) {
    QTextStream out(stdout);

//    out << "Error: " << error.errorString() << endl;
}











