INSTRUCTIONS/INFO: 

- Copy this entire folder to Max file path or root of your main working patch. 
- Add this folder to the Max search path (Options > File Preferences) and ensure subfolders box is checked ON. 
- *** SEE 'main-patch-demo.maxpat' FOR DETAILS ON USE: ***
	- load [genki-controller] as abstraction in your main patch. 
	- it takes 2 arguments:
		1. argument of the device number (140 - 143)
		2. (optional) inital preset number (0 - n)
	- outputs signals in OSC messages (see INSTRUCTIONS and main-patch-demo for details)

CHANGELOG: 

Still todo: 
- Gesture and audio recording with MuBu. 

[2017-04-02]
- Revised for multiple simultaneous controller use
- All basic controls available via OSC (see main-patch-demo)
- Presets are saved per controller number (140 - 143)
- By default 2d and 3d visualization is off to reduce CPU load. Can turn on w/ OSC or in the patch


[2017-03-30]
- new preset system saves min/max range calibrations
- new signal filtering algorithm for input smoothing (biquad LPF)
- rewrote entire scaling and calibration routines - shoudl work much better
- formatted inputs and outputs to send and receive OSC messages.
- still to do: 
	- write new multibuffer module to replace visual output. 
	- tweak for multi-controller use. 

[2017-03-25]
- added min/max scaling for each axis. set these first. 
    - These will not (currently) save with presets, but i'll add this.
- added smoothing. 0.25 seems nice but adjust to your own liking. 
    - 0.00 == no smoothing; 1.00 = max smoothing (slower change over time)
- Improvements to visual output. 
- Working with Baptiste this afternoon to implement better gesture recording. 

